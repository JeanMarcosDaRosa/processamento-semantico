# rastrea td o web_cache e retorna os id dos objetos cadastrados e executa etapa 1

import sys
import os
import base64
import calendar
import os
import rfc822
import sys
import tempfile
import textwrap
import time
import urllib
import urllib2
import urlparse
import thread
 
import subprocess
import string

#import BeautifulSoup
import umisc 
#import bitly

import xml.dom.minidom 

import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index

import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('IndexProd_Stage1')


ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)

log.addHandler(logHandler) 
log.addHandler(ch) 



pool2 = ConnectionPool('MINDNET', ['213.136.81.102:9160'],timeout=10)
#
wb2 = pycassa.ColumnFamily(pool2, 'web_cache10') # lugar para indexar 
wb22= pycassa.ColumnFamily(pool2, 'web_cache1')


def clear_tables():# limpar as tabelas para iniciar a indexacao
 tb_object_dt3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT_DT3') 
 tb_object3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT3') 
 tb_object_relaction3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_RELACTIONS3') 
 #======================
 tb_object3.truncate()
 tb_object_dt3.truncate()
 tb_object_relaction3.truncate()
 #======================


def run(id): 
 cmd='apy SemaIndexerStage1.pyc "igor.moraes" "parse-learn-product" "'+id+'"  '
 os.system(cmd)
 
 
all_index=0  
def get_to_index():
  global all_index
  rg=wb2.get_range()
  #
  cnt=0
  for ky,col in rg:
    id_pg=ky
    col['indexed']='S'
    wb22.insert(ky,col)
    wb2.remove(ky)
    #
    run(str(id_pg))
    #
    cnt+=1
    print 'page:',all_index
    all_index+=1
  return cnt
                              
    
    
#=======================
#clear_tables()
#=======================
rc=get_to_index()
while rc > 0:
  rc=get_to_index() 
 
print 'All.pg.processed!!!' 
