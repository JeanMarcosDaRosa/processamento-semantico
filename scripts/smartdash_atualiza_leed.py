#coding: latin-1
#scp root@localhost:/mindnet/server/smartdash_atualiza_leed.py  .
#scp smartdash_atualiza_leed.py root@localhost:/mindnet/server/  

import sys
import Pyro.core
import threading
import simplejson
import cookielib
import urllib2
import urllib
import umisc
from datetime import datetime
import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('fcb_post_comments')

#  TOKENS +++++++++++++++++++++++++++++


########################################

atualiza_tab_leeds=True
LOJA='americanas'

########################################


base_ids=['222977965716','127587077302295','362752461813','194775443140','115244248489160','163737793668569','128249847206220','259007827634',\
          '133481940001233','267796029913881','140871914508','162523134477','218694078173902','127110964018134','141466772559841',\
          '123874468062','114278431986431','194839529381','101036196646365','168235628589','167799875673','188394007863369',\
          '143598718987403','352806884759036','525738414202280','106121679436713','110387212341584','104725456232742','141304895927080']

cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [('Referer', 'http://login.facebook.com/login.php'),('Content-Type', 'application/x-www-form-urlencoded'),('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7 (.NET CLR 3.5.30729)')]

import bson

sys.path.append('./pymongo')

import pymongo

MONGO_URL = 'mongodb://transend:transend1@104.131.194.196:27017/smartdash'
conexao = pymongo.Connection(MONGO_URL)
banco = conexao.smartdash
prod_tbl = banco['produtos']
emp_tbl = banco['empresa']
plan_tbl = banco['planos']
est_tbl = banco['estatistica']
ativ_tbl = banco['atividades']
user_tbl = banco['usuario_empr']
leed_tbl = banco['face_leed']
rel_tbl = banco['relacionamentos']

chaves = banco['chaves']

#key='1'
#token_vitrine=chaves.find_one({'key':key})['token']
#print 'use token:',token_vitrine


def post_comment(token,obj_id,msg):
     args = {}    
     args['access_token'],args['message'] ,args['METHOD']=  token,msg,'POST'
     url='https://graph.facebook.com/'+obj_id+'/comments?'
     file = urllib2.urlopen(url,urllib.urlencode(args) )
     rt= file.read()   

def post_album(token,album):
     args = {}    
     args['access_token'],args['name'],args['METHOD'],args['message'],args['privacy']=token,album,'POST','Oferta','{"value":"EVERYONE"}'
     url='https://graph.facebook.com/me/albums?'
     file = urllib2.urlopen(url,urllib.urlencode(args) )
     rt= file.read()
     rt=rt.replace("\"","")
     rt=rt.replace("{","")
     rt=rt.replace("}","")
     rt=rt.replace("id:","")
     return rt     

def post_picture(token,album,foto_url,message_foto):
     args = {}    
     args['access_token'],args['url'],args['message'],args['METHOD'],args['upload file']=token,foto_url,message_foto,'post','false'
     #url='https://graph.facebook.com/'+album+'/photos?access_token='+token+'&message='+message_foto+'&url='+foto_url
     url='https://graph.facebook.com/'+album+'/photos'
     print 'post.url:',url

     params = urllib.urlencode(args) # parameters is dicitonar
     req = urllib2.Request(url, params) # PP_URL is the destionation URL
     req.add_header("Content-type", "application/x-www-form-urlencoded")
     file = urllib2.urlopen(req)

     #==
     #file = urllib2.urlopen(url,urllib.urlencode(args) )
     rt= file.read()
     rt=rt.replace("\"","")
     rt=rt.replace("{","")
     rt=rt.replace("}","")
     rt=rt.replace("id:","")
     return rt     

def post_object(token,obj_id,msg):
     args = {}    
     args['access_token'],args['message'] ,args['METHOD']=  token,msg,'POST'
     url='https://graph.facebook.com/'+obj_id+'/comments?'
     file = urllib2.urlopen(url,urllib.urlencode(args) )
     print file.read()


def addInteracaoEstatistica(empresa, tipo, valor, data):
     global est_tbl
     e=est_tbl.find_one({'_id':empresa})
     if e == None:
          try:
               est_tbl.insert({'_id':empresa,'interacoes':[{'tipo':tipo,'valor':valor,'data':data}] })
          except:
               pass
     #===========================
     arr=e['interacoes']
     arr.append({'tipo':tipo,'valor':valor,'data':data});
     est_tbl.update({'_id':empresa},{'$set':{'interacoes':arr}},upsert=True)
     inserirAtividade(empresa, tipo, valor);

def inserirAtividade(empresa, tipo, valor):
     global ativ_tbl
     e=ativ_tbl.find_one({'id_empresa':empresa})
     d=datetime.now().strftime("%Y-%m-%d %H:%M:%S %p")
     if( e!=None and e['visualizado']==0 ):
          valor+=e['qtd']
     ativ_tbl.update({'id_empresa': empresa, 'tipo': tipo}, {'$set':{'qtd':valor, 'visualizado':0, 'datahora':d, 'datahoracad': d}}, upsert=True)


def addFaceLeed(id_empresa, id_produto, fbid, nome_usuario, link_perfil, link_thumb, link_comment, gerou_ads, datahora, comment,descr_produto):
     global leed_tbl, prod_tbl
     #
     try:
          prod=bson.Binary(descr_produto.decode('utf8').encode('latin-1'))
     except:
          log.exception("-->"+descr_produto)
          prod=bson.Binary('--')
     #   
     try:
          usr=bson.Binary(nome_usuario.decode('latin-1').encode('utf8'))
     except: 
          try: 
               usr=bson.Binary(nome_usuario.encode('utf8'))
          except:
               try:
                    usr=bson.Binary(nome_usuario)
               except:
                    return
     try:
          comment=bson.Binary(comment).decode('latin-1').encode('utf8') 
     except:     
          comment=bson.Binary(comment.encode('utf8')) 
     r = leed_tbl.insert({
          'id_empresa':id_empresa,
          'id_produto':id_produto,
          'desc_produto':prod,
          'fbid':str(fbid),
          'nome_usuario':usr,
          'link_perfil':link_perfil,
          'link_thumb':link_thumb,
          'link_comment':link_comment,
          'gerou_ads':gerou_ads,      
          'datahora':datahora,
          'datahoracad':datetime.now().strftime("%Y-%m-%d %H:%M:%S %p"),
          'comment':comment
     })
     addInteracaoEstatistica(id_empresa, 'leeds', 1, datetime.now().strftime("%d-%m-%Y"))   


def addRelacionamento(id_empresa, id_produto, fbid, nome_usuario, link_perfil, link_thumb, link_comment, gerou_ads, datahora, comment):
     global rel_tbl, prod_tbl
     p = buscaProduto(id_produto, id_empresa)
     r = rel_tbl.insert({
          'id_empresa':id_empresa,
          'id_produto':id_produto,
          'desc_produto':p['desc_produto'],
          'fbid':fbid,
          'nome_usuario':nome_usuario,
          'link_perfil':link_perfil,
          'link_thumb':link_thumb,
          'datahora':datahora,
          'datahoracad':datetime.now().strftime("%Y-%m-%d %H:%M:%S %p")
     })
     addInteracaoEstatistica(id_empresa, 'relacionamentos', 1, datetime.now().strftime("%d-%m-%Y")) 


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def limpa_smartdash():
     #================================    
     leed_tbl.remove({'id_empresa':LOJA})
     #================================
     fil=open('/mindnet/vitrines/publish_file_r',"w")
     fil.close()
     #================================
     import pycassa
     from pycassa.pool import ConnectionPool
     from pycassa.columnfamily import ColumnFamily
     from pycassa import index
     pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=100000)
     pub = pycassa.ColumnFamily(pool2, 'to_posting2')     
     prodc=[]
     for ky,cols in pub.get_range():
          cols['kidexpub'] = 'N'   
          pub.insert(ky,cols)

clients=[]
prods_keys=[]
def cache_prods():
     global prods_keys 
     global LOJA
     import pycassa
     from pycassa.pool import ConnectionPool
     from pycassa.columnfamily import ColumnFamily
     from pycassa import index
     pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=100000)
     pub = pycassa.ColumnFamily(pool2, 'to_posting2')     
     prodc=[]
     for ky,cols in pub.get_range():
          fnd=False 
          try:
               if cols['kidexpub'] == 'S' or cols['kidexpub'] == 'E' :
                    fnd=True
          except:
               fnd=False   
          if fnd: continue  
          prodc.append([ky,cols])
          if len(prodc) >= 100:
               prods_keys.append(prodc)
               prodc=[]
          if len(prods_keys) >= 10:
               return 
     if len(prodc) > 0:
          prods_keys.append(prodc)          

def atualiza_post_process(subset,ithread,startcounter):
     global LOJA
     ithread.started=True     
     prod=''
     import pycassa
     from pycassa.pool import ConnectionPool
     from pycassa.columnfamily import ColumnFamily
     from pycassa import index
     pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=100000)
     tab2 = pycassa.ColumnFamily(pool2, 'cache_products')     
     pub = pycassa.ColumnFamily(pool2, 'to_posting2')     
     web1 = pycassa.ColumnFamily(pool2, 'web_cache1')     

     MONGO_URL='mongodb://mdnet1:acc159753@localhost:27017/mdnet'
     connM = pymongo.Connection(MONGO_URL)
     dbM=connM.mdnet
     fcbu=dbM['fcb_users']

     counter=startcounter
     for ky,col in subset: 
          prods=col['id_product'].decode('hex')
          rc=tab2.get(prods)  
          descr=''
          try:
               descr=rc['descricao']
          except:
               descr=''  
          if descr=='':
               print '***********************************getdescr from id from table*************************'	
               colc1=web1.get(col['id_product'])
               descr=colc1['title']
          from_id3=col['from_id']
          from_id3=from_id3.replace('\'','')
          from_id3=from_id3.replace('[','')
          from_id3=from_id3.replace(']','')
          from_id2=from_id3.split(',' )
          from_id=''
          for ifr in from_id2:
               ifr=umisc.trim(ifr)
               if len(ifr) > 1:
                    if ifr[0] in ['0','1','2','3','4','5','6','7','8','9'] and ifr[1] in ['0','1','2','3','4','5','6','7','8','9']:
                         from_id=ifr 
                         break
          counter+=1
          col['id_post']=str(counter)
          pub.insert(ky,col)
          prod=rc['cod_p'] 
          prod+='.'+str(counter)
          prodk='http://smartsocial.com.br/p.php?p='+prod
          prod=rc['cod_p'] 
          #====================================================================
          print 'atualiza leed prod:',counter
          #====================================================================
          msg_id=from_id
          print 'msg.id:',msg_id
          try:
               colc=web1.get(msg_id)
          except:
               continue	
          id_user=colc['id_usr']
          comm=colc['pg']
          comm=comm.replace('{$title-destac$}','')
          url_message='http://graph.facebook.com/'+id_user
          name_user=''
          try:
               #cfile = urllib2.urlopen(url_message)
               #result = simplejson.load(cfile)
               #name_user=result['name']
               resultb1=fcbu.find_one({'_id':id_user})
               name_user=resultb1['u_name']
               #==
               url_picture_large='http://graph.facebook.com/'+id_user+'/picture?width=140&height=140'
               usock = opener.open(url_picture_large)
               finalurl = usock.geturl()  
               #============
          except: 
               log.exception("msg_id:"+str(id_user))
               time.sleep(5)
               try:
                    cfile = urllib2.urlopen(url_message)
                    result = simplejson.load(cfile)
                    name_user=result['name']
                    #==
                    url_picture_large='http://graph.facebook.com/'+id_user+'/picture?width=140&height=140'
                    usock = opener.open(url_picture_large)
                    finalurl = usock.geturl()  
               except:     
                    time.sleep(5)
                    try:
                         cfile = urllib2.urlopen(url_message)
                         result = simplejson.load(cfile)
                         name_user=result['name']
                         #==
                         try:
                              url_picture_large='http://graph.facebook.com/'+id_user+'/picture?width=140&height=140'
                              usock = opener.open(url_picture_large)
                              finalurl = usock.geturl()  
                         except:
                              pass
                    except:     
                         continue
          perr=False		  
          try:		  
               addFaceLeed(LOJA, prod, id_user, name_user, 'http://facebook.com/'+id_user, finalurl, 'http://graph.facebook.com/'+msg_id, 'N', datetime.now().strftime("%Y-%m-%d"), comm,descr)      
          except:
               log.exception("")
               perr=True
          #
          if not perr:
               col['kidexpub'] = 'S'
               pub.insert(ky,col)
          else: 
               col['kidexpub'] = 'E'
               pub.insert(ky,col)
          #====================================================================
          #msg_k=('Encontramos em seus posts interesses em determinados conte�dos. Tomamos a liberdade de fazer essa conex�o entre voc� e o que gosta. Nossas bases identificaram que o post abaixo � interessante para voc� \n '.decode('latin1').encode('utf8')+prodk.decode('latin1').encode('utf8') )
          #post_object(token,msg_id,msg_k)
          #
          if not perr:
               clients.append([msg_id,prodk,id_user])
     ithread.flag=True 
#================      
class th_cnt:
     def __init__(self,flg):
          self.flag=flg
          self.started=False
#===          
import thread
import time
if len(sys.argv) > 1:
     limpa_smartdash()
     print 'Clean SmartDash Leed Cache...'
     time.sleep(5)
#========= 
def processo():
     cache_prods()
     if len(prods_keys)==0:
          return False
     ithread=[]
     ithread.append(th_cnt(False))
     ithread.append(th_cnt(False))
     ithread.append(th_cnt(False))
     ithread.append(th_cnt(False))
     ithread.append(th_cnt(False))
     #======================
     ll=len(prods_keys)-1
     ith=0
     startcnt=1
     while ll >=0:     
          startcounterb=startcnt    
          startcnt+=20
          produto=prods_keys[ll]
          print 'len.ithread:',len(ithread)
          print 'ith:',ith
          thread.start_new_thread(atualiza_post_process,(produto,ithread[ith],startcounterb) )  
          ll-=1
          if ith>=4 or ll==0 :
               ith=0
               while True:
                    time.sleep(5)
                    r=False
                    indpth=0
                    for p in ithread:
                         if p.started == True:
                              if p.flag != True:
                                   print 'thread:',indpth,'not finished'
                                   r=True
                         indpth+=1     
                    if not r:
                         ithread=[]
                         ithread.append(th_cnt(False))
                         ithread.append(th_cnt(False))
                         ithread.append(th_cnt(False))
                         ithread.append(th_cnt(False))
                         ithread.append(th_cnt(False))
                         break         
          else:
               ith+=1   

     ant=''
     try:
          fil=open('/mindnet/vitrines/publish_file_r',"r")     
          ant=fil.read()
          fil.close()
     except:
          pass
     #=====================
     fil=open('/mindnet/vitrines/publish_file_r',"w")
     if ant != '':
          fil.write(ant)
     for cli in clients:
          [msg_id,prodk,id_user]=cli 
          #msg_k=('Encontramos em seus posts interesses em determinados conte�dos. Tomamos a liberdade de fazer essa conex�o entre voc� e o que pode gostar. Nossas bases identificaram que o post abaixo � interessante para voc� \n '.decode('latin1').encode('utf8')+prodk.decode('latin1').encode('utf8') )
          #=====    
          fil.write(msg_id+'\1'+prodk+'\1'+id_user+'\2') 
     fil.close()
     return True




processo() 


