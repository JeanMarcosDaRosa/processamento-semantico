#coding: latin-1
import sys
import umisc
from datetime import datetime
import logging
from StringIO import StringIO
import urllib
import urllib2

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('vitrines_publish_from_file')


########################################



is_win=False
if sys.platform == 'win32' or sys.platform == 'win64':
     is_win=True


import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index
pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=100000)
tab2 = pycassa.ColumnFamily(pool2, 'cache_products')     
pub = pycassa.ColumnFamily(pool2, 'to_posting2')     
web1 = pycassa.ColumnFamily(pool2, 'web_cache1')     


def ids_by_p(s1):
     ln=len(s1)-1
     rt=''
     while ln >=0 :
          if s1[ln]=='.': break
          rt=s1[ln]+rt 
          ln-=1
     return rt     

def get_prod(ids):
     cl4 = index.create_index_expression(column_name='id_post', value=ids)
     clausec = index.create_index_clause([cl4],count=1)
     rest=pub.get_indexed_slices(clausec)
     for ky,col in rest:
            colc1=web1.get(col['id_product'])
            colc1['id_post']=ids
            print 'found page:',ids
            return colc1
prods_vr=[]
def insere_new_prod_id(id_msg,prodrefer,col):
     ind=0
     ln=len(prods_vr)
     found=False
     while ind <= len(prods_vr)-1:          
       prod = prods_vr[ind]
       if prod[0]==prodrefer:
         prods_vr[ind][1].append(id_msg)     
         found=True
         break
       ind+=1 
     if not found:
        prods_vr.append([prodrefer,[id_msg],col])  

def classifica(id_msg,prod):
     c=get_prod(prod)
     insere_new_prod_id(id_msg,c['prod'],c)     
     
if not is_win:
 fil=open('/mindnet/vitrines/publish_file_r',"r")
else:
 fil=open('h:\\mindnet\\publish_file\\publish_file_r',"r")
all_text=fil.read()
lines=all_text.split('\2')
print 'total lines:',len(lines)
clline1=1
for cli in lines:
    print 'run line:',clline1 , 'of:',len(lines)
    clline1+=1
    if clline1 > 5000: break
    try:
     elements=cli.split('\1') 
     idm=elements[0]
     prodk=elements[1]
     id_prod=ids_by_p(prodk)
     usuario=web1.get(idm)
     # classificar os produtos c/ seus clientes
     classifica(usuario['id_usr'],id_prod)
    except: pass 
fil.close() 
print 'Total prods:',len(prods_vr)
for r in prods_vr:
     [prod,msgs,cols]=r
     if not is_win:
      fil=open('/mindnet/vitrines/ready/'+cols['id_post'],"w")
     else:
      fil=open('h:\\mindnet\\publish_file\\ready\\'+cols['id_post'],"w")
     fil.write(cols['lomadee_url']+'\n') 
     for m in msgs:
          fil.write(m+'\n')
     fil.close()