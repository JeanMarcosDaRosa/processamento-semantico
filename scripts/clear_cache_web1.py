import sys
import os

import pycassa
from pycassa.pool import ConnectionPool
from pycassa import index
from pycassa.columnfamily import ColumnFamily

pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10000)
web=pycassa.ColumnFamily(pool2, 'web_cache1')

idx=0
for ky,col in web.get_range():
 col['indexed']='N'
 web.insert(ky,col)
 idx+=1
 
print 'total:',idx 