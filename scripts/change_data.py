import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import sys

def insert_post(poolDst, cf, trunc=False):
	cfD = ColumnFamily(poolDst, cf)
	if(trunc): cfD.truncate()
	print 'modificando dados em',cf,'...'
	key = '469570479854037_469577646519987'
	row = {'doc_id': '469570479854037_469577646519987', 'id': '469570479854037_469577646519987', \
	       'id_usr': '1512771255', 'indexed': 'S', 'name_usr': '', \
	       'pg': 'pare este verao 2014 quer comprar um blusao da marca colcci, cor branco', 'processed': 'N', \
	       'purpose': 'user', 'story': '-', 'temo': 'user', 'title': ' Jean Marcos da Rosa', 'tp': 'photo', 'tps': 'F', \
	       'url': 'https://www.facebook.com/CorrePaula/photos/a.315384201939333.1073741825.195949310549490/469570479854037/?ty:1&relevant_cou:1', \
	       'url_icon': 'https://fbstatic-a.akamaihd.net/rsrc.php/v2/yx/r/og8V99JVf8G.gif', 'url_picture': '-', 'usr': 'jean.marcos' }
	
	cfD.insert(key, row)
	
def insert_product(poolDst, cf, trunc=False):
	cfD = ColumnFamily(poolDst, cf)
	if(trunc): cfD.truncate()
	print 'modificando dados em',cf,'...'
	key = '687474703a2f2f7777772e7375626d6172696e6f2e636f6d2e62722f70726f6475746f2f3131353034373436362f626c7573612d6d616e64692d6c69737472616461236f70696e696f6573'
	row = {'id_usr': '', 'preco': '', \
	       'id': '687474703a2f2f7777772e7375626d6172696e6f2e636f6d2e62722f70726f6475746f2f3131353034373436362f626c7573612d6d616e64692d6c69737472616461236f70696e696f6573', \
	       'story': '', 'url_icon': '', 'title': '', 'name_usr': '', 'tp': 'P', \
	       'pg': 'definicao defs Tamanho: 10A,12A,14A,16A,18A. \n definicao defs Cor: Amarelo,Azul. \n definicao defs Composi\xe7\xe3: 96% Modal / 04% Elastano. \n definicao defs Marca: Mandi. \n definicao defs Cole\xe7\xe3: Ver\xe3o 2014. \n definicao defs Ocasi\xe3: Casual. \n definicao defs Tend\xeancia: Geom\xe9trico. \n definicao defs Sa: (11) 2186\n9000. \n definicao defs Fabricante: Mandi. \n definicao defs C\xf3digo de Barras: 1069100919093, 1069100864652, 1069100646494, 1069100200993, 1069100490592, 1069100992287, 1069101008338, 1069100057450, 1069100188024, 1069100624614. \n ', \
	       'prod': 'http://www.submarino.com.br/produto/115047466/blusa-mandi-listrada#opinioes', 'indexed': 'N', 't_url': '', 'usr': 'index', \
	       'lomadee_url': '', 't_description': '', 't_title': '', 'purpose': 'index', 'termo': 'product', 'url_picture': '', \
	       'url': 'http://www.submarino.com.br/produto/115047466/blusa-mandi-listrada#opinioes', 'tps': 'P', 'processed': 'N', \
	       '_id': '687474703a2f2f7777772e7375626d6172696e6f2e636f6d2e62722f70726f6475746f2f3131353034373436362f626c7573612d6d616e64692d6c69737472616461236f70696e696f6573', \
	       'doc_id': '687474703a2f2f7777772e7375626d6172696e6f2e636f6d2e62722f70726f6475746f2f3131353034373436362f626c7573612d6d616e64692d6c69737472616461236f70696e696f6573', \
	       't_image': ''}

	cfD.insert(key, row)
	
def change_post(poolDst, cf, key):
	cfD = ColumnFamily(poolDst, cf)
	print 'modificando dados em',cf,'...'
	post=cfD.get(key)
	post['pg']= 'quero comprar um Blusao Colcci, na cor branca'
	cfD.insert(key, post)

poolDestino = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)
#insert_post(poolDestino, 'web_cache1', True)
#insert_product(poolDestino, 'web_cache1', True)
change_post(poolDestino, 'web_cache1', '469570479854037_469577646519987')
print 'finalizou.'
