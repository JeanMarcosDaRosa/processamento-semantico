import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa.system_manager import *
import json
import sys

def create_keyspace(sys, k):
	sys.create_keyspace(k, strategy_options={"replication_factor": "1"})

def create_index(sys, key_space):
	print 'Criando indices...'
	
	print 'create index on SEMANTIC_RELACTIONS...'
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS', 'obj_orig', 'BytesType', index_name='relaction_obj_orig')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS', 'obj_dest', 'BytesType', index_name='relaction_oobj_dest')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS', 'opcode', 'BytesType', index_name='relaction_opcode')
	
	print 'create index on SEMANTIC_OBJECT_DT3...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT3', 'LEV', 'BytesType', index_name='object_LEV3')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT3', 'datach', 'BytesType', index_name='datach_index3')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT3', 'topico', 'BytesType', index_name='topico_index3')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT3', 'object', 'BytesType', index_name='object_index3')
	
	print 'create index on to_posting2...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT3', 'id_post', 'AsciiType', index_name='to_posting2_69645f706f7374_idx')
	
	print 'create index on cache_products...'
	sys.create_index(key_space, 'cache_products', 'INDEXED', 'AsciiType', index_name='cache_products_494e4445584544_idx')
	
	print 'create index on SEMANTIC_OBJECT_DT3_1_4...'
	sys.create_index(key_space, 'cache_products', 'LEV', 'BytesType', index_name='object_LEV3_1_4')
	sys.create_index(key_space, 'cache_products', 'datach', 'BytesType', index_name='datach_index3_1_4')
	sys.create_index(key_space, 'cache_products', 'topico', 'BytesType', index_name='topico_index3_1_4')
	sys.create_index(key_space, 'cache_products', 'object', 'BytesType', index_name='object_index3_1_4')
	
	print 'create index on web_cache1...'
	sys.create_index(key_space, 'web_cache1', 'indexed', 'AsciiType', index_name='web_cache1_696e6465786564_idx')
	
	print 'create index on SEMANTIC_OBJECT...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT', 'objeto', 'BytesType', index_name='object_objeto')
	
	print 'create index on web_cache3...'
	sys.create_index(key_space, 'web_cache3', 'indexed', 'AsciiType', index_name='web_cache3_696e6465786564_idx')
	
	print 'create index on SEMANTIC_OBJECT3_1_4...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT3_1_4', 'objeto', 'BytesType', index_name='object_objeto3_1_4')
	
	print 'create index on cache_links...'
	sys.create_index(key_space, 'cache_links', 'INDEXED', 'AsciiType', index_name='cache_links_494e4445584544_idx')
	
	print 'create index on web_cache10...'
	sys.create_index(key_space, 'web_cache10', 'doc_id', 'AsciiType', index_name='web_cache10_646f635f6964_idx')
	
	print 'create index on SEMANTIC_OBJECT_DT...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT', 'LEV', 'BytesType', index_name='object_LEV')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT', 'datach', 'BytesType', index_name='datach_index')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT', 'topico', 'BytesType', index_name='topico_index')
	sys.create_index(key_space, 'SEMANTIC_OBJECT_DT', 'object', 'BytesType', index_name='object_index')
	
	print 'create index on web_cache15...'
	sys.create_index(key_space, 'web_cache15', 'id_usr', 'AsciiType', index_name='web_cache15_69645f757372_idx')
	
	print 'create index on fuzzy_store...'
	sys.create_index(key_space, 'fuzzy_store', 'layout_onto', 'BytesType', index_name='fuzzy_store_layout_onto')
	
	print 'create index on fcb_users1...'
	sys.create_index(key_space, 'fcb_users1', 'indexed', 'BytesType', index_name='fcb_users1_696e6465786564_idx')
	
	print 'create index on SEMANTIC_RELACTIONS3...'
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3', 'obj_orig', 'BytesType', index_name='relaction3_obj_orig')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3', 'obj_dest', 'BytesType', index_name='relaction3_obj_dest')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3', 'opcode', 'BytesType', index_name='relaction3_opcode')
	
	print 'create index on SEMANTIC_OBJECT3...'
	sys.create_index(key_space, 'SEMANTIC_OBJECT3', 'objeto', 'BytesType', index_name='object_objeto3')
	
	print 'create index on SEMANTIC_RELACTIONS3_1_4...'
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3_1_4', 'obj_orig', 'BytesType', index_name='relaction3_1_4_obj_orig')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3_1_4', 'obj_dest', 'BytesType', index_name='relaction3_1_4_obj_dest')
	sys.create_index(key_space, 'SEMANTIC_RELACTIONS3_1_4', 'opcode', 'BytesType', index_name='relaction3_1_4_opcode')

def drop_cf(pool, sys, key_space, list_cf):
	print 'Dropando column familys da keyspace',key_space,'...'
	for cf in list_cf:
		cfT = ColumnFamily(pool, cf)
		cfT.truncate()
		print '-- drop', cf, 
		sys.drop_column_family(key_space, cf)
		print 'OK.'

def create_cf(sys, key_space, list_cf):
	print 'Criando column familys na keyspace',key_space,'...'
	for cf in list_cf:
		print '-- create', cf, 
		sys.create_column_family(key_space, cf, key_validation_class='BytesType')
		print 'OK.'
	create_index(sys, key_space)
	
def show_rows_default(pool, cf):
	c = ColumnFamily(pool,  cf)
	for k, r in c.get_range():
		print k, r

def count_rows_default(pool, tbl):
	c = ColumnFamily(pool,  tbl)
	print len(dict(c.get_range()).keys())

def clone_objs(poolSrc, poolDst, list_cf, trunc=False):
	for cf in list_cf:
		idx = 0
		cfS = ColumnFamily(poolSrc,  cf)
		cfD = ColumnFamily(poolDst, cf)
		print 'clonando ',cf, '...'
		if trunc: cfD.truncate()
		#print len(dict(cfS.get_range(column_count=1)).keys())
		for key, row in cfS.get_range(buffer_size=4096):
			cfD.insert(key, row)
			idx+=1
			if idx % 5 == 0:
				print '\rRegistro' , idx,
				sys.stdout.flush()	
		print '\n', cf, '- done. count:', idx

def clone_objs_limit(poolSrc, poolDst, list_cf, trunc=False, limit=20):
	for cf in list_cf:
		idx = 0
		cfS = ColumnFamily(poolSrc,  cf)
		cfD = ColumnFamily(poolDst, cf)
		print 'clonando ',cf, '...'
		if trunc: cfD.truncate()
		#print len(dict(cfS.get_range(column_count=1)).keys())
		for key, row in cfS.get_range(buffer_size=limit):
			cfD.insert(key, row)
			idx+=1
			if idx % 5 == 0:
				print '\rRegistro' , idx,
				sys.stdout.flush()
			if idx >= limit:
				break
		print '\n', cf, '- done. count:', idx

keySrc  = 'MINDNET_BKP'
keyDest = 'MINDNET'
sysDestino  = SystemManager('localhost:9160')
sysSource  = SystemManager('localhost:9160')
#create_keyspace(sysDestino, keyDest)
poolSource  = ConnectionPool(keySrc,['localhost:9160'],timeout=1000)
poolDestino = ConnectionPool(keyDest,['localhost:9160'],timeout=1000)

kD = sysDestino.get_keyspace_column_families(keyDest)
kS = sysSource.get_keyspace_column_families(keySrc)

#drop_cf(poolDestino, sysDestino, keyDest, kD)
#create_cf(sysDestino, keyDest, kS) #passa a lista de tabelas que tem na origem, para serem criadas no destino

#list_get = ['fz_store_defs', 'knowledge_manager', 'fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY']

list_get = ['SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'SEMANTIC_RELACTIONS']

clone_objs(poolSource, poolDestino, list_get, True)

#clone_objs_limit(poolSource, poolDestino, list_get, True, 10)

#count_rows_default(poolMind, 'DATA_BEHAVIOUR_CODE_PY')
#count_rows_default(poolLocal, 'DATA_BEHAVIOUR_CODE_PY')

sysSource.close()
sysDestino.close()

print 'done.'
