#coding: latin-1

import sys
import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index

#pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10)

pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10)

tab2 = pycassa.ColumnFamily(pool2, 'cache_products')
tab3 = pycassa.ColumnFamily(pool2, 'cache_links') # links temporarios


tb_object_dt = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT_DT3') 
tb_object = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT3') 
tb_object_relaction = pycassa.ColumnFamily(pool2, 'SEMANTIC_RELACTIONS3') 


tb_object_dt3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT_DT3_1_4') 
tb_object3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_OBJECT3_1_4') 
tb_object_relaction3 = pycassa.ColumnFamily(pool2, 'SEMANTIC_RELACTIONS3_1_4') 


tb_object.truncate()
tb_object3.truncate()

tb_object_dt.truncate()
tb_object_dt3.truncate()

tb_object_relaction.truncate()
tb_object_relaction3.truncate()


tab2.truncate()
tab3.truncate()


tab4 = pycassa.ColumnFamily(pool2, 'web_cache1')
tab5 = pycassa.ColumnFamily(pool2, 'fcb_users1')


tab4.truncate()
tab5.truncate()
