import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa.system_manager import *
import json
import sys

sysDestino  = SystemManager('213.136.81.102:9160')
#pool  = ConnectionPool('MINDNET',['5.189.144.4:9160'],timeout=1000)

def drop_cf(pool, sys, list_cf):
	print 'Dropando column familys da keyspace','MINDNET','...'
	for cf in list_cf:
		try:
			cfT = ColumnFamily(pool, cf)
			cfT.truncate()
			print '-- drop', cf, 
			sys.drop_column_family('MINDNET', cf)
			print 'OK.'
		except:
			print 'OK.'
		
#list_cf = ['fuzzy_store', 'fz_store_defs', 'fz_store_refer', 'fz_store_pref', 'fz_store_sufix', 'fz_arround_points', 'knowledge_manager', 'DATA_BEHAVIOUR_PY', 'DATA_BEHAVIOUR_CODE_PY', 'web_cache1', 'web_cache3', 'web_cache10', 'web_cache15', 'cache_links', 'cache_products', 'to_posting', 'to_posting2', 'to_posting3', 'to_posting_perfil', 'fcb_users1']
list_cf = ['mindnet_cxpostal_classif','SEMANTIC_RELACTIONS', 'mindnet_usuario', 'mindnet_groups_ref', 'mindnet_nick_ref', 'SEMANTIC_OBJECT_DT3', 'SEMANTIC_OBJECT3_1_4', 'SEMANTIC_OBJECT_DT3_1_4', 'SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'mindnet_groups', 'mindnet_cxpostal_comments', 'SEMANTIC_RELACTIONS3', 'SEMANTIC_OBJECT3', 'mindnet_cxpostal', 'mindnet_feed3', 'mindnet_feed2', 'mindnet_cxpostal_classif', 'mindnet_comments', 'mindnet_contatos_of', 'mindnet_feed', 'SEMANTIC_RELACTIONS3_1_4', 'mindnet_cache_images', 'freeb_defs', 'web_cache10_users', 'freeb_c2', 'freeb_cl', 'freeb_objs']
#drop_cf(pool, sysDestino, list_cf)
print sysDestino.list_keyspaces()
#print sysDestino.get_keyspace_column_families('MINDNET')
sysDestino.drop_keyspace('demo')
print sysDestino.list_keyspaces()