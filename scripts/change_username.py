import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import sys

def change_usr(poolDst, list_cf, usr_src, usr_dst):
	for cf in list_cf:
		idx = 0
		cfD = ColumnFamily(poolDst, cf)
		print 'modificando usuario',usr_src,'para',usr_dst,'em',cf,'...'
		for key, row in cfD.get_range(buffer_size=4096):
			found = False
			#print key, row
			if 'username' in row and row['username'] == usr_src :
				row['username'] = usr_dst
				found=True
			if 'usr' in row and row['usr'] == usr_src :
				row['usr'] = usr_dst
				found=True			
			if 'USERNAME' in row and row['USERNAME'] == usr_src :
				row['USERNAME'] = usr_dst
				found=True			
			if found :			
				cfD.insert(key, row)
				idx+=1
				if idx % 5 == 0:
					print '\rRegistro' , idx,
					sys.stdout.flush()	
		print '\n', cf, '- pronto. atualizou:', idx

poolDestino = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)
#list_get = ['SEMANTIC_RELACTIONS', 'fz_store_defs', 'knowledge_manager', 'SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'SEMANTIC_OBJECT3', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY', 'SEMANTIC_RELACTIONS3_1_4', 'web_cache15', 'web_cache1']
#list_get = ['fz_store_defs', 'knowledge_manager','fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix','fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY']
#list_get = ['SEMANTIC_RELACTIONS', 'fz_store_defs', 'knowledge_manager', 'SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY']
#list_get = ['fz_store_defs', 'knowledge_manager', 'fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY']

list_get = ['SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'SEMANTIC_RELACTIONS']
change_usr(poolDestino, list_get, 'igor.moraes', 'jean.marcos')
print 'finalizou.'
