import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import sys

def saveExtracted(filepath, filename, content):
 file = open(filepath+'/'+filename, "w")
 file.write(content)
 file.close()

def clean_s_k(strc2):
 k=''
 strc=''+strc2
 for ss in strc:
  if ss.lower () in ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','z','y','w','0','1','2','3','4','5','6','7','8','9','~','`','!','@','#','$','%','^','&','*','(',')','{','}','[',']','\'','"',':',';','/','?','<',',','>','.','\\','|','-','_','=','+',' ','\n']:
   k+=(ss)
 return k

def extract(poolDst, cf):
 cfD = ColumnFamily(poolDst, cf)
 for key, row in cfD.get_range(buffer_size=4096):
   o=clean_s_k(row[u'CODE'])
   print 'Extracting', key,
   saveExtracted('extract_code', key+'.py', o)
   print '- OK'
   
poolDestino = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)
extract(poolDestino, 'DATA_BEHAVIOUR_CODE_PY')
