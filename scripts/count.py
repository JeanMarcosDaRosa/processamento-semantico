import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa.system_manager import *
import json
import sys

def count_rows_default(pool, tbl):
	c = ColumnFamily(pool,  tbl)
	print tbl,' - ',len(dict(c.get_range()).keys())

def count_objs(pool, list_cf, trunc=False):
	for cf in list_cf:
		count_rows_default(pool, cf)
keyDest = 'MINDNET'

poolDestino = ConnectionPool(keyDest,['localhost:9160'],timeout=1000)

list_get = ["SEMANTIC_RELACTIONS", "SEMANTIC_RELACTIONS3", "SEMANTIC_RELACTIONS3_1_4", "SEMANTIC_OBJECT", "SEMANTIC_OBJECT3", "SEMANTIC_OBJECT3_1_4", "SEMANTIC_OBJECT_DT", "SEMANTIC_OBJECT_DT3", "SEMANTIC_OBJECT_DT3_1_4", "to_posting", "to_posting2", "to_posting_perfil"]

count_objs(poolDestino, list_get, True)

print 'done.'
