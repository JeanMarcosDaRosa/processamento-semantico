<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1422653470804" ID="ID_77101571" MODIFIED="1422746264307" STYLE="fork" TEXT="Processamento Sem&#xe2;ntico">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="launch"/>
<node CREATED="1422663190453" HGAP="112" ID="ID_1583078469" MODIFIED="1422667999995" POSITION="right" STYLE="bubble" TEXT="Coleta de Usu&#xe1;rios" VSHIFT="-122">
<node CREATED="1422663411098" HGAP="62" ID="ID_297234726" MODIFIED="1422663639932" STYLE="bubble" TEXT="Busca na base do aplicativo" VSHIFT="-60"/>
</node>
<node CREATED="1422663272841" HGAP="99" ID="ID_1993786570" MODIFIED="1422663718862" POSITION="right" STYLE="bubble" TEXT="Coleta de Feeds (postagens)" VSHIFT="49">
<node CREATED="1422663430836" ID="ID_773189637" MODIFIED="1422663614670" STYLE="bubble" TEXT="Busca na base do aplicativo"/>
</node>
<node CREATED="1422663297604" HGAP="108" ID="ID_1969944775" MODIFIED="1422663720200" POSITION="right" STYLE="bubble" TEXT="Coleta de Conte&#xfa;do para inferencia" VSHIFT="101">
<node CREATED="1422663449943" HGAP="30" ID="ID_1796024056" MODIFIED="1422663614673" STYLE="bubble" TEXT="Busca na base do aplicativo" VSHIFT="-38"/>
<node CREATED="1422663464905" HGAP="32" ID="ID_749446864" MODIFIED="1422663614674" STYLE="bubble" TEXT="Coleta de canais conhecidos (com crawlers)" VSHIFT="31"/>
</node>
<node CREATED="1422746168418" ID="ID_1825921627" MODIFIED="1422746266027" POSITION="left" TEXT="Compilar L&#xf3;gica Fuzzy">
<node CREATED="1422746199991" ID="ID_1309201356" MODIFIED="1422810450885" TEXT="run mindnet/compile  -&gt; compile_rct5_2_file.py" VSHIFT="-82">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Insere no banco de dados todas as senten&#231;as semanticas encontradas na pasta mindnet/
    </p>
    <p>
      Atualizando o conte&#250;do das tabelas:
    </p>
    <p>
      fuzzy_store
    </p>
    <p>
      fz_store_pref
    </p>
    <p>
      fz_store_defs
    </p>
    <p>
      fz_store_sufix
    </p>
    <p>
      fz_store_refer
    </p>
    <p>
      fz_arround_points
    </p>
    <p>
      
    </p>
    <p>
      SEMANTIC_OBJECT
    </p>
    <p>
      SEMANTIC_OBJECT_DT
    </p>
    <p>
      SEMANTIC_RELACTIONS
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
