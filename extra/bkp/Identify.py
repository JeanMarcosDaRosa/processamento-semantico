import mdLayout
import mdOntology

def start_db(pool2):
    mdOntology.start_db(pool2)


import umisc

def prepare_layout(id, username):
    lay = mdLayout.mdLayout(id, username)
    print 'prepare_layout:', lay.name
    lay.prepare()
    return lay


def prepare_data(dt):

    def tokeniz(dt):
        rts = []
        tmp = ''
        start_comm = False
        for e in dt:
            if e in ('"',):
                if not start_comm:
                    start_comm = True
                else:
                    start_comm = False
            if e in ('!',):
                tmp += e
            elif e in (' ', '.', ';', ',', '(', ')', '[', ']', '{', '}', '?', '!', '"', "'", ':', '@', '\\', '/', '$'):
                if tmp != '':
                    rts.append(tmp)
                if e in ('.', ';', ',', '(', ')', '[', ']', '{', '}', '?', '!', '"', "'", ':', '@', '\\', '/', '$'):
                    rts.append(e)
                tmp = ''
            else:
                tmp += e

        if tmp != '':
            rts.append(tmp)
        rts2 = []
        hvounf = False
        if True:
            for r in rts:
                if len(r) > 20:
                    rts2.append(r)
                elif len(r) > 0:
                    if r[0] in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'):
                        f = True
                        tmp = ''
                        r1 = False
                        for s in r:
                            if r1:
                                pass
                            elif not f:
                                if s not in ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_'):
                                    rts2.append(tmp)
                                    tmp = ''
                                    r1 = True
                            tmp += s
                            f = False

                        if tmp != '':
                            rts2.append(tmp)
                    else:
                        rts2.append(r)

        return rts2

    frases = []
    acumul = []
    toks = tokeniz(dt)
    sm = []
    for tok in toks:
        if tok in sm:
            if tok == '?':
                acumul += '?'
            frases.append(acumul)
            acumul = []
        elif umisc.trim(tok) != '':
            acumul.append(tok)

    if len(acumul) > 0:
        frases.append(acumul)
    print 'Data.len:', len(dt), ',result:', len(frases)
    return frases


def process_data(lay, data, onto, purpose, usr, th = 0):
    rts = prepare_data(data)
    print 'prepare data(process_data):', len(rts), ',DT:LEN:', len(data)
    sentences = []
    u = lay.get_ontology(rts, 'object')
    for us in u:
        sentences.append(us)

    ctree = onto
    krt = ctree.process(sentences, purpose, usr)
    return krt


def pre_process_data(lay, data, onto, purpose, usr, thread_id, array_return):
    print 'Start Identify->pre_process_data->prepare_data():'
    rts = prepare_data(data)
    print 'End Identify->pre_process_data->prepare_data(): Len(s):', len(rts)
    sentences = []
    u = lay.get_ontology(rts, 'object')
    print 'pre_process_data(lay.get_ontology->u):', u
    for us in u:
        sentences.append(us)

    array_return2 = sentences
    print 'array_return(1)->', len(array_return2)
    thread_id.finished = True
    return array_return2


class pref_prepare_data_st:

    def __init__self(self):
        self.data = None
        return

    def prepare_data(self, semicol):
        return prepare_data(self.data, semicol)


def pre_process_data2(lays, lays2, data, purpose, usr, thread_id, array_return):
    print 'Start Identify->pre_process_data->prepare_data():'
    semicols = []
    sentences = []
    print 'Start pre-process->get_ontology()'
    if True:
        idsk = 0
        ex_lays = []
        p_lay = None
        p_lay2 = None
        group_run = False
        for lay in lays:
            idsk += 1
            if lay != None:
                if lay.group:
                    group_run = True
            if p_lay == None:
                p_lay = lay
            elif p_lay2 == None:
                p_lay2 = lay
            else:
                ex_lays.append(lay)

        try:
            r_obj = pref_prepare_data_st()
            r_obj.data = data
            print 'lay->get_ontology () run.'
            if p_lay != None:
                print 'Run prepare_data.'
                rts = prepare_data(data)
                print 'dt:', rts
                print 'End Identify->pre_process_data->prepare_data(', p_lay, '): Len(s):', len(rts)
                print 'Process pre_ontology(', len(rts), ')'
                dtps = p_lay.get_pre_ontology(rts)
                print 'Process pre_ontology(', len(dtps), ').OK'
                print 'Start first level:', p_lay2.name, 'group:', group_run
                u = p_lay2.get_ontology(dtps, 'object', ex_lays, r_obj, group_run)
                print 'p_lay->get_ontology () OK.', u
                for us1 in u:
                    sentences.append(us1)

        except Exception as ess:
            print 'Error on get_ontology()', ess

        ex_lays2 = []
        p_lay2 = None
        idsk = 0
        mxlevel = 0
        group_run = False
        for lay2 in lays2:
            idsk += 1
            if lay2 != None:
                if lay2.group:
                    group_run = True
            if p_lay2 == None:
                p_lay2 = lay2
            else:
                ex_lays2.append(lay2)
            if lay != None:
                if lay2.runlevel > mxlevel:
                    mxlevel = lay2.runlevel

        print 'To-Get Ontology-3():mxlevel:', mxlevel
        for u_sent_onde in sentences:
            if u_sent_onde != None and p_lay2 != None:
                atual_runl = 0
                while atual_runl <= mxlevel:
                    print 'Start runlevel:', atual_runl, 'group:', group_run
                    p_lay2.get_ontology3(u_sent_onde, 'No_Def', ex_lays2, atual_runl, group_run)
                    atual_runl += 1

                if mxlevel > 0:
                    print 'Start runlevel:', atual_runl, 'group:', group_run
                    p_lay2.get_ontology3(u_sent_onde, 'No_Def', ex_lays2, -1, group_run)
                print 'end get_ontology-3()============================'

    array_return2 = sentences
    print 'array_return(2-)->', len(array_return2)
    thread_id.finished = True
    return [array_return2, purpose]


def resume_process_data(sentences, onto, purpose, usr):
    ctree = onto
    krt = ctree.process(sentences, purpose, usr)
    return krt


def resume_process_datac(sentences, onto, purpose, usr, relactionate):
    ctree = onto
    print 'Ontology.processC:', sentences
    krt = ctree.processc(sentences, purpose, usr, relactionate)
    return krt


def prepare_data_by_ask(lay, data, purpose, usr, allp):
    rts = prepare_data(data)
    sentences = []
    for rt in rts:
        u = lay.get_ontology(rt, 'object')
        if u != None:
            sentences.append(u)

    def get_topico_s(nr, lr):
        for d in nr.dt:
            tp = lr.get_topico(d)
            if tp != None:
                return True

        return False

    ctree = mdOntology.mdBaseOntology(purpose, usr)
    for sp in ctree.all_purpouses:
        allp.append(sp)

    for lays in sentences:
        for tp_CONSID in lays.topicos:
            nodes2 = ctree.nodesER[0].get_links('FACT')
            if len(nodes2) > 0:
                nodeER = nodes2[0].lr
                _tp = get_topico_s(tp_CONSID, nodeER)
                if not _tp:
                    nodeER.set_topico_nr(tp_CONSID)

    return ctree
# okay decompyling Identify.pyc 
# decompiled 1 files: 1 okay, 0 failed, 0 verify failed
# 2015.02.01 02:57:40 BRST
