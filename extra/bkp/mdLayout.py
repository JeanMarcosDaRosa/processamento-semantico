# 2015.02.01 03:05:02 BRST
# Embedded file name: /mindnet/stage1-model/mdLayout.py
"""
 ->composicao(object->caracts)
 ->dominio(influencia,efeito)
 ->classificar( classe de informacao )
 ->posse
 ->referencial(ponto referencial,elemento refernencial, etc... )
 ->comparacao
 ->qualitativo
 ->quantitativo
 ->importancia(valor,grau de importancia )
 ->intensidade(mais,menos,maior,menor,...)
 ->state
 ->referenciamento( ex:space, posicionamento )
 ->need
 ->way
 ->relacao
 ->do(actions->time,space,..) facts->historico
 ->construcao ( etapa,modelo )
 ->etapa
 ->modelo
 ->historico(facts->)
 ->fact acao+elementos+influenciadores+time(relacao de tempo em q ocorre )
 ->mean- exaplain de significado, linka nodes ontologicos com proposito de significado
 ->sintoma
 ->filtro(excessao,restricao)
 ->cruzamento
 ->controle
"""
import Fuzzy
import mdOntology
import mdNeural
import umisc
import sys
#sys.path.append('/home/public_html/Neural/Pyro')
#import Pyro.core
dump_all_state = True
dump_all_state2 = True
dump_all_state3 = True
dump_all_state5 = True
c_before = '0'
c_next = '1'
import logging
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('DYNAMIC-CODE-ENGINE')
RemoteL = False

def latinupper(string):
    return string.decode('latin-1').upper().encode('latin-1')


if RemoteL:
    Pyro.core.initClient()
    objectName = 'layoutBean'
    hostname = '213.136.81.102'
    port = '26'
    print 'Creating proxy for object', objectName, ' on ', hostname + ':' + port
    if port:
        URI = 'PYROLOC://' + hostname + ':' + port + '/' + objectName
    else:
        URI = 'PYROLOC://' + hostname + '/' + objectName
    print 'The URI is', URI
    proxy = Pyro.core.getProxyForURI(URI)
    
if not RemoteL:
    import pycassa
    from pycassa.pool import ConnectionPool
    from pycassa import index
    from pycassa.columnfamily import ColumnFamily
    pool2 = ConnectionPool('MINDNET', ['localhost:9160'], timeout=10000)
    tb_fuzzy = pycassa.ColumnFamily(pool2, 'fuzzy_store')
    tb_fz_store_pref = pycassa.ColumnFamily(pool2, 'fz_store_pref')
    tb_fz_store_defs = pycassa.ColumnFamily(pool2, 'fz_store_defs')
    tb_fz_store_sufix = pycassa.ColumnFamily(pool2, 'fz_store_sufix')
    tb_fz_store_refer = pycassa.ColumnFamily(pool2, 'fz_store_refer')
    tb_fz_arround_points = pycassa.ColumnFamily(pool2, 'fz_arround_points')
from operator import itemgetter, attrgetter

class mdLayout:
    """
      usado para implementar rastreamento de informacoes. contem todos os fuzzys e determina os dominio de cada um dentro dos dados coletados
    """

    def __init__(self, id, name):
        self.fzs = []
        self.id = id
        self.group = False
        if type(name) == type([]):
            self.name = name[0]
            self.runlevel = 0
            if len(name) > 1:
                self.runlevel = name[1]
            if len(name) > 2:
                self.group = name[2]
        else:
            self.name = name
            self.group = False
        self.ult_level = 0

    def set_fz(self, fznm, fzC, cmand, refer = [], force_position = False, arround = [], snret = [], direction = None, f_an = None, breaks = []):
        """ force_position(True) forca nao haver outros elementos entre dois fzC"""
        fz = Fuzzy.mdFuzzy()
        fz.name = fznm
        fz.set_fz(fzC, cmand, refer, force_position, arround, snret, direction, f_an, breaks)
        self.fzs.append(fz)

    def remote_f(self):
        print 'Getting remote-layout...'
        return proxy.get_layout(self.name, self.id)

    def get_remote_fz(self):
        rts = self.remote_f()
        for r in rts:
            fzname, sents, mandatory, referer, force_position, arround, sinap_result, direction, f_an = r
            self.set_fz(fzname, sents, mandatory, referer, force_position, arround, sinap_result, direction, f_an)

    def get_fuzzy(self):
        """ """
        affinity = self.name
        cl4 = index.create_index_expression(column_name='layout_onto', value=affinity)
        clausec = index.create_index_clause([cl4], count=1000000)
        resultSet2 = tb_fuzzy.get_indexed_slices(clausec)
        resultSet = []
        for ky, re in resultSet2:
            fzname = re[u'fzname']
            force_position = re[u'force_position']
            mandatory = re[u'mandatory']
            direction = re[u'direction']
            an = re[u'an']
            sq = int(re[u'sq'])
            resultSet.append([ky,
             fzname,
             force_position,
             mandatory,
             direction,
             an,
             sq])

        resultSet = sorted(resultSet, key=itemgetter(6), reverse=True)
        for results in resultSet:
            ky, fzname, force_position, mandatory, direction, an, sq = results
            force_position = umisc.trim(force_position).upper() == 'Y' or umisc.trim(force_position).upper() == 'S'
            mandatory = umisc.trim(mandatory).upper() == 'Y' or umisc.trim(mandatory).upper() == 'S'
            direction = umisc.trim(direction).upper()
            f_an = umisc.trim(an).upper()
            referer = []
            start_i = 0
            while True:
                start_i += 1
                try:
                    r1 = tb_fz_store_refer.get(fzname + '|' + str(start_i))
                except:
                    break

                referer.append(r1[u'refer'])

            has_break = False
            breaks = []
            arround = []
            DEFS = []
            sinap_result = []
            start_i = 0
            while True:
                start_i += 1
                try:
                    results2 = tb_fz_store_defs.get(fzname + '|' + str(start_i))
                except:
                    break

                returns = []
                vl_ret1 = results2[u'vl_ret']
                special_direct = results2[u'special_direct']
                if special_direct == None:
                    special_direct = ''
                if vl_ret1 == None:
                    vl_ret1 = ''
                tuples = []
                tmp = ''
                for s in vl_ret1:
                    if s == '[':
                        tmp = ''
                    elif s == ']':
                        tuples.append(tmp)
                        tmp = ''
                    else:
                        tmp += s

                for tup in tuples:
                    top = ''
                    sub = ''
                    sin = ''
                    tmp = ''
                    for s in tup:
                        if s == ',':
                            if top == '':
                                top = tmp
                                tmp = ''
                            elif sub == '':
                                sub = tmp
                                tmp = ''
                            else:
                                sin = tmp
                                tmp = ''
                        else:
                            tmp += s

                    if umisc.trim(tmp) != '':
                        sin = tmp
                    if umisc.trim(top) != '':
                        if top == 'break':
                            has_break = True
                        else:
                            returns.append([top,
                             sub,
                             sin,
                             special_direct])

                defs1 = results2[u'defs']
                ps = []
                if len(defs1) > 0:
                    tmp = ''
                    ind = 0
                    for d in defs1:
                        if d == ',':
                            if defs1[ind - 1] != '\\':
                                ps.append(tmp)
                                tmp = ''
                            elif d != '\\':
                                tmp += d
                        elif d != '\\':
                            tmp += d
                        ind += 1

                    if umisc.trim(tmp) != '':
                        ps.append(tmp)
                else:
                    ps.append('')
                if has_break:
                    for ss in ps:
                        breaks.append(ss)

                else:
                    DEFS.append([ps, returns])
                    sin_ret = results2[u'sin_ret']
                    if umisc.trim(sin_ret) != '':
                        sinap_result.append(sin_ret)

            PREF = []
            start_i = 0
            while True:
                start_i += 1
                try:
                    r1 = tb_fz_store_pref.get(fzname + '|' + str(start_i))
                except:
                    break

                pref = r1[u'pref']
                ps = []
                if len(pref) > 0:
                    tmp = ''
                    ind = 0
                    for d in pref:
                        if d == ',':
                            if pref[ind - 1] != '\\':
                                ps.append(tmp)
                                tmp = ''
                            else:
                                tmp += d
                        else:
                            tmp += d

                    if tmp != '':
                        ps.append(tmp)
                else:
                    ps.append('')
                PREF.append(ps)

            SUFX = []
            start_i = 0
            while True:
                start_i += 1
                try:
                    r1 = tb_fz_store_sufix.get(fzname + '|' + str(start_i))
                except:
                    break

                sufix = r1[u'sufix']
                ps = []
                if len(sufix) > 0:
                    tmp = ''
                    ind = 0
                    for d in sufix:
                        if d == ',':
                            if sufix[ind - 1] != '\\':
                                ps.append(tmp)
                                tmp = ''
                            else:
                                tmp += d
                        else:
                            tmp += d

                    if tmp != '':
                        ps.append(tmp)
                else:
                    ps.append('')
                SUFX.append(ps)

            ind = 0
            sents = []
            for cDF in DEFS:
                PR = ['']
                SF = [['']]
                if ind < len(PREF):
                    PR = PREF[ind]
                if ind < len(SUFX):
                    SF = SUFX
                DEF = cDF
                sent = [PR, DEF, SF]
                sents.append(sent)
                ind += 1

            self.set_fz(fzname, sents, mandatory, referer, force_position, arround, sinap_result, direction, f_an, breaks)

        return

    def prepare(self):
        if not RemoteL:
            self.get_fuzzy()
        else:
            self.get_remote_fz()

    def get_pre_ontology(self, dt):
        rts = []
        data_s = []
        lines_process = []
        indic_data = 0
        dtks = dt
        dtks2 = []
        while indic_data < len(dtks):
            data = dtks[indic_data]
            indic_data += 1
            positions = []
            indice = 0
            ps_mahor = -1
            indcs = 0
            processed_f = False
            ps_mahor = 0
            lndata = len(data)
            while indice < lndata:
                dts = data[indice]
                indcs += 1
                if indice < ps_mahor:
                    indice += 1
                    if indice == lndata:
                        dtks2.append(data)
                    continue
                for f in self.fzs:
                    try:
                        pr2c = f.process(dts, data, indice)
                    except Exception as e:
                        print 'Error call fz(1):', f.name, 'err:', e
                        continue

                    for pr in pr2c:
                        if len(pr) > 0:
                            ulpos = pr[2]
                            if pr[0] == 'stop':
                                new_d = data[indcs:]
                                ds = data[:indcs - 1]
                                data = new_d
                                indice = 0
                                ps_mahor = 0
                                indcs = 1
                                dtks2.append(ds)
                                processed_f = True
                            if pr[0] == 'stops':
                                new_d = data[indcs:]
                                ds = data[:indcs - 1]
                                data = new_d
                                indice = 0
                                ps_mahor = 0
                                indcs = 1
                                ds.insert(0, '$[start]')
                                dtks2.append(ds)
                                processed_f = True
                            if pr[0] == 'stope':
                                new_d = data[indcs:]
                                ds = data[:indcs - 1]
                                data = new_d
                                indice = 0
                                ps_mahor = 0
                                indcs = 1
                                ds.append('$[end]')
                                dtks2.append(ds)
                                processed_f = True
                            if pr[0] == 'stopse':
                                new_d = data[indcs:]
                                ds = data[:indcs - 1]
                                data = new_d
                                indice = 0
                                ps_mahor = 0
                                indcs = 1
                                ds.insert(0, '$[start]')
                                ds.append('$[end]')
                                dtks2.append(ds)
                                processed_f = True
                            if ulpos > -1:
                                ps_mahor = ulpos

                indice += 1
                lndata = len(data)
                if indice == lndata:
                    dtks2.append(data)

            if not processed_f:
                dtks2.append(data)
            print 'dtks2:', dtks2

        return dtks2

    def get_ontology(self, dt, default, extra_laysC = [], r_obj = None, group_run = False):
        rts = []
        data_s = []
        lines_process = []
        indic_data = 0
        dtks = dt
        while indic_data < len(dtks):
            data = dtks[indic_data]
            indic_data += 1
            positions = []
            indice = 0
            ps_mahor = -1
            print 'Process line:', indic_data, ',len:', len(data)
            if dump_all_state2:
                print 'DT:', data
            indcs = 0
            bl_all = False
            fnd_one = False
            c_accept = False
            lays_found_c = []
            for dts in data:
                indcs += 1
                if indice < ps_mahor:
                    indice += 1
                    continue
                positions_c = []
                fnd_ss = False
                extra_lays = []
                extra_lays.append(self)
                for a in extra_laysC:
                    extra_lays.append(a)

                if not fnd_ss:
                    indice_lay = 0
                    size_layts = len(extra_lays)
                    while indice_lay < size_layts:
                        lay_s = extra_lays[indice_lay]
                        if bl_all:
                            break
                        if fnd_ss:
                            break
                        for f in lay_s.fzs:
                            if indice < ps_mahor:
                                break
                            if bl_all:
                                break
                            try:
                                pr2c = f.process(dts, data, indice)
                            except Exception as e:
                                print 'Error call fuzzy(1):', f.name, '[', e, ']'
                                fnd_one = False
                                continue

                            if len(pr2c) > 0:
                                fnd_one = True
                                if lay_s not in lays_found_c:
                                    lays_found_c.append(lay_s)
                            man = f.mandatory
                            pr = None
                            try:
                                kindc2 = 0
                                for pr in pr2c:
                                    if f.mandatory:
                                        man = f.mandatory_by_return(kindc2)
                                    kindc2 += 1
                                    direction = f.direction
                                    if len(pr) > 0:
                                        fnd_ss = True
                                        ulpos = pr[2]
                                        refer = pr[3]
                                        sn_ret = pr[4]
                                        if len(pr) > 6:
                                            if pr[6] != None:
                                                if pr[6] != '':
                                                    direction = pr[6]
                                        ind2 = indice
                                        if len(pr) > 5:
                                            if pr[5] > -1:
                                                positions.append([pr[:2],
                                                 pr[5],
                                                 [],
                                                 man,
                                                 refer,
                                                 sn_ret,
                                                 direction,
                                                 f.name])
                                                ind2 = pr[5] + 1
                                            else:
                                                positions.append([pr[:2],
                                                 ind2,
                                                 [],
                                                 man,
                                                 refer,
                                                 sn_ret,
                                                 direction,
                                                 f.name])
                                        else:
                                            positions.append([pr[:2],
                                             ind2,
                                             [],
                                             man,
                                             refer,
                                             sn_ret,
                                             direction,
                                             f.name])
                                        if ulpos > -1:
                                            ps_mahor = ulpos

                            except Exception as e:
                                print ('Error get fuzzy(2): --- Error:', e)
                                for prc in pr2c:
                                    print 'PR-N:', prc

                                print 'PR_AT:', pr
                                log.exception('-----------------------')

                        if not group_run:
                            indice_lay += 1
                        elif fnd_one:
                            c_accept = True
                            indice_lay += 1
                        else:
                            break

                indice += 1

            if len(positions) > 0:
                lines_process.append([positions,
                 data,
                 c_accept,
                 extra_lays,
                 lays_found_c])
            positions = []

        line_ind = 0
        line_ind2 = 0
        print 'Fz-Gravity-Positons:', len(lines_process)
        ind_pg = 0
        for positions, data, c_accept, extra_lays2, lays_found_c2 in lines_process:
            ind_pg += 1
            ('Process page:',
             ind_pg,
             ' of:',
             len(lines_process))
            if len(positions) == 0:
                continue
            if group_run:
                print 'Layout resume:', c_accept, 'extra_lays:', len(extra_lays2), 'lays_found_c:', len(lays_found_c2)
                if not c_accept or len(extra_lays2) != len(lays_found_c2):
                    continue
            line_ind += 1
            line_ind2 += 1
            if line_ind2 >= 200:
                line_ind2 = 0
            lay = mdNeural.mdLayer()
            menor_pos = -1
            for ps in positions:
                ps2 = ps[1]
                if ps2 < menor_pos or menor_pos == -1:
                    menor_pos = ps2

            if menor_pos > -1:
                menor_pos -= 1
            if menor_pos > -1 or len(positions) == 0:
                if len(data) > 0:
                    p1 = [default, default]
                    p2 = [p1,
                     0,
                     [],
                     True,
                     [],
                     [],
                     'R']
                    positions.insert(0, p2)
            if len(positions) > 0:
                rts.append(lay)
                indi = 0
                ind = 0
                pd = positions[ind]
                pd_valid = pd
                if not pd[3]:
                    p1 = [default, [default]]
                    p2 = [p1,
                     0,
                     [],
                     True,
                     [],
                     [],
                     'R',
                     None]
                    positions.insert(0, p2)
                    pd_valid = positions[0]
                direction = positions[ind][6]
                indice_dt_r = -1
                for d in data:
                    indice_dt_r += 1
                    if len(positions) > ind + 1:
                        direction = positions[ind][6]
                        ant_k = positions[ind + 1][1]
                        if indi == positions[ind + 1][1]:
                            ind += 1
                            if len(positions) > ind + 1:
                                while ant_k == positions[ind + 1][1]:
                                    ind += 1
                                    if len(positions) <= ind + 1:
                                        break

                            pd = positions[ind]
                            if not pd[3]:
                                pd = positions[ind - 1]
                            else:
                                pd_valid = pd
                            direction = pd_valid[6]
                            if direction.upper() == 'L' or direction.upper() == 'A':
                                ind2 = ind
                                if ind2 > 0:
                                    pd2 = positions[ind2]
                                    if pd2[3]:
                                        indexks = -1
                                        for d1 in data:
                                            indexks += 1
                                            if indice_dt_r - 1 <= indexks:
                                                break
                                            pd2[2].append(d1)

                        else:
                            if not pd[3]:
                                pd = pd_valid
                            if direction.upper() == 'L' or direction.upper() == 'A':
                                ind2 = ind + 1
                                while ind2 < len(positions):
                                    pd2 = positions[ind2]
                                    if pd2[3]:
                                        pd2[2].append(d)
                                        break
                                    ind2 += 1

                            if direction.upper() == 'R' or direction.upper() == 'A':
                                pd[2].append(d)
                    else:
                        if not pd[3]:
                            pd = pd_valid
                        if direction.upper() == 'L' or direction.upper() == 'A':
                            ind2 = ind + 1
                            while ind2 < len(positions):
                                pd2 = positions[ind2]
                                if pd2[3]:
                                    pd2[2].append(d)
                                    break
                                ind2 += 1

                        if direction.upper() == 'R' or direction.upper() == 'A':
                            if indice_dt_r > pd[1]:
                                pd[2].append(d)
                    indi += 1

                def check_novable(it):
                    return True

                center_p = None
                ps_exclude = []
                post_ref = 0
                for p in positions:
                    if p in ps_exclude:
                        continue
                    found_top = False
                    refer = p[4]
                    if len(refer) > 0:
                        print 'With:Refers:', refers
                        open_cycles = []
                        for s in refer:
                            center_p = lay.get_last_topico(s)
                            if center_p == None:
                                inds = post_ref - 1
                                while inds > 0:
                                    ps2 = positions[inds]
                                    inds -= 1
                                    if ps2[0][0].upper() == s.upper():
                                        ps_exclude.append(ps2)
                                        center_p = lay.set_topico(ps2[0][0])
                                        found_top = True
                                        if len(ps2[2]) > 0:
                                            for n1 in ps2[2]:
                                                if check_novable(n1):
                                                    nr = lay.set_nr(n1)
                                                    if len(ps2[5]) > 0:
                                                        for sns_1 in ps2[5]:
                                                            center_p.connect_to(lay, nr, sns_1)

                                                    else:
                                                        center_p.connect_to(lay, nr, ps2[0][0])

                                        nrC = lay.set_nr(p[0][0])
                                        center_p.connect_to(lay, nrC, 'Relaction-oper-opcode')
                                        if len(p[2]) > 0:
                                            for n1 in p[2]:
                                                if check_novable(n1):
                                                    nr = lay.set_nr(n1)
                                                    if len(p[5]) > 0:
                                                        for sns_1 in p[5]:
                                                            nrC.connect_to(lay, nr, sns_1)

                                                    else:
                                                        nrC.connect_to(lay, nr, p[0][0])

                                        break

                            else:
                                found_top = True
                                nrC = lay.set_nr(p[0][0])
                                center_p.connect_to(lay, nrC, 'Relaction-oper-opcode')
                                if len(p[2]) > 0:
                                    for n1 in p[2]:
                                        if check_novable(n1):
                                            nr = lay.set_nr(n1)
                                            if len(p[5]) > 0:
                                                for sns_1 in p[5]:
                                                    nrC.connect_to(lay, nr, sns_1)

                                            else:
                                                nrC.connect_to(lay, nr, p[0][0])

                        continue
                    elif umisc.trim(p[0][0]) != '' and umisc.trim(p[0][0]) != '\n':
                        center_p = lay.set_topico(p[0][0])
                        for d2 in p[0][1]:
                            if type(p[0][1]) == type([]):
                                if umisc.trim(d2) != '':
                                    nr = lay.set_nr(d2)
                                    for sns_1 in p[5]:
                                        center_p.connect_to(lay, nr, sns_1)

                        found_top = True
                    if len(p[2]) > 0:
                        for n1 in p[2]:
                            if check_novable(n1):
                                if center_p != None:
                                    nr = lay.set_nr(n1)
                                    if len(p[5]) > 0:
                                        sns_1 = p[5]
                                        if type(sns_1) == type(''):
                                            center_p.connect_to(lay, nr, sns_1)
                                        else:
                                            for sn1 in sns_1:
                                                center_p.connect_to(lay, nr, sn1)

                                    else:
                                        center_p.connect_to(lay, nr, p[0][0])

                    post_ref += 1

            tpsc = lay.get_topico('object')
            tpsc2 = lay.get_topico('identificador')
            fnds = False
            if tpsc2 == None:
                if len(rts) > 1:
                    lay2 = rts[len(rts) - 2]
                    tpsc2 = lay2.get_topico('identificador')
                    if tpsc2 != None:
                        lay.set_topico_nr_fir(tpsc2)
                        fnds = True
            if tpsc == None and not fnds:
                if len(rts) > 1:
                    lay2 = rts[len(rts) - 2]
                    tpsc2 = lay2.get_topico('object')
                    if tpsc2 != None:
                        lay.set_topico_nr_fir(tpsc2)
                        fnds = True

        for r in rts:
            print 'RTS:------------'
            for s in r.topicos:
                print 'TOP:', s.dt
                print '++++++++++++++++++'
                for s1 in s.sinapses:
                    print s1.nr.dt

                print '++++++++++++++++++'

            print 'RTS(END):------------'

        return rts

    def get_ontology3(self, lay, default, extra_laysC = [], crunlevel = 0, group_run = False):
        dt1 = []
        if group_run:
            print 'Found group!!!!(2) '
        crunlevel2 = crunlevel
        if crunlevel > -1:
            self.ult_level = crunlevel
        if len(self.fzs) == 0:
            return []
        else:

            def get_level(n, arr, atu = 0):
                print 'Run-LEVEL:(', atu, ')->', self.ult_level
                arr2 = []
                if n != atu:
                    for d in arr:
                        for s in d.sinapses:
                            arr2.append(s.nr)

                    atu += 1
                    if len(arr2) > 0:
                        k = get_level(n, arr2, atu)
                        return k
                    else:
                        return []
                else:
                    for d in arr:
                        for s in d.sinapses:
                            arr2.append([s.nr, d])

                    return arr2

            if dump_all_state:
                print 'initial state:=========================crunlevel:', crunlevel
                lay.dump_layer()
                print 'END====================================='
            if crunlevel == -1:
                if self.ult_level in (0, -1):
                    self.ult_level = 0
                crunlevel = self.ult_level
            if dump_all_state:
                print 'Enter runlevel:', crunlevel
            if crunlevel == 0:
                for dts2 in lay.topicos:
                    for s in dts2.sinapses:
                        for d in s.nr.dt:
                            dt1.append([d, s.nr, dts2])

            else:
                arr2 = []
                for ac in lay.topicos:
                    arr2.append(ac)

                arr2c = get_level(crunlevel, arr2)
                for s, fath in arr2c:
                    for d in s.dt:
                        dt1.append([d, s, fath])

            dt = [dt1]
            extra_lays = []
            extra_lays.append(self)
            for a in extra_laysC:
                extra_lays.append(a)

            if dump_all_state3:
                print 'Run layouts:==='
                for lac in extra_lays:
                    print 'l:', lac.name

            rts = []
            lines_process = []
            if dump_all_state2:
                print 'start:data:', len(dt)
            for data in dt:
                if len(data) == 0:
                    continue
                indice = 0
                ps_mahor = -1
                if dump_all_state2:
                    print 'Process line:', len(data)
                positions = []
                rem_silent = []
                for dts in data:
                    if indice < ps_mahor:
                        indice += 1
                        continue
                    if dump_all_state2:
                        print 'Run index:', indice
                    fnd_ss = False
                    if not fnd_ss:
                        indice_lay = 0
                        size_layts = len(extra_lays)
                        while indice_lay < size_layts:
                            lay_s = extra_lays[indice_lay]
                            indice_lay += 1
                            if fnd_ss:
                                break
                            if lay_s.runlevel != crunlevel2:
                                if dump_all_state5:
                                    print 'skip layout(', lay_s.name, '):levellayout:', lay_s.runlevel, ',runlevel:', crunlevel2
                                continue
                            if dump_all_state5:
                                print 'Run layout:', lay_s.name, 'of:', len(extra_lays), 'dts:', dts
                            for f in lay_s.fzs:
                                if dump_all_state3:
                                    print 'RunFZ:', f.name
                                    print 'consid(', indice, ').dts---'
                                try:
                                    pr2c, pos_nr = f.process2(dts, data, indice)
                                except Exception as e:
                                    print 'Error call fuzzy(2):', f.name, '[', e, ']'
                                    continue

                                man = f.mandatory
                                if dump_all_state2:
                                    print 'consid(', indice, ').dts:', dts, dts[1].dt, dts[2].dt, '-->', pr2c
                                pr = None
                                try:
                                    kindc2 = 0
                                    indic_nr = -1
                                    for pr in pr2c:
                                        indic_nr += 1
                                        neuron_ref, neuron_ref_ow = pos_nr[indic_nr]
                                        orig_kindc2 = False
                                        direction = f.direction
                                        if f.mandatory:
                                            man = f.mandatory_by_return(kindc2)
                                            if not man:
                                                orig_kindc2 = True
                                        kindc2 += 1
                                        if len(pr) > 0:
                                            fnd_ss = True
                                            ulpos = pr[2]
                                            refer = pr[3]
                                            sn_ret = pr[4]
                                            if len(pr) > 6:
                                                man = False
                                                pr[1] = []
                                            ack = dts[1]
                                            ack_ow = dts[2]
                                            ack = neuron_ref
                                            ack_ow = neuron_ref_ow
                                            if len(pr) > 5:
                                                if pr[5] > -1:
                                                    positions.append([pr[:2],
                                                     pr[5],
                                                     [],
                                                     man,
                                                     refer,
                                                     sn_ret,
                                                     direction,
                                                     ack,
                                                     ack_ow,
                                                     orig_kindc2,
                                                     dts])
                                                    ind2 = pr[5] + 1
                                                else:
                                                    positions.append([pr[:2],
                                                     indice,
                                                     [],
                                                     man,
                                                     refer,
                                                     sn_ret,
                                                     direction,
                                                     ack,
                                                     ack_ow,
                                                     orig_kindc2,
                                                     dts])
                                            else:
                                                positions.append([pr[:2],
                                                 indice,
                                                 [],
                                                 man,
                                                 refer,
                                                 sn_ret,
                                                 direction,
                                                 ack,
                                                 ack_ow,
                                                 orig_kindc2,
                                                 dts])
                                            if dump_all_state2:
                                                print 'n.positions:', positions[len(positions) - 1]
                                            if ulpos > -1:
                                                ps_mahor = ulpos

                                except Exception as e:
                                    print 'Error get fuzzy:', pr2c, pr

                    indice += 1

                if len(positions) > 0:
                    lines_process.append([positions, data])
                positions = []

            line_ind = 0
            line_ind2 = 0
            if dump_all_state2:
                print 'run:len(', len(lines_process), ')'
            ind_pg = 0
            for positions, data in lines_process:
                ind_pg += 1
                if len(positions) == 0:
                    continue
                line_ind += 1
                line_ind2 += 1
                if line_ind2 >= 200:
                    line_ind2 = 0
                menor_pos = -1
                for ps in positions:
                    ps2 = ps[1]
                    if ps2 < menor_pos or menor_pos == -1:
                        menor_pos = ps2

                if menor_pos > -1:
                    menor_pos -= 1
                if menor_pos > -1 or len(positions) == 0:
                    if len(data) > 0:
                        p1 = [default, default]
                        p2 = [p1,
                         0,
                         [],
                         True,
                         [],
                         [],
                         'R',
                         None,
                         None,
                         False,
                         None]
                        positions.insert(0, p2)
                if len(positions) > 0:
                    rts.append(lay)
                    indi = 0
                    ind = 0
                    pd = positions[ind]
                    pd_valid = pd
                    if not pd[3]:
                        p1 = [default, [default]]
                        p2 = [p1,
                         0,
                         [],
                         True,
                         [],
                         [],
                         'R',
                         None,
                         None,
                         False,
                         None]
                        positions.insert(0, p2)
                        pd_valid = positions[0]
                    not_valids = []
                    direction = positions[ind][6]
                    indice_dt_r = -1
                    for d in data:
                        indice_dt_r += 1
                        if len(positions) > ind + 1:
                            direction = positions[ind][6]
                            ant_k = positions[ind + 1][1]
                            if indi == positions[ind + 1][1]:
                                ind += 1
                                if len(positions) > ind + 1:
                                    while ant_k == positions[ind + 1][1]:
                                        ind += 1
                                        if len(positions) <= ind + 1:
                                            break

                                pd = positions[ind]
                                if not pd[3]:
                                    not_valids.append([indice_dt_r, pd])
                                    pd = positions[ind - 1]
                                else:
                                    pd_valid = pd
                            else:
                                if not pd[3]:
                                    not_valids.append([indice_dt_r, pd])
                                    pd = pd_valid
                                if direction.upper() == 'L' or direction.upper() == 'A':
                                    ind2 = ind + 1
                                    while ind2 < len(positions):
                                        pd2 = positions[ind2]
                                        not_valids.append([ind2, pd2])
                                        if pd2[3]:
                                            pd2[2].append(d)
                                            break
                                        ind2 += 1

                                if direction.upper() == 'R' or direction.upper() == 'A':
                                    pd[2].append(d)
                        else:
                            if not pd[3]:
                                not_valids.append([indice_dt_r, pd])
                                pd = pd_valid
                            if direction.upper() == 'L' or direction.upper() == 'A':
                                ind2 = ind + 1
                                while ind2 < len(positions):
                                    pd2 = positions[ind2]
                                    if not pd2[3]:
                                        not_valids.append([ind2, pd2])
                                    if pd2[3]:
                                        pd2[2].append(d)
                                        break
                                    ind2 += 1

                            if direction.upper() == 'R' or direction.upper() == 'A':
                                if indice_dt_r > pd[1]:
                                    pd[2].append(d)
                        indi += 1

                    def check_novable(it):
                        if it in (',', 'e'):
                            return False
                        return True

                    center_p = None
                    ps_exclude = []
                    post_ref = 0
                    not_valids2 = []
                    for nd, pose in not_valids:
                        dtk = data[nd]
                        ts = None
                        if len(pose) > 0:
                            if len(pose[0]) > 0:
                                ts = pose[0][0]
                        not_valids2.append([dtk[1],
                         dtk[2],
                         ts,
                         pose])

                    not_valids = not_valids2
                    kfound = False
                    to_R = []
                    has_no_def = False
                    for p in positions:
                        if p[0] == 'No_Def':
                            has_no_def = True

                    ult_center_p = None
                    for p in positions:
                        if p in ps_exclude:
                            continue
                        if p[9]:
                            continue
                        found_top = False
                        center_p = p[7]
                        center_p_a = p[8]
                        fnd_ck = False
                        if center_p != None:
                            center_p_dt_ = center_p.dt
                        if len(p[2]) > 0 and center_p_a != None:
                            for dk in center_p_a.sinapses:
                                if dk.nr == center_p:
                                    pass

                        if center_p_a == None:
                            continue
                        if p[10] != None:
                            dtk = data[p[1]]
                            for dk in center_p_a.sinapses:
                                if dk.nr == dtk[1]:
                                    to_R.append(dk)

                            for dk1 in p[0][1]:
                                if dump_all_state2:
                                    print 'test.dk1:', p
                                if '$$data$$' in dk1:
                                    fnd_ck = True
                                    if dump_all_state2:
                                        print 'FOUND.K:', p

                            if dump_all_state2:
                                print 'P:',
                                print 'CENTER_P:', dtk[1].dt, dtk, '-->', center_p_a.dt
                        found_top = True
                        if dump_all_state2:
                            print 'TPS(z):', center_p.dt, center_p_a.dt, ',POSE:', p
                        top = center_p_a
                        found_tpc = False
                        for sn in top.sinapses:
                            if sn.nr == center_p:
                                found_tpc = True

                        if not found_tpc and fnd_ck:
                            if ult_center_p != None:
                                ult_center_p.connect_to(ult_center_p, dtk[1], 'Composicao')
                        at_center_p = center_p
                        if dump_all_state2:
                            print 'star p:, ', center_p.dt
                        for sn in top.sinapses:
                            if sn.nr == center_p:
                                if True:
                                    nrc = sn.nr
                                    indc = -1
                                    for c in top.sinapses:
                                        indc += 1
                                        if c == sn:
                                            break

                                    top.sinapses.remove(sn)
                                    if dump_all_state2:
                                        print 'star p:, ', center_p.dt, center_p
                                        if p[0][0] == 'event-data-time':
                                            for p2 in p:
                                                if p2.__class__.__name__ == 'mdNeuron':
                                                    print 'P.dt:', p2.dt

                                    if not fnd_ck:
                                        nr = top.owner.set_nr(p[0][0])
                                        nrc.dt = []
                                    else:
                                        nr = top.owner.set_nr(p[0][0])
                                        nrc.dt = dtk[1].dt
                                    for sec in p[0][1]:
                                        nrc.dt.append(sec)
                                        if dump_all_state2:
                                            print 'create second nr:', nrc.dt

                                    if dump_all_state2:
                                        print 'star p2:, ', center_p.dt, center_p
                                    if dump_all_state2:
                                        print 'Create nr:', nr.dt, nr, '-->', nrc.dt, ' ,tpz:', at_center_p.dt, ',PP:', p[0][1]
                                    top.connect_toi(top.owner, nr, 'Composicao', indc)
                                    if len(nrc.dt) > 0:
                                        nr.connect_to(nr, nrc, 'Composicao')
                                    center_p2 = nr
                                    ult_center_p = center_p2
                                    if len(p[2]) > 0:
                                        for n1 in p[2]:
                                            if check_novable(n1):
                                                if center_p2 != None:
                                                    for dk in top.sinapses:
                                                        if dk.nr == n1[1]:
                                                            to_R.append(dk)

                                                    nr = lay.set_nr_nr(n1[1])
                                                    if len(p[5]) > 0:
                                                        sns_1 = p[5]
                                                        if type(sns_1) == type(''):
                                                            center_p2.connect_to(lay, nr, sns_1)
                                                        else:
                                                            for sn1 in sns_1:
                                                                center_p2.connect_to(lay, nr, sn1)

                                                    else:
                                                        center_p2.connect_to(lay, nr, p[0][0])

                        for dk in to_R:
                            try:
                                top.sinapses.remove(dk)
                            except:
                                pass

                        post_ref += 1

                    for nd in not_valids:
                        ex, top, pose, pose2 = nd
                        if pose == 'No_Def':
                            continue
                        p = pose2
                        if dump_all_state2:
                            print 'TPS:>>>>>>>>>>>>>>>>>>(2)', ex.dt, top.dt, ',POSE:', pose2
                        to_R = []
                        for sn in top.sinapses:
                            if sn.nr == ex:
                                if True:
                                    nrc = sn.nr
                                    indc = -1
                                    for c in top.sinapses:
                                        indc += 1
                                        if c == sn:
                                            break

                                    top.sinapses.remove(sn)
                                    nr = top.owner.set_nr(pose)
                                    top.connect_toi(top.owner, nr, 'Composicao', indc)
                                    nr.connect_to(nr, nrc, 'Composicao')
                                    center_p = nr
                                    if dump_all_state2:
                                        print 'P2:', p
                                    if len(p[2]) > 0:
                                        for n1 in p[2]:
                                            if check_novable(n1):
                                                if center_p != None:
                                                    for dk in top.sinapses:
                                                        if dk.nr == n1[1]:
                                                            to_R.append(dk)

                                                    nr = lay.set_nr_nr(n1[1])
                                                    if len(p[5]) > 0:
                                                        sns_1 = p[5]
                                                        if type(sns_1) == type(''):
                                                            center_p.connect_to(lay, nr, sns_1)
                                                        else:
                                                            for sn1 in sns_1:
                                                                center_p.connect_to(lay, nr, sn1)

                                                    else:
                                                        center_p.connect_to(lay, nr, p[0][0])

                                break

                        for dk in to_R:
                            try:
                                top.sinapses.remove(dk)
                            except:
                                pass

            if dump_all_state:
                if len(rts) <= 0:
                    print 'Dont have final state========================'
                for la in rts:
                    print 'final state:========================='
                    la.dump_layer()
                    print 'initial state=========================='

            return rts

    def get_ontology2(self, lay, default):
        dt = []
        if len(self.fzs) == 0:
            return []
        for dts2 in lay.topicos:
            for s in dts2.sinapses:
                for d in s.nr.dt:
                    dt.append([d, dts2])

        positions = []
        indice = 0
        ps_mahor = -1
        for data in dt:
            if data[0] in (',', '.', '?', '|', ';', '*', '-'):
                continue
            if indice < ps_mahor:
                indice += 1
                continue
            for f in self.fzs:
                pr2c = f.process2(data, dt, indice)
                if len(pr2c) > 0:
                    print 'RUNFS:', f.name, pr2c
                for pr in pr2c:
                    man = f.mandatory
                    direction = f.direction
                    if len(pr) > 0:
                        ulpos = pr[2]
                        refer = pr[3]
                        sn_ret = pr[4]
                        positions.append([pr[:2],
                         indice,
                         [],
                         man,
                         refer,
                         sn_ret,
                         direction,
                         data[1]])
                        if ulpos > -1:
                            ps_mahor = ulpos
                        break

            indice += 1

        menor_pos = -1
        for ps in positions:
            ps2 = ps[1]
            if ps2 < menor_pos or menor_pos == -1:
                menor_pos = ps2

        if menor_pos > -1:
            menor_pos -= 1
        if menor_pos > -1 or len(positions) == 0:
            if len(dt) > 0:
                p1 = [default, default]
                p2 = [p1,
                 0,
                 [],
                 True,
                 [],
                 [],
                 'R',
                 None]
                positions.insert(0, p2)
        if len(positions) > 0:
            indi = 0
            ind = 0
            pd = positions[ind]
            direction = positions[ind][6]
            for d in dt:
                if len(positions) > ind + 1:
                    direction = positions[ind][6]
                    if indi == positions[ind + 1][1]:
                        ind += 1
                        pd = positions[ind]
                        if not pd[3]:
                            pd = positions[ind - 1]
                    else:
                        if direction.upper() == 'L' or direction.upper() == 'A':
                            pd2 = positions[ind + 1]
                            pd2[2].append(d)
                        if direction.upper() == 'R' or direction.upper() == 'A':
                            pd[2].append(d)
                else:
                    if direction.upper() == 'L' or direction.upper() == 'A':
                        pd2 = positions[ind + 1]
                        pd2[2].append(d)
                    if direction.upper() == 'R' or direction.upper() == 'A':
                        pd[2].append(d)
                indi += 1

            def check_novable(it):
                if it in (',', 'e'):
                    return False
                return True

            center_p = None
            ps_exclude = []
            post_ref = 0
            for p in positions:
                if p in ps_exclude:
                    continue
                center_p = p[7]
                refer = p[3]
                if len(refer) > 0:
                    open_cycles = []
                    for s in refer:
                        if center_p == None:
                            center_p = lay.get_last_topico(s)
                        if center_p == None:
                            inds = post_ref - 1
                            while inds > 0:
                                ps2 = positions[inds]
                                inds -= 1
                                if ps2[0][0].upper() == s.upper():
                                    ps_exclude.append(ps2)
                                    center_p = lay.set_nr(ps2[0][0])
                                    p[7].connect_to(lay, center_p, 'Relaction-oper-opcode')
                                    if len(ps2[2]) > 0:
                                        for n1 in ps2[2]:
                                            if check_novable(n1):
                                                nr = lay.set_nr(n1)
                                                if len(ps2[5]) > 0:
                                                    for sns_1 in ps2[5]:
                                                        center_p.connect_to(lay, nr, sns_1)

                                                else:
                                                    center_p.connect_to(lay, nr, ps2[0][0])

                                    nrC = lay.set_nr(p[0][0])
                                    center_p.connect_to(lay, nrC, 'Relaction-oper-opcode')
                                    if len(p[2]) > 0:
                                        for n1 in p[2]:
                                            if check_novable(n1):
                                                nr = lay.set_nr(n1)
                                                if len(p[5]) > 0:
                                                    for sns_1 in p[5]:
                                                        nrC.connect_to(lay, nr, sns_1)

                                                else:
                                                    nrC.connect_to(lay, nr, p[0][0])

                                    break

                        else:
                            nrC = lay.set_nr(p[0][0])
                            center_p.connect_to(lay, nrC, 'Relaction-oper-opcode')
                            if len(p[2]) > 0:
                                for n1 in p[2]:
                                    if check_novable(n1):
                                        nr = lay.set_nr(n1)
                                        if len(p[5]) > 0:
                                            for sns_1 in p[5]:
                                                nrC.connect_to(lay, nr, sns_1)

                                        else:
                                            nrC.connect_to(lay, nr, p[0][0])

                    continue
                elif p[7] != None:
                    center_p = lay.set_nr(p[0][0])
                    p[7].connect_to(lay, center_p, 'Relaction-oper-opcode')
                else:
                    center_p = lay.set_topico(p[0][0])
                if len(p[2]) > 0:
                    for n1 in p[2]:
                        if check_novable(n1):
                            if center_p != None:
                                nr = lay.set_nr(n1)
                                if len(p[5]) > 0:
                                    for sns_1 in p[5]:
                                        center_p.connect_to(lay, nr, sns_1)

                                else:
                                    center_p.connect_to(lay, nr, p[0][0])
                            else:
                                center_p = lay.set_nr(n1)
                                p[7].connect_to(lay, center_p, 'Relaction-oper-opcode')

                else:
                    n1c = p[0]
                    first = True
                    for n1 in n1c:
                        if first:
                            first = False
                            continue
                        for ks in n1:
                            if check_novable(ks):
                                if center_p != None:
                                    nr = lay.set_nr(ks)
                                    if len(p[5]) > 0:
                                        for sns_1 in p[5]:
                                            center_p.connect_to(lay, nr, sns_1)

                                    else:
                                        center_p.connect_to(lay, nr, p[0][0])
                                else:
                                    center_p = lay.set_nr(n1)
                                    p[7].connect_to(lay, center_p, 'Relaction-oper-opcode')

                post_ref += 1

            return lay
        else:
            return
