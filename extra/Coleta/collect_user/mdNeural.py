#encoding: latin-1

import os
import umisc
import logging
from StringIO import StringIO
import datetime
import time, datetime
import mdFuzzy
import mdTb
from operator import itemgetter, attrgetter

import pycassa
from pycassa.pool import ConnectionPool
from pycassa import index
from pycassa.columnfamily import ColumnFamily

import gradiente as grad1

from datetime import datetime

def start_db(pool2):
  mdTb.start_db(pool2)

 
kstermo=None


type_coll='0' # 0->get_object,1->get_object2



logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('DYNAMIC-CODE-ENGINE->mdNeural')


self_usr=''

ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)
#formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")self.logHandler.setFormatter(formatter)

log.addHandler(logHandler) 
log.addHandler(ch) 

#===--------------------------------------
def collect_objs_orig( i,usr,mdlayer):
 objs_r=[]
 objs_r2=[] 
 objs_r22=[]
 if mdlayer.tb == 1:  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(i))
  cl5 = index.create_index_expression(column_name='cntk', value='0',op=index.IndexOperator.GTE)
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   ido6=int(resultsC[u'cntk'])
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r2.append([ido,ido2,ido3,ido4,ido5,ido6])  
  #============== 
 objs_r22=sorted(objs_r2, key=itemgetter(5), reverse=False) 
 objs_r2=[]
 for [ido,ido2,ido3,ido4,ido5,ido6] in objs_r22:
    objs_r.append([ido,ido2,ido3,ido4,ido5])
 return objs_r 
#===--------------------------------------
def collect_objs_orig_obj2( i,usr,mdlayer):
 objs_r=[]
 if mdlayer.tb ==2 : # 2 -> semantic_object3        
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])
 elif mdlayer.tb == 3: # 3 -> semantic_object3_1_4:
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction31.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])
 return objs_r 
#===--------------------------------------
def collect_objs_dest( i,usr,mdlayer):
 objs_r=[]
 if mdlayer.tb == 1:
  objs_r2=[] 
  cl4 = index.create_index_expression(column_name='obj_dest', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_orig']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   ido6=int(resultsC[u'cntk'])
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   if ido6  == None:
    ido6=''
   #===
   objs_r2.append([ido,ido2,ido3,ido4,ido5,ido6])  
  objs_r22=sorted(objs_r2, key=itemgetter(5), reverse=False) 
  for [ido,ido2,ido3,ido4,ido5,ido6] in objs_r22:
    objs_r.append([ido,ido2,ido3,ido4,ido5])   
  #================================================ 
 elif mdlayer.tb == 2: # semantic_object3
  cl4 = index.create_index_expression(column_name='obj_dest', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_orig']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])   
 elif mdlayer.tb == 3: # semantic_object3_1_4
  cl4 = index.create_index_expression(column_name='obj_dest', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction31.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_orig']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])   
 return objs_r 
#===--------------------------------------
def collect_objs_dest_obj2( i,usr,mdlayer):
 objs_r=[]
 if mdlayer.tb == 2:
  cl4 = index.create_index_expression(column_name='obj_dest', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_orig']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])   
 else:
  cl4 = index.create_index_expression(column_name='obj_dest', value=str(i))
  clausec = index.create_index_clause([cl4],count=1000000)
  resultSet=mdTb.tb_relaction31.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_orig']
   ido2=resultsC[u'opcode']
   ido3=resultsC[u'foco']
   ido4=resultsC[u'foco_d']
   ido5=resultsC[u'cond']
   #===
   if ido2  == None:
    ido2=''
   if ido3  == None:
    ido3=''
   if ido4  == None:
    ido4=''
   if ido5  == None:
    ido5=''
   #===
   objs_r.append([ido,ido2,ido3,ido4,ido5])   
 return objs_r 
#===--------------------------------------

def pr_par(s):
 rt=[]
 tmp=''
 for a in s:
  if a == ',':
   rt.append(tmp)
   tmp=''
  else:
   tmp+=a
 #=====================  
 return rt    

def collect_links(obj_principal2,i2,usr):
 obj_principal2.links=[]
 c1=collect_objs_orig(i2,usr,obj_principal2)
 if len(c1) == 0:
  c1=collect_objs_orig_obj2(i2,usr,obj_principal2)
 #==----------------------------------------
 for c in c1:
  obj_id=c[0]
  opc=c[1]
  foco=pr_par(c[2])
  foco_d=pr_par(c[3])
  cond=pr_par(c[4])  
  if obj_principal2.tb == 1:#semantic_object
   obj_k=get_object_by_data(obj_id,usr)
  elif obj_principal2.tb == 2:#semantic_object3
   obj_k=mdTb.get_object_by_data(obj_id,obj_id)
  elif obj_principal2.tb == 3:#semantic_object3_1_4
   antz=mdTb.Zeus_Mode
   mdTb.Zeus_Mode=False
   obj_k=mdTb.get_object_by_data2z(obj_id,obj_id,True)
   mdTb.Zeus_Mode=antz
  obj_principal2.set_link_ds(obj_k,opc,foco,foco_d,cond)
 

def collect_links2(obj_principal2,i2,usr):
 
 c1=collect_objs_dest(i2,usr,obj_principal2)
 if len(c1) == 0:
  c1=collect_objs_dest_obj2(i2,usr,obj_principal2)
 rt=[] 
 #==----------------------------------------
 for c in c1:
  obj_id=c[0]
  opc=c[1]
  foco=pr_par(c[2])
  foco_d=pr_par(c[3])
  cond=pr_par(c[4])  
  obj_k=get_object_by_data(obj_id,usr)
  #================
  mc=mdDynamicLayerLink(obj_k,opc,cond)
  mc.foco_o=foco
  mc.foco_d=foco_d
  rt.append(mc)
 
 return rt



import get_object
import get_object2 


 

def get_object_by_data2(obj,usr):
  if type_coll == '0':
   return get_object.get_object_by_data2(obj,usr)
  else: 
   return get_object2.get_object_by_data2(obj,usr)
 
def get_object_by_data29(obj,usr,max_purposes):
  if type_coll == '0':
   return get_object.get_object_by_data29(obj,usr,max_purposes)
  else: 
   return get_object2.get_object_by_data29(obj,usr,max_purposes)
 
 
def get_object_by_data23(obj,usr):
  if type_coll == '0':
   return get_object.get_object_by_data23(obj,usr)
  else: 
   return get_object2.get_object_by_data23(obj,usr)

  

def get_object_by_data22(obj,usr,plus):
  if type_coll == '0':
   return get_object.get_object_by_data22(obj,usr,plus)
  else: 
   return get_object2.get_object_by_data22(obj,usr,plus)

 

 
def get_object_by_data223(obj,usr,plus):
  if type_coll == '0':
   return get_object.get_object_by_data223(obj,usr,plus)
  else: 
   return get_object2.get_object_by_data223(obj,usr,plus)
  
 
 
def clear_obj(usr,name ):
  if type_coll == '0':
   return get_object.clear_obj(usr,name )
  else: 
   return get_object2.clear_obj(usr,name )

 
 
def post_object_by_data_es(layer,usr): 
  #if type_coll == '0':
  # return get_object.post_object_by_data_es(layer,usr)
  #else: 
  # return get_object2.post_object_by_data_es(layer,usr)
  return get_object2.post_object_by_data_es(layer,usr) 

def get_ontology_s2s(aliases,opcode,usr): 
  if type_coll == '0':
   return get_object.get_ontology_s2s(aliases,opcode,purposes,usr )
  else: 
   return get_object2.get_ontology_s2s(aliases,opcode,purposes,usr )
 
def get_ontology(aliases,purposes,usr ): #  considera os purposes nos filtros quando alias == None 
  if type_coll == '0':
   return get_object.get_ontology(aliases,purposes,usr )
  else: 
   return get_object2.get_ontology(aliases,purposes,usr )

  
 
 
 
def get_ontology_s(aliases,purposes,destinats,usr ): #  considera os purposes nos filtros quando alias == None 
  if type_coll == '0':
   return get_object.get_ontology_s(aliases,purposes,destinats,usr )
  else: 
   return get_object2.get_ontology_s(aliases,purposes,destinats,usr )
   
def get_ontology_s_p(aliases,purposes,destinats,usr ): #  considera os purposes nos filtros quando alias == None 
  return get_object2.get_ontology_s_p(aliases,purposes,destinats,usr )
  
 
 
def get_ontology_ponderate(aliases,min_purposes,max_purposes,usr,dfin=[],sinapses_consid=[] ): # min_purposes=mandatory, max_purposes=max ideal 
  if type_coll == '0':
   return get_object.get_ontology_ponderate(aliases,min_purposes,max_purposes,usr,dfin,sinapses_consid )
  else: 
   return get_object2.get_ontology_ponderate(aliases,min_purposes,max_purposes,usr,dfin,sinapses_consid )
  
 
 
def get_ontology2(aliases,purposes,usr ): #  considera os purposes nos filtros quando alias == None 
  if type_coll == '0':
   return get_object.get_ontology2(aliases,purposes,usr )
  else: 
   return get_object2.get_ontology2(aliases,purposes,usr )

   

def post_error(usr):

 sql = ' insert into  status_err ( USERNAME,MSG ) values( ? , ? ) ' 
 serr= lbuffer.getvalue ()   
 now = datetime.datetime.now ()
 dts=str(now) 
 serr=serr.replace('File','') 
 serr=serr.replace('\"/wamp/www/neural/mdNeural.py\",','' )
 serr=serr.replace('exec(code, locals(), locals())','')
 serr=serr.replace('line 112, in run_layout_parser','')
 serr=serr.replace('\"<string>\",','')
 serr='Dt:'+dts+' '+serr
 print '---------------------------------------'
 print serr
 try:
  conn.sqlX (sql,([usr,serr]))
 except Exception,e:
   print 'Erro post err:',e 
 print '---------------------------------------'

class CGlobalStack:
  def __init__(self):
   self.stack=[]
   self.proc_pg=None
   self.kstermo=kstermo
   self.tmp=[]
   self.memorize=[]


GlobalStack=CGlobalStack ()

   
#===================
def get_typ(obj,usr2):
  # 
  typ=0
  #=======================
  resultSet=mdTb.tb_py.get(obj)
  typ=int(resultSet[u'TYP'])
  return  typ
#=============================================== 

def clean_s_k(strc2):
   k=''
   if (type(strc2).__name__) == 'SapDB_LongReader':
     strc=strc2.read()
   else:  
     strc=''+strc2
   #===  
   for ss in strc:
     if ss.lower () in ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','z','y','w',
                       '0','1','2','3','4','5','6','7','8','9', 
                       '~','`','!','@','#','$','%','^','&','*','(',')','{','}','[',']','\'','"',':',';','/','?','<',',','>','.','\\','|','-','_','=','+',' ','\n']:     
      k+=(ss)
   return k
 
  
def prepare_search_customlayouts(layer_Start,purposes,dts,usr,relactionate):
 
 #====================================================================================================
 #chamar os codigos 
 codes_Result=[]
 for dt in purposes:
  #
  try:  
   resultSet=mdTb.tb_py_code.get(dt)
  except: 
   resultSet=[] 
  # 
  if True:
    results = resultSet
    typ=get_typ(dt,usr)
    o=clean_s_k(results[u'CODE'])
    code=(o)
    sr_int_cmd_param=dts
    relactional=relactionate
    startL=layer_Start
    usuario=usr
    GlobalStack.kstermo=kstermo
    stack=GlobalStack
    print 'call onto-layout:',dt,'.........................'
    if typ == 1: #executavel
     code+=' \n\nretorno_srt=run(sr_int_cmd_param,relactional,startL,usuario,stack)'
    else: # executa somente o codigo
     pass
    #================================== 
    try:
     exec(code, locals(), locals())
    except Exception,e:
     #print 'Exec Error:',e 
     log.exception( 'Exec Error:['+ dt +']') 
     #post_error(usr)
     print 'Code:\n',code
    if typ == 1: #executavel
     # adicionar ao codes_Result o retorno_srt(lines->[endereco,dados] ) 
     if retorno_srt != None:
      codes_Result.append( retorno_srt )
 #===================
 return codes_Result 

def get_object_by_data(obj,usr): 
 return mdTb.get_object_by_data2z_cod(obj,obj,True)

def get_object_by_data_o22(obj,usr,obj_caller): 
 is_rct=False
 if obj_caller != None:
   is_rct=obj_caller.rct
 if mdTb.Zeus_Mode or is_rct:
  lay=mdTb.get_object_by_data(obj_nm,'')   
 else: 
  try: 
   resultSet=[[obj,tb_object.get(obj)]]
  except:
   resultSet=[]  
  for ky,resultsC in resultSet: 
       obj_nm=results[u'objeto'] 
  #-----------
  lay=mdLayer ()
  if obj_nm == None: obj_nm=obj
  lay.name=obj_nm
  lay=mdTb.get_object_by_data(obj_nm,'')
  #-----------
 return lay   

 
def get_object_by_name(aliases,usr,caller=None): 
 tree_h=[]
 is_rct=False
 if caller != None:
   is_rct=caller.rct
 for alias in aliases:
  if mdTb.Zeus_Mode or is_rct:
     #alia=alias.upper ()
     alia=alias
     # 
     try:
      resultSet=[[ alia, mdTb.tb_object1.get(alia) ]] 
     except: continue  
     #print 'Get.obj.by.name:',alia,usr
     for ky,results in resultSet:
        i=ky
        #====
        avaliable_objs=[]
        #===--------------------------------------
        obj_principal=get_object_by_data(i,usr)
        tree_h.append(obj_principal)
        #===        
  else: 
     alia=alias
     rt=get_object_by_data_o22(alia,usr,caller)   
     tree_h.append(rt)
   
 return tree_h  


class mdNeuron:
 class mdSinapse:
   def __init__(self):   
    self.lr=None
    self.nr=None
    self.opcode=None
    self.dyn=False
 class mdDynSinapse(mdSinapse):
   def __init__(self):   
    self.dyn=True
 def clone_nr(self,nr):
   nrr=mdNeuron(nr.owner)
   nrr.dt=nr.dt
   nrr.uuid=nr.uuid
   nrr.Mandatory=nr.Mandatory
   nrr.cmandatory=nr.cmandatory
   for s in nr.sinapses:
    sd=self.mdSinapse ()
    sd.opcode=s.opcode
    sd.lr=s.lr
    sd.nr=self.clone_nr(s.nr)
    nrr.sinapses.append(sd)
   return nrr    
 def compare(self,nr):
  pass
 def get_all(self):
  return self.sinapses
 def __init__(self,_owner):
  self.dt=[]
  self.owner=_owner
  self.sinapses=[]
  self.dynsinap=[]
  self.reactive=True # se marcado com reactive, esta preparado para responder find-path e comparacoes
  self.Mandatory=True
  self.dyn=False
  self.uuid=0
  self.cmandatory=False
 def connect_toi(self,lr,nr,opcode,index):
  sn=self.mdSinapse ()
  sn.lr=lr
  sn.nr=nr
  sn.opcode=opcode
  have_found=False
  for s in self.sinapses:
   if s.nr == nr:
    have_found=True
    break
  if not have_found:
   self.sinapses.insert(index,sn)
 def connect_to(self,lr,nr,opcode):
  sn=self.mdSinapse ()
  sn.lr=lr
  sn.nr=nr
  sn.opcode=opcode
  have_found=False
  for s in self.sinapses:
   if s.nr == nr:
    have_found=True
    break
  if not have_found:
   self.sinapses.append(sn)
 #==========  
 def connect_to_dyn_c(self,lr,nr,opcode):
  sn=self.mdDynSinapse ()
  sn.lr=lr
  sn.nr=nr
  sn.opcode=opcode
  have_found=False
  for s in self.dynsinap:
   if s.nr == nr:
    have_found=True
    break
  if not have_found:
   self.dynsinap.append(sn)   
 #============  
 def connect_to_dyn(self,lr,nr,opcode):
  sn=self.mdDynSinapse ()
  sn.lr=lr
  sn.nr=nr
  sn.opcode=opcode
  have_found=False
  for s in self.sinapses:
   if s.nr == nr:
    have_found=True
    break
  if not have_found:
   self.sinapses.append(sn)
 #====================================================================

    
 def translate_dts(self,one,two):
   afnd=False
   one=one.clone_nr(one)
   nrr=one
   for d1 in one.dt:
      d2 = two.dt
      if d1.lower()=='$$all$$':
        nrr.dt=d2 
        afnd=True
        break
      
   indc=0
   for sn in one.sinapses:   
    try:   
      for sn2 in two.sinapses:
       one.sinapses[indc].nr=self.translate_dts( sn.nr,sn2.nr )
       break
    except Exception,e:
     print e,':', sn.opcode   
 
   if afnd: 
    print 'From :', one.dt, ',TO:',nrr.dt
    one=nrr
    
   return one 

 
 def mont_return_data2(self):
    ow=self
    if  True : 
        sn1=None
        for s in ow.dynsinap:
         if s.opcode =='Compare-DYN':          
          sn1=s.nr
          print 'mont_return_data2================'
          print 'Found:DynSinap:',s.opcode,', Ob:',ow.dt
          print 'Sn1:',sn1.dt
          print '================================='
          r= self.translate_dts(ow,sn1) 
          #ow.dynsinap.remove(s)
          return r
    return ow      
 
 def mont_return_data(self,ows,foco):
    ow=self
    print 'Mount->foco()',foco 
    if len(foco) == 0 : 
        sn1=None
        for s in ow.dynsinap:
         if s.opcode =='Compare-DYN':
          sn1=s.nr
          print 'Found:DynSinap:',s.opcode,', Ob:',ow.dt
          ow.dynsinap.remove(s)
          #break
          print 'Sn1:',sn1.dt
          return self.translate_dts(ow,sn1)
          #return ow
      
    #=  
    for s in ows:
     fnd=False
     for s2 in foco:
      for dt in s.dt:
       for dt2 in s2.nr.dt:
        if dt.upper () == dt2.upper () or dt2.upper == "$$ALL$$":
          fnd=True
          md1=s
          if  len( s2.nr.sinapses) <= 0 :  # se nao tem verif menor
           ow.connect_to(ow.owner,md1,s2.opcode)
          else: 
           fcs=[]
           for s3 in s2.nr.sinapses:
            fcs.append(s3)
           ow.mont_return_data(ows,fcs)
        break   
     if fnd:
       pass
     else:
       pass
    return ow 
     
class mdLayerLink:
  def __init__(self,lr,opcode,opcode_seg=[]):
    self.opcode=opcode
    self.lr=lr   
    self.opcode_seg=opcode_seg # usado para segundo opcode( ex. relactions(opcode)->determina relacao + tipo-relacao(opcode_Seg) ->define a relacao 
  def getCl(self):
    return self.__class__.__name__  
    
class mdDynamicLayerLink(mdLayerLink):
   def __init__(self,lr,opcode,opcode_seg=[]):
    mdLayerLink.__init__(self,lr,opcode,opcode_seg)
    self.foco_o=[]   
    self.foco_d=[]

class mdLayer: #mantem a ontologia do conhecimento montada
  def dump_layer_file(self):
   def spaces(n):
    r=''
    i=1
    while i<=n:
     r+=' ' 
     i+=1
    return r
   #===========================   
   fil=open("c:\\python24\\kk.txt","wr")
   def print_s(nr,lev=1):
    if lev > 1:
     fil.write(spaces(lev)+'DT:'+str(nr.dt)+','+str(nr)+'\n')
    if len(nr.sinapses) > 0 :
     fil.write( spaces(lev)+'sub-level:-----' +'\n')
     for d in nr.sinapses:
       print_s(d.nr,lev+1)
     fil.write( spaces(lev)+'sub-level------'+'\n' )
   #=====================
   fil.write( 'dump-layer:'+self.name+'\n' )
   for s in self.topicos:
    fil.write(  'TOP:'+str(s.dt)+','+str(s)+'\n' )
    print_s(s)
   fil.write( 'dump-layer--------------------------'+'\n' )
   fil.close()
  #=  
  def dump_layer(self):
   def spaces(n):
    r=''
    i=1
    while i<=n:
     r+=' ' 
     i+=1
    return r
   #===========================    
   def print_s(nr,lev=1):
    if lev > 1:
     print spaces(lev)+'DT:',nr.dt,nr
    if len(nr.sinapses) > 0 :
     print spaces(lev)+'sub-level:-----'
     for d in nr.sinapses:
       print_s(d.nr,lev+1)
     print spaces(lev)+'sub-level------'
   #=====================
   print 'dump-layer:',self.name
   for s in self.topicos:
    print  'TOP:',s.dt,s
    print_s(s)
   print 'dump-layer--------------------------'
   
  def s_compare_dt(self,nr,dt):
      for s in nr.dt:
       if dt.upper () == s.upper():
         return True
      return False   
  def s_post_object_by_data_es(self,layer,usr):
   return post_object_by_data_es(layer,usr)
  def s_get_ontology_s(self,aliases,purposes,destinats,usr):
   return get_ontology_s(aliases,purposes,destinats,usr)
  #== 
  def s_get_ontology_ponderate(self,aliases,min_purposes,max_purposes,usr ):
    return get_ontology_ponderate(aliases,min_purposes,max_purposes,usr )
  def parse_variances(self,ite):
    variances=self.variancy[1:]
    novar=self.novariancy
    #variances=[]
    #====================
    if ite in novar:
       return ite
    #=================
    #variances=['m>ns']
    for msub in variances:
     sub=msub.split('>')[0]
     pos=msub.split('>')[1]
     ln1=len(ite)
     ln2=len(sub)
     if ln2 < ln1:      
      ps=ite[-ln2:]
      if ps == sub:
       consider=ite[:-ln2]+pos 
       return consider
    return ite
  #==============================================
  def get_variancy_elements(self,ite):
        mn=ite 
        basec=mn.split('>')
        base=''
        lsk=len(basec)
        infk=0
        for b in basec:
          if base != '': base+='>'
          base+=b
          infk+=1
          if infk >= (lsk-1): break
        change=mn.split('>')[-1]
        tmpk=change.split('|')
        kfnd=False
        for mn2 in tmpk:
         mn=base+'>'+mn2
         rtc=self.parse_variances(mn2)
         #print 'parse variance:',mn2,',orig:',mn
         if rtc != mn2:
          #print 'parse next. variance:',rtc,',orig:',mn
          ite+='|'+rtc
        #===
        return ite
    
  def variancy_app(self,arr_it):
    #print 'start variancy:',arr_it
    indv=0
    for indc in arr_it:
       ret=self.get_variancy_elements(indc)
       #print 'found variances:',ret
       indc=ret
       arr_it[indv]=ret
       indv+=1
    #arr_it=['defs>interaction.get.action>viagem|viagens', 'defs>interaction.get.action>internacional|internacionais']
    print 'considere variances:',arr_it
    return arr_it 
  def s_get_ontology_ponderate_caracts(self,aliases,min_purposes,max_purposes,usr,purpose,top_considers=[] ,dfin=[],sinapses_consid=[]):
    min_purposes=self.variancy_app(min_purposes)
    print 'get_ontology_ponderate_Caracts.get_ontology_ponderate():',aliases,min_purposes,[],usr
    rts=get_ontology_ponderate(aliases,min_purposes,[],usr,dfin,sinapses_consid )
    rts_o=[]
    print 'Ponderate found:',rts
    for [mx,obj] in rts:
     print 'Ponderate min(',obj.name,'):',len(max_purposes)
     attr=[]      
     try:
      for cons in top_considers:  
            print 'Considere:',cons           
            pass 
     
      alss=obj.get_all()
      print 'Ponderate found:',len(alss)
      alls2=max_purposes
      fnds=0
      for nr_atu in alss:      
       found_k=False
       for n_ca in alls2:
        if umisc.trim(n_ca) == '':continue
        print 'Ponderate Get_ontology-start{',n_ca,'}'
        rets_onto=self.s_get_ontology_ponderate(n_ca,[],[],usr )
        print 'Ponderate Get_ontology{',n_ca,'},result:',rets_onto
        for [attr,o] in rets_onto:
          print 'Ponderate found obj:',o,o.name 
          rts_n_c=o.get_all()
          print 'Ponderate found obj->tops:',rts_n_c
          for n_c in rts_n_c:
           nrt_oc_comp=nr_atu.clone_nr(nr_atu)
           #===========
           for cons in top_considers:             
            if self.s_compare_dt(nrt_oc_comp,cons):
              nrt_oc_comp.dt=['$$all$$']
           #===========
           print 'Ponderate compare topico:',nrt_oc_comp.dt,n_c.dt            
           chk_i=self.compare_dt_depend(usr,purpose,nrt_oc_comp,n_c,[])
           if chk_i:
             fnds+=1
             found_k=True
             break
     except Exception,e:
      print 'Error pondete funcion:',e
      log.exception("======================")      
     print 'Ponderate min:',len(max_purposes),' ,found:',fnds
     if fnds >=  len(max_purposes):
      print 'Ponderate found collected:',obj
      rts_o.append([attr,obj])
      
    return rts_o 
     
  #==============================================
  def release_din_sinapse(self):
   def check_syns(n):
    for s in n.sinapses:
     if s.dyn:
      n.sinapses.remove(s)
   #usada p fazer o release das sinapses temporarias por process_inter_find_path
   for s in self.topicos:
    check_syns(s)
   for d in self.dyns:
    check_syns(d)
    self.topicos.remove(d)
    
    
  def release_filter(self):
   self.published=[]
  def add_filter(self,nr):
   self.published.append(nr)
  def do_filter(self,dts):
   def do_filter_ch(nr,dt):
       if len(dts) > 0 :
        tp=dts[0]
        f=False
        passi=[]
        for t in dts:
         if f:
          passi.aapend(t)
         f=True
        #==
        ch=nr.sinapses
        for c in ch:
         if self.s_compare_dt(c.nr,tp) or tp == '$$all$$':
          do_filter_ch(c.nr,passi)
       else:
         nr.owner.published.append(nr)
   if len(dts) > 0 :
    tp=dts[0]
    f=False
    passi=[]
    for t in dts:
     if f:
      passi.aapend(t)
     f=True
    #==
    ch=self.get_all ()
    for c in ch:
     if self.s_compare_dt(c,tp) or tp == '$$all$$':
      do_filter_ch(c,passi)
   
  def _get_ontology(self,aliases,purposes,usr):
   return get_ontology(aliases,purposes,usr)
   
  def compare_nr_Dt(self,dt,nr):
      for s in nr.dt:
       if dt.upper () == s.upper():
         return True
      return False          
  
  def get_all_f(self):
   rts_f=[]
   nrs_d_cmp=self.topicos
   for spr in nrs_d_cmp:
      if self.compare_nr_Dt( 'interface-res' ,  spr ) :
       for d in spr.sinapses:
          rts_f.append(spr)
   #====================================
   return rts_f 
    
  def get_all_no_interface(self):
   #===
   apub=[]
   apubs=[]
   ind_cmp=0
   nrs_d_cmp=self.topicos
   for spr in nrs_d_cmp:
      if self.compare_nr_Dt( 'interface' ,  spr ) or self.compare_nr_Dt( 'interface-res' ,  spr ) :
        for snc in spr.sinapses:
         for d in snc.nr.dt:
          apub.append(d)
   if len(apub) > 0 :
    for s in self.topicos:
     for ap in apub:
       if self.s2_compare_dt(s,ap):
         apubs.append(s)      
   #===
   if len(apubs) > 0 :
    aret=[]
    for s in self.topicos:
     fnds=False
     for d in apubs:
      if s == d:
        fnds=True
     if not fnds :
       aret.append(s)
    return aret       
   return []
  
  def get_all(self):   
   areturn=[]
   #===
   apub=[]
   amand=[]
   nrs_d_cmp=self.topicos
   ind_cmp=0
   self.published=[]
   #print 'get.all===='
   #self.dump_layer()
   #print '==========='
   for spr in nrs_d_cmp:
      if self.compare_nr_Dt( 'interface' ,  spr ) :
        for snc in spr.sinapses:
         for d in snc.nr.dt:
          apub.append(d)
      if self.compare_nr_Dt( 'mandator' ,  spr ) : # mandatory
        self.published.append(spr)
        for snc in spr.sinapses:
         for d in snc.nr.dt:
          amand.append(d)
        print 'Mandator:',amand  
   if len(apub) > 0 :
    for s in self.topicos:
      for apu in apub:
       if self.s2_compare_dt(s,apu):
         if s not in self.published:
          self.published.append(s)  
      #==========
      for a2 in amand:
        if self.s2_compare_dt(s,a2):
          s.cmandatory=True
   #===
   if len(self.published)>0:
     areturn = self.published
   elif len(self.topicos)>0:
     areturn = self.topicos   
   else:
     areturn = self.nrs
     
   sins=[]
   for s in areturn:
    f=False
    for d in s.dt:
     if d=='tstatic':
      #f=True
      pass
      #for d in s.sinapses:
      # sins.append(d.nr)
    if not f:
     sins.append(s)   
   return sins
   
   
  def get_all_raw(self,row_trans): 
   #row_trans=[]  
   areturn=[]
   #===
   apub=[]
   nrs_d_cmp=self.topicos
   ind_cmp=0
   for spr in nrs_d_cmp:
    fnd_tr=False
    for [s2,s3] in row_trans:
     #print 'row_Trans compare from:',s2.dt,',to:',s3.dt,'nr:',s2,',nr2:',spr
     if spr ==  s2:
      print 'row_Trans from:',s2.dt,',to:',s3.dt 
      fnd_tr=True
      break
    if fnd_tr:
     apub.append(s3)   
    else:    
      if self.compare_nr_Dt( 'interface' ,  spr ) :
        #for snc in spr.sinapses:
        #  apub.append(snc.nr)
        pass
      else:
        apub.append(spr)    
   #=====================================        
   if len(apub) > 0 :
     areturn = apub
   elif len(self.topicos)>0:
     areturn = self.topicos   
   else:
     areturn = self.nrs
     
   sins=[]
   for s in areturn:
    f=False
    for d in s.dt:
     if d=='tstatic':
      #f=True
      #for d in s.sinapses:
      # sins.append(d.nr)
      pass
    if not f:
     sins.append(s)   
   return sins   
   
  
  def get_all_static(self): 
   #row_trans=[]  
   areturn=[]
   #===
   return areturn
   #===
   apub=[]
   #=====================================        
   if len(apub) > 0 :
     areturn = apub
   elif len(self.topicos)>0:
     areturn = self.topicos   
   else:
     areturn = self.nrs
     
   sins=[]
   for s in areturn:
    f=False
    for d in s.dt:
     if d=='tstatic':
        sins.append(s) 
        break
   return sins     
  
  def put_publish(self,n):
   fnd=False
   for d in self.published:
    if n == d:
      fnd=True
   if not fnd:
    self.published.append(topico)   
  
  def set_foco(self,foco):
    for fc in foco:
     for n in self.topicos:
      for dt in n.dt:
       for f in fc.dt:
        if f.upper () == dt.upper():
         self.put_publish(n)      
  def __init__(self):
    self.nrs=[]
    self.topicos=[]
    self.name=''
    self.published=[]
    self.links=[]
    self.cache=[]   
    self.dyns=[]
    #self.e_layers_collect_s=[]
    self.tb=1 # 1->semantic_object, 2->semantic_object3, 3->semantic_object3_1_4
    self.rct=False
    self.variancy=[]
    self.novariancy=[]
  #== 
  def update_links(self):
   collect_links(self,self.name,self_usr)
  def get_Reverse_links(self):
   return collect_links2(self,self.name,self_usr)
  #== 
  def get_links_nm(self,opcode,dst):
   rts=[]
   if len(self.links   ) == 0 :
    update_links()
   for s in self.links:
    if s.opcode.upper () == opcode.upper () or opcode=='':
     fnd=False
     #----------------------
     parcial_foco=[]
     if la.getCl () == 'mdDynamicLayerLink':
       parcial_foco=la.foco_d 
     for d1 in dst:
      ifnd=False
      for p in parcial_foco:
       for d in dst.dt:
        for pd in p.dt:
         if d.upper () == pd.upper():
           ifnd=True
      if not fnd:
       for p in parcial_foco:
        d=la.name
        for pd in p.dt:
         if d.upper () == pd.upper():
           ifnd=True
     if len(parcial_foco) ==0:
       fnd=True
     elif ifnd:
       fnd=True     
     #----------------------
     if fnd:
      rts.append(s)
   return rts
  def get_links(self,opcode):
   rts=[]
   if len(self.links   ) == 0 :
    self.update_links()
   for s in self.links:
    if s.opcode.upper () == opcode.upper () or opcode=='':
     rts.append(s)
   return rts
  def get_links_raw(self):
   rts=[]
   if len(self.links   ) == 0 :
    self.update_links()
   for s in self.links:
     rts.append(s)
   return rts
  def get_links_o2(self,opcodes,nrfoco):
   rts=[]
   if len(self.links   ) == 0 :
    self.update_links()
   for opcode in opcodes:
    [link,topico,sinret]=opcode
    for s in self.links:
     fnd=False
     if s.opcode.upper () == link.upper () or link=='':
      tps=None
      if nrfoco != None:
       tps=nrfoco.owner.get_topico(topico)
      for o in s.foco_o:
       if (o == nrfoco or nrfoco == None) or tps == o:
         fnd=True
     if fnd: 
      opcodes.pop ()
      rts2=get_links_o2(opcodes,None) 
      if len(rts2 ) > 0 :
       for rt1 in rts2:
         rts.append(rt1)
      else:
       rts.append(s)
   return rts
  def get_links_o(self,opcode,foco,sin=None):
   rts=[]
   if len(self.links   ) == 0 :
    self.update_links()
   for s in self.links:
    fnd=False
    if s.opcode.upper () == opcode.upper () or opcode=='':
     for o in s.foco_o:
      if o == foco:
       kfnd=True
       if sin != None: 
         kfnd=False
         for sins_s in o.sinapses:
          if sins_s.opcode.upper () == sin.upper () :
            fnd=True
       if kfnd:
        fnd=True
    if fnd: 
     rts.append(s)
   return rts
  def set_link(self,lr,opcode,opcode_seg=[]):
    mc=mdLayerLink(lr,opcode,opcode_seg)
    self.links.append(mc)
  def set_link_ds(self,lr,opcode,foco_o=[],foco_d=[],opcode_seg=[]):
    mc=mdDynamicLayerLink(lr,opcode,opcode_seg)
    mc.foco_o=foco_o
    mc.foco_d=foco_d
    self.links.append(mc)
  def set_link_d(self,d):
    self.links.append(d)
  def get_topico_s(self,dt):
    ns=[]
    for n in self.topicos:
     for d in n.dt:
       if d.upper () == dt.upper():
         ns.append( n )
    return ns
  def get_topico(self,dt):
    for n in self.topicos:
     for d in n.dt:
       if d.upper () == dt.upper():
         return n
    return None
  def get_last_topico(self,dt):
    n2=None
    for n in self.topicos:
     for d in n.dt:
       if d.upper () == dt.upper():
         n2=n
    return n2
  def set_topico_aq(self,dts):
    if len(dts) > 0 :
     nr=mdNeuron(self)
     nr.dt.append(dts[0])
     self.topicos.append(nr)
     top= self.topicos[len(self.topicos)-1]
     f=True
     for ds in dts:
      if not f:
        top=self.set_nr_ch(top,ds,'Composicao')
      f=False
    return None 
  def set_topico(self,dt):
    nr=mdNeuron(self)
    nr.dt.append(dt)
    self.topicos.append(nr)
    return self.topicos[len(self.topicos)-1]
  def set_topico_a(self,dt):
    nr=mdNeuron(self)
    nr.dt=(dt)
    self.topicos.append(nr)
    return self.topicos[len(self.topicos)-1]
  def set_topico_nr(self,nr):
    fnd=False
    foco=None
    for s in self.topicos:
     if nr == s:
      fnd=True
      foco = s
      break
    if not fnd:  
     self.topicos.append(nr)
     return self.topicos[len(self.topicos)-1]
    else:
     return foco    
  def set_topico_nr_fir(self,nr):
    self.topicos.insert(0,nr)
    return self.topicos[len(self.topicos)-1]
  def set_nr_ch(self,top,dt,opcode):
    nr=mdNeuron(self)
    nr.dt.append(dt)
    top.connect_to(self,nr,opcode)
    return nr
  def set_nr_ch_a(self,top,dt,opcode):
    nr=mdNeuron(self)
    nr.dt=(dt)
    top.connect_to(self,nr,opcode)
    return nr
  def set_nr_ch_a2(self,top,ch,opcode):
    nr=ch
    top.connect_to(self,nr,opcode)
    return nr
  def set_nr(self,dt):
    nr=mdNeuron(self)
    nr.dt.append(dt)
    self.nrs.append(nr)
    return self.nrs[len(self.nrs)-1]
  def set_nr_arr(self,dt):
    nr=mdNeuron(self)
    nr.dt=(dt)
    self.nrs.append(nr)
    return self.nrs[len(self.nrs)-1]
  def set_nr_nr(self,nr):
    self.nrs.append(nr)
    return self.nrs[len(self.nrs)-1]
    
  def compare_dt(self,nr1,nr2):
   for d1 in nr1.dt:
    for d2 in nr2.dt:
      if d1.upper () ==  d2.upper():
        return True
   #=======================================
   for sn in nr1.sinapses:   
    try:   
     if sn.opcode.upper () in ['MEAN'] :
      for sn2 in nr2.sinapses:
       if sn2.opcode.upper () in ['MEAN'] :
        if self.compare_dt(sn.nr,sne.nr) :
         return True
    except Exception,e:
     print e,':', sn.opcode   
   return False     
  def get_raw_object_code(self,nm,usr):
     return get_object_by_name(nm,usr,self)
  def get_all_it(self,nm,usr):
     objc2=get_object_by_name([nm],usr,self)
     if objc2 == None: return []
     for objc in objc2:
      krt= objc.topicos
      if len(krt) > 0: return krt
     return []
     
  def expand_cache(self,dt):  
   def parse(dt):
    rt=[]
    tmp=''
    for s in dt:
      if s==' ' or s == ',' or s == ')' or s == '(' or s==';' or s == '{' or s == '}':
       rt.append(tmp)
       tmp=''
      else: tmp+=s
    if tmp!='': rt.append(tmp)
    return rt
   #========================    
   fnd=False
   for [sld,itens] in self.cache:
     if sld == dt:
      return itens
   dt=umisc.trim(dt)
   if dt.find(' ') > -1 :
    new_d=parse(dt)
    self.cache.append([dt,new_d])
    return new_d
      
   return []
   
  def compare_dt_depend1(self,usr,purpose,nr1,nr2,consider_items=[],debug=False):
   afnd=False
   for d1 in nr1.dt:
    for d2 in nr2.dt:
      expand=self.expand_cache(d2)
      if len(expand) > 0 :
       #print 'Compare:',d1.upper(),' with:',expand 
       #if d1.upper () ==  d2.upper () or d1.lower()=='$$all$$' or self.compare_data_h(d1,d2) :
       #if mdFuzzy.compare_n(d1,d2) > 0 or mdFuzzy.compare(d1,'$$all$$') > 0 or self.compare_data_h(d1,d2) :
       if mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0 or d1.lower()=='$$all$$' or self.compare_data_h(d1,d2) :
        afnd=True
        break
       for ex in expand:         
         #if d1.upper () ==  ex.upper () or d1.lower()=='$$all$$':
         #if mdFuzzy.compare_n(d1,ex) > 0 or mdFuzzy.compare(d1,'$$all$$') > 0 :
         if mdFuzzy.compare_n2(d1,ex,self.variancy,self.novariancy) > 0 or d1.lower()=='$$all$$':
           afnd=True
           break
       if afnd: break       
      #elif d1.upper () ==  d2.upper () or d1.lower()=='$$all$$':
      #elif mdFuzzy.compare_n(d1,d2) > 0 or mdFuzzy.compare(d1,'$$all$$') > 0 :
      elif mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0 or d1.lower()=='$$all$$' :
        #print d1.upper(),d2.upper(),len(nr1.sinapses),'....................................................................'
        afnd= True   
        break   
   return afnd
   
  def compare_Date_del(self,t_format,dt):
    #timestring = "2005-09-01 12:30:09"
    #time_format = "%Y-%m-%d %H:%M:%S"
    #datetime.datetime.fromtimestamp(time.mktime(time.strptime(mytime, time_format)))
    rt=False
    try:
     r=datetime.datetime.fromtimestamp(time.mktime(time.strptime(dt, t_format)))
     return True
    except:
     pass
    return False
  
   
  def compare_data_h(self,dt1,dt2):
   d1=dt1
   d2=dt2
   if d1.lower() == '#month':
     if d2.lower() in ['janeiro','fevereiro','mar�o','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro']:
       return True
   #===================
   if d1.lower() == '#day':
    try:
     if int(d2) in  [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,28,30,31]:
       return True     
    except: 
      pass
   #==   
   if d1.lower() == '#year':
    try:
      if int(d2) > 0 :
        return True
    except Exception,e:
     #print 'Err:Year:',e
     pass     
   #=========================  
   if d1.lower() == '#time':
    try:
      #def compare_Date_del(self,t_format,dt):
      #  timestring = "2005-09-01 12:30:09"
      #  time_format = "%Y-%m-%d %H:%M:%S"
      if self.compare_Date_del('%H:%M:%S',d2) :
        return True
    except:
     pass
   #=========================  
   if d1.lower() == '#date':
    try:
      #def compare_Date_del(self,t_format,dt):
      #  timestring = "2005-09-01 12:30:09"
      #  time_format = "%Y-%m-%d %H:%M:%S"
      if self.compare_Date_del('%Y-%m-%d',d2) :
        return True
    except:
     pass
   if d1.lower() == '#datetime':
    try:
      #def compare_Date_del(self,t_format,dt):
      #  timestring = "2005-09-01 12:30:09"
      #  time_format = "%Y-%m-%d %H:%M:%S"
      if self.compare_Date_del('%Y-%m-%d %H:%M:%S',d2) :
        return True
    except:
     pass
    #======================================= 
    return False  
   
  def compare_dt_depend(self,usr,purpose,nr1,nr2,consider_items=[],debug=False):
   afnd=False
   for d1 in nr1.dt:
    for d2 in nr2.dt:
      expand=self.expand_cache(d2)
      if d1.lower()=='dlink':
       lnks=nr2.owner.get_links(d2)
       if len(lnks) > 0 :
        afnd= True   
        break
      #==============================================
      #==============================================            
      if len(expand) > 0 :
       #print 'Compare:',d1.upper(),' with:',expand 
       #if d1.upper () ==  d2.upper () or d1.lower()=='$$all$$'  or self.compare_data_h(d1,d2) :
       #if mdFuzzy.compare_n(d1,d2) > 0 or mdFuzzy.compare(d1,'$$all$$') > 0 or self.compare_data_h(d1,d2) :
       if mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0 or d1.lower()=='$stack' or d1.lower()=='$$all$$' or self.compare_data_h(d1,d2) :
        afnd=True
        break
       for ex in expand:
         #if d1.upper () ==  ex.upper () or d1.lower()=='$$all$$':
         #if mdFuzzy.compare_n(d1,ex) > 0  or  mdFuzzy.compare(d1,'$$all$$') >0 :
         if mdFuzzy.compare_n2(d1,ex,self.variancy,self.novariancy) > 0  or  d1.lower()=='$$all$$' or d1.lower()=='$stack':
           afnd=True
           break
       if afnd: break      
      #elif d1.upper () ==  d2.upper () or d1.lower()=='$$all$$':
      #elif mdFuzzy.compare_n(d1,d2) > 0  or  mdFuzzy.compare(d1,'$$all$$') > 0 :
      elif mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0  or  d1.lower()=='$$all$$' or d1.lower()=='$stack' :
        #print d1.upper(),d2.upper(),len(nr1.sinapses),'....................................................................'
        afnd= True   
        break
   
   
   have_sn_app=True
   have_sn_app_allw=False
   #== 
   for sn in nr1.sinapses:   
    try:   
     if sn.opcode.upper () in ['MEAN'] :
      for sn2 in nr2.sinapses:
       if sn2.opcode.upper () in ['MEAN'] :
        if self.compare_dt(sn.nr,sne.nr) :
         return True
     else:
      for sn2 in nr2.sinapses:
       #if afnd:
       #  print 'Now-compare:',sn.nr.dt,sn2.nr.dt,len(sn.nr.sinapses),len(sn2.nr.sinapses)
       rt_sf=self.compare_dt_depend(usr,purpose,sn.nr,sn2.nr,consider_items,True)
       if len(sn.nr.sinapses)==0: # se nao tem mais sinapses filhas, considerar modalidade 'or'
         if rt_sf:
           have_sn_app_allw=True
       if not rt_sf :
         have_sn_app=False
       if have_sn_app_allw:
        have_sn_app=True
       #if afnd:
       #  print 'Now-compared:',sn.nr.dt,sn2.nr.dt,len(sn.nr.sinapses),len(sn2.nr.sinapses),' result:',have_sn_app
         
     #=================     
    except Exception,e:
     print e,':', sn.opcode   
   #===
   #if afnd:
   # print 'All-cmp:',have_sn_app,afnd
   
   if have_sn_app and afnd: 
    return True
   #===
   return False     
  #=====================================================   
  #=====================================================   
  #=====================================================   
  #=====================================================   
  #=====================================================   
  def compare_dt_dependf(self,usr,purpose,nr1,nr2,consider_items=[],debug=False):
   afnd=False
   res=[]
   for d1 in nr1.dt:
    for d2 in nr2.dt:
      expand=self.expand_cache(d2)
      if d1.lower()=='dlink':
       lnks=nr2.owner.get_links(d2)
       if len(lnks) > 0 :
        afnd= True   
        break
      #==============================================
      #==============================================            
      if len(expand) > 0 :
       #print 'Compare:',d1.upper(),' with:',expand 
       #if d1.upper () ==  d2.upper () or d1.lower()=='$$all$$'  or self.compare_data_h(d1,d2) :
       #if mdFuzzy.compare_n(d1,d2) > 0 or mdFuzzy.compare(d1,'$$all$$') > 0 or self.compare_data_h(d1,d2) :
       if mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0 or d1.lower()=='$stack' or d1.lower()=='$$all$$' or self.compare_data_h(d1,d2) :
        afnd=True
        break
       for ex in expand:
         #if d1.upper () ==  ex.upper () or d1.lower()=='$$all$$':
         #if mdFuzzy.compare_n(d1,ex) > 0  or  mdFuzzy.compare(d1,'$$all$$') >0 :
         if mdFuzzy.compare_n2(d1,ex,self.variancy,self.novariancy) > 0  or  d1.lower()=='$$all$$' or d1.lower()=='$stack':
           afnd=True
           break
       if afnd: break      
      #elif d1.upper () ==  d2.upper () or d1.lower()=='$$all$$':
      #elif mdFuzzy.compare_n(d1,d2) > 0  or  mdFuzzy.compare(d1,'$$all$$') > 0 :
      elif mdFuzzy.compare_n2(d1,d2,self.variancy,self.novariancy) > 0  or  d1.lower()=='$$all$$' or d1.lower()=='$stack' :
        #print d1.upper(),d2.upper(),len(nr1.sinapses),'....................................................................'
        afnd= True 
        break
   
   
   have_sn_app=True
   have_sn_app_allw=False
   #== 
   for sn in nr1.sinapses:   
    try:   
     if sn.opcode.upper () in ['MEAN'] :
      for sn2 in nr2.sinapses:
       if sn2.opcode.upper () in ['MEAN'] :
        if self.compare_dt(sn.nr,sne.nr) :
         return [True,[nr1,sn.nr]]
     else:
      for sn2 in nr2.sinapses:
       #if afnd:
       #  print 'Now-compare:',sn.nr.dt,sn2.nr.dt,len(sn.nr.sinapses),len(sn2.nr.sinapses)
       rt_sf=self.compare_dt_depend(usr,purpose,sn.nr,sn2.nr,consider_items,True)
       if len(sn.nr.sinapses)==0: # se nao tem mais sinapses filhas, considerar modalidade 'or'
         if rt_sf:
           have_sn_app_allw=True
       if not rt_sf :
         have_sn_app=False
       if have_sn_app_allw:
        res=[nr2,sn2.nr] 
        have_sn_app=True
       #if afnd:
       #  print 'Now-compared:',sn.nr.dt,sn2.nr.dt,len(sn.nr.sinapses),len(sn2.nr.sinapses),' result:',have_sn_app
         
     #=================     
    except Exception,e:
     print e,':', sn.opcode   
   #===
   #if afnd:
   # print 'All-cmp:',have_sn_app,afnd
   
   if have_sn_app and afnd: 
    return [True,res]
   #===
   return [False,[]] 
  #=====================================================   
  #=====================================================   
  #=====================================================   
  #=====================================================   
  #=====================================================   
  def compare_sub_dt(self,nr1,nr2):
   def compare_dt_s(self,nr1,nr2):
    for d1 in nr1.dt:
     for d2 in nr2.dt:
       if d1.upper () ==  d2.upper():
         return True
    for sn in nr1.sinapses:    
      for sn2 in nr2.sinapses:
        if self.compare_dt_s(sn.nr,sne.nr) :
         return True
    return False
   #=============================      
   for sn in nr1.sinapses:    
     for sn2 in nr2.sinapses:
       if self.compare_dt_s(sn.nr,sne.nr) :
        return True
   return False     

  def s2_compare_dt(self,nr,dt):
      for s in nr.dt:
       if dt.upper () == s.upper():
         return True
      return False          
   
  def get_find_path(self,layer,dt,usr,purpose,owner): #   
   def get_all_dt(lay,topico):
    rts=[]
    if True:
     d=lay.get_topico(topico)
     if d!=None:
        for rs in d.sinapses:
          for drs in rs.nr.dt:
            rts.append(drs)
    return rts        
      
   def get_all_dt_raw(lay):
    rts=[]
    for d in lay.topicos:
     #d=lay.get_topico(topico)
     if d!=None:
        for rs in d.sinapses:
          for drs in rs.nr.dt:
            rts.append(drs)
    return rts        
   
   def parse(dt):
    rt=[]
    tmp=''
    for s in dt:
      if s==' ' or s == ',' or s == ')' or s == '(' or s==';' or s == '{' or s == '}':
       rt.append(tmp)
       tmp=''
      else: tmp+=s
    if tmp!='': rt.append(tmp)
    return rt
   #========================    
   def parse1(dt):
    rt=[]
    tmp=''
    for s in dt:
      if s==' ' or s == ',' or s == ')' or s == '(' or s==';' or s == '{' or s == '}':
       rt.append(tmp)
       tmp=''
      else: tmp+=s
    if tmp!='': rt.append(tmp)
    rt2=[]
    for r in rt:
     rtsc=r.split('@') # object@dest-topico
     if len(rtsc) > 1 and len(rtsc) < 3:
       rt2.append(rtsc)
    return rt2
   #========================    
   def parse2(dt):
    rt=[]
    tmp=''
    for s in dt:
      if s==' ' or s == ',' or s == ')' or s == '(' or s==';' or s == '{' or s == '}':
       rt.append(tmp)
       tmp=''
      else: tmp+=s
    if tmp!='': rt.append(tmp)
    rt2=[]
    for r in rt:
     rtsc=r.split('@') # object@opcode-link@dest-topico
     if len(rtsc) > 2 and len(rtsc) < 4  :
       rt2.append(rtsc)
    return rt2
   #========================    
   def parse3(dt):
    rt=[]
    tmp=''
    for s in dt:
      if s==' ' or s == ',' or s == ')' or s == '(' or s==';' or s == '{' or s == '}':
       rt.append(tmp)
       tmp=''
      else: tmp+=s
    if tmp!='': rt.append(tmp)
    rt2=[]
    for r in rt:
     # // get_ontology_s(s[0],s[1],topico_read,usr ): #  considera os purposes nos filtros quando alias == None //get_ontology_s(aliases,purposes,destinats,usr )
     rtsc=r.split('@') # object-@topico-relevante-remote@topico-read(p/ competar dt)@dest-topico  
     if len(rtsc) > 3:
       if rtsc[0] == '?': rtsc[0] = None
       rt2.append(rtsc)
    return rt2
   #  
   print 'Get find-path for :',layer.name,'[',dt,']',' Owner:',owner.name
   #==
   layer.release_filter ()   
   nrs=layer.get_all ()
   if True:
    '''  em cada dt=[a,b,c,...] � um nome de um fact, onde vai ter os topicos implementados. nesses, procurar object->object implementador ou no cenario.objects.find-path=>nome dos objetos pelo apelido passado, ou cenario.objects.find-paths.defs -> definicoes direto no cenario  '''
    all_dt=parse1(dt)  
    all_dt2=parse2(dt) 
    all_dt3=parse3(dt) 
    print 'FdPath:',dt,all_dt
    facts_defs=[]
    lnks=owner.get_links('FACT')
    #print 'lnks:', len(lnks)
    search=[]
    search2=[]
    search3=[]
    if True:
     ln=layer
     for [al,dest] in all_dt:
       tps_a=layer.get_topico(dest)
       search.append( [tp_a,al] )
        #====
    if True:
     for [atp,adt] in all_dt2:
       search2.append( [atp,adt] )
    if True:
     for atp in all_dt3:
       search3.append( atp )
     
    if len(search2) > 0 : # procura por objetos linkados ao obj_orig
     print 'find-path search.2:',search2
     for [obj_orig,opcode_links,dest_topico] in search2:#
        lrse=get_ontology_s2s(obj_orig,opcode_links,usr) # procura por links
        for lr in lrse:
          tp=layer.get_topico(dest_topico)
          if tp != None:
           nr=layer.set_nr(lr.name)
           tp.connect_to(layer,nr,opcode) 
           owner.set_link(lr,opcode_links)
                           
                        
                        
    if len(search3) > 0 :
     print 'find-path search.3:',search3
     #{3}
     for sc in search3:
       #object-@topico-relevante-remote@topico-read(p/ competar dt)@dest-topico
       #
       topico=layer.get_topico(sc[3])
       topico_tag=layer.get_topico('tag') # se houver pedido de identificacao
       if topico_tag !=None:
         topico_tag.sinapses=[]
       #=======================================
       if topico == None : continue
       topico_read=sc[1]
       #lrs=get_ontology(d,purpose,usr)
       dts_conf=get_all_dt(layer,sc[2])
       ant_z=mdTb.Zeus_Mode=True
       lrs=get_ontology_s(sc[0],dts_conf,[topico_read],usr ) 
       #print 'Get.get_ontology_s_p():'
       mdTb.Zeus_Mode=False
       lrs_1=get_ontology_s(sc[0],dts_conf,[topico_read],usr ) 
       mdTb.Zeus_Mode=ant_z        
       #print 'find.get_obj():(',sc[0],dts_conf,[topico_read],usr,')->',lrs_1,'.',lrs
       for l in lrs_1:
         lrs.append(l)
       #====================================================
       fpaths=[]
       rts=[]
       indc=-1
       for lr in lrs:
         indc+=1
         fpaths=lr.get_all ()
         f_integra=False
         for f in fpaths:
          if self.s_compare_dt(f,'f-sub'):#substituto, remove o topico anterior
           f_integra=True
         rts=fpaths  
         '''
         print 'fpaths:',f_integra
         for p in rts:
           print '.dt:',p.dt
           for a in p.sinapses:
            print '.sin:',a.nr.dt
         print 'caller:',topico.dt
         '''
         #=============
         remote_tag=[]
         if len(rts):
           for p in rts:
            if self.compare_nr_Dt( 'tag' ,  p ) :
               if p not in remote_tag:
                remote_tag.append(p)                
         inserid_tag=False    
         sp1=None
         for t in rts:
            if not self.compare_nr_Dt( topico.dt[0] ,  t ) :continue
            # confere tag
            if not inserid_tag and topico_tag!=None:
              if len(remote_tag)==0:
                 if sp1==None:
                  #set_nr_ch(self,top,dt,opcode) 
                  sp1=topico_tag.owner.set_nr_ch(topico_tag,'tag','Composicao')
                 s2=sp1.owner.set_nr_ch(sp1,'NODEF','Comosicao')
                 print 'set.tag:',s2.dt
                 inserid_tag=True
              for s in remote_tag:
                for s1 in s.sinapses:
                 if sp1==None:
                  sp1=topico_tag.owner.set_nr_ch(topico_tag,'tag','Composicao')
                 sp1.connect_to(topico_tag.owner,s1.nr,'Composicao')
                 print 'set.tag:',s1.nr.dt
                 inserid_tag=True
            #
            for k in t.sinapses:
             if not f_integra:              
              #print 'tpc:',k.nr.dt
              if indc > 0 :
               topico.connect_to(topico.owner,k.nr,'Composicao'+str(indc))
              else:
               topico.connect_to(topico.owner,k.nr,'Composicao') 
              #for a in topico.sinapses:
              # print '.sin2:',a.nr.dt
             else: 
              layer.add_filter(k.nr) # foca apenas os substitutos
              
         #print 'dump.lr:',layer.dump_layer()                  
    if len(search) > 0 :
     print 'find-path search.1:',search
     for [tps,d] in search:  
       topico=layer.get_topico(tps)
       lrs=get_ontology(d,purpose,usr)
       if len(lrs) == 0: 
        # procurar no cenario na lista de objects de conhecimentos previos e globais
        for sclient in GlobalStack.stack:     
          #print 'cmp.fdp:',sclient.assinat.lower(),d.lower()        
          if sclient.assinat.lower()==d.lower(): # 
           for cen in sclient.cenarios:
            for defs in cen.defs_findp:
             #print 'cmp.fdp.run:',topico.dt,defs[0]       
             if self.s_compare_dt(topico,defs[0]) : # compara a implementacao    
                nrk=topico.owner.set_nr(defs[1])
                topico.connect_to(topico.owner,nrk,'Composicao') # linka ao resultado            
        #==        
        continue
        #==        
       fpaths=[]
       rts=[]
       for lr in lrs:
         fpaths=lr.get_all ()         
         f_integra=False
         for f in fpaths:
          if self.s_compare_dt(f,'f-sub'):#substituto, remove o topico anterior
           f_integra=True
         rts=fpaths  
         #=============
         for t in rts:
           if self.s_compare_dt(t,'composicao') or self.s_compare_dt(t,'definicao') or self.s_compare_dt(t,'composition') or self.s_compare_dt(t,'definition'):
            for k in t.sinapses:
             if not f_integra:
              topico.connect_to(topico.owner,k.nr,'Composicao')
             else: 
              layer.add_filter(k.nr) # foca apenas os substitutos
         
  def process_Call_RCT(self,data_c,ontology,usr,purpose,relactionate,have_collected):
   print 'Enter.to.call.links.CALL:',self.name
   nodes=self.get_links('CALL')
   for node_R in nodes:
     node=node_R.lr
     dests=[]
     #========
     nds=node.get_all ()
     generics=[]
     to_gen=False
     for n1 in nds:
      if self.s_compare_dt(n1,'destination'): 
        for d in n1.sinapses:
         if self.s_compare_dt(n1,'generic'):
          to_gen=True
         elif to_gen:
          for ds in d.dt:
           generics.append(ds)           
      
     if not to_gen:
      print 'Identify RCT:',node.name
      #node.e_layers_collect_s=self.e_layers_collect_s
      node.process_RCT(data_c,ontology,usr,purpose,relactionate,have_collected,True )
     else:
      # generic � um objeto que simboliza a chamada de outros, descrevendo em seus destinations os dests a serem procurads em rcts nao likadas, mas q implementem essas destinations    
      rcts=get_ontology(None,generics,usr)
      print 'Identify.2.0 RCT:',rcts
      for r in rcts:
        print 'Identify RCT.2:',r.name
        r.process_RCT(data_c,ontology,usr,purpose,relactionate,have_collected,True )    
   
  def es_compare_dt(self,nr,dt):
      for s in nr.dt:
       if dt.upper () == s.upper():
         return True
      return False          
    
  def compare_objs(self,identif,dest,foco,usr,purpose):
   def set_foco(lr,foco):
    for s in foco:
     for n in lr.topicos:
      if self.compare_nr_Dt(s,n):
       lr.published.append(n)
     for n in lr.nrs:
      if self.compare_nr_Dt(s,n):
       lr.published.append(n)
   rt=[]
   rt1=[]
   rt2=[]
   for id in identif:
    obj=get_ontology2(id,purpose,usr)
    for des in dest:
     obj2=get_ontology2(des,purpose,usr)
     if len(obj) > 0  and len(obj2) > 0 :
      if len(foco) > 0:
       set_foco(obj[0],foco)
       set_foco(obj2[0],foco)
      #
      [r,r2]=obj[0].process_raw_RCT(obj2,ontology,usr,purpose)
      if r!= None :
        for c in r.topicos:
         rt1.append(c)
        for c in r2.topicos:
         rt2.append(c)
      #=============
   if len(rt1) or len(rt2):
    rt=[rt1,rt2]
   return rt
  
  def process_behavior(self,layers_collect_s,layer,node,stack_proc,node_layer,qual1,qual2,return_top,number_oper,purpose,usr):
   import datetime as date2
   import time as time2
   # qual1->qualiticador que esta guardardo a informacao no modelo[o qual deve ser procurado para comparar os limits]
   # qual2->qualificador que esta guardando a informacao no layer a ser conferido   
   # return_top-> topico a ser gerado no node e no stack_proc se estiver dentro dos limites
   #1 - achar topicos contendo os limites,intervalo a ser conferido 
   # tp-> interval(direct,indirect): data,quantity,quality
   def compare_Date_del2(t_format,dt,ex=''):
       import datetime as date2
       import time as time2
       #timestring = "2005-09-01 12:30:09"
       #time_format = "%Y-%m-%d %H:%M:%S"
       #datetime.datetime.fromtimestamp(time.mktime(time.strptime(mytime, time_format)))
       rt=False
       try:
        if ex != '':     
          dt=dt.replace(ex,'')    
        r=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(dt, t_format)))
        return [True,r]
       except:
        pass
       return [False,None]    

      
   if True:
       lr_atu=layer
       #
       is_dt=False
       is_time=False
       is_number=False
       is_qualify=False
       for k2 in qual1:
         recp1=None
         format=''
         try:
           [r,recp1]= compare_Date_del2('%d-%m-%Y',k2) 
           if r:
            format= '%d-%m-%Y'
            is_dt=True  
           else:           
             [r,recp1]= compare_Date_del2('%Y-%m-%d',k2) 
             if r:
              format= '%Y-%m-%d'
              is_dt=True  
             else:
              [r,recp1]= compare_Date_del2('%Y/%m/%d',k2) 
              if r:  
               format= '%Y/%m/%d'
               is_dt=True  
              else:
               [r,recp1]= compare_Date_del2('%d/%m/%Y',k2) 
               if r:  
                 format= '%d/%m/%Y'
                 is_dt=True  
               else:
                 [r,recp1]=compare_Date_del2('%H:%M:%S',d2) 
                 if r:
                     is_time=True
                     format= '%H:%M:%S'
                 else:
                   [r,recp1]=compare_Date_del2('%H:%M',d2) 
                   if r:
                     is_time=True
                     format= '%H:%M'
                   else:
                     try:
                      recp1=float(k2) 
                      is_number=True
                     except:
                       is_qualify=True  
                       recp1=None
         except:  
            is_qualify=True    
            
         #=============================================================
         # tps_compare->contem os limites impostos para o intervalo
         ob=lr_atu
         if is_dt:
           fir=qual2[0]
           last=qual2[len(qual2)-1]
           # intermediarios
           try:             
             r1=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(fir, format)))
           except:
             return 
           #
           try:             
             r2=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(last, format)))
           except:
             return 
           #
           if recp1 >=r1 and recp1<= r2:
               nm = return_top
               ob.set_topico(nm)
               node.set_topico(nm)
               for [lr,r2] in layers_collect_s:
                 lr.set_topico(nm)
                 
           #
         elif is_time:  
           fir=tps_compare2[0]
           last=tps_compare2[len(tps_compare2)-1]
           # intermediarios
           try:             
             r1=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(fir, format)))
           except:
             return 
           #
           try:             
             r2=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(last, format)))
           except:
             return 
           #
           if recp1 >=r1 and recp1<= r2:
               nm = return_top
               ob.set_topico(nm)
               node.set_topico(nm)
               for [lr,r2] in layers_collect_s:
                 lr.set_topico(nm)
               
         elif is_number:
           fir=tps_compare2[0]
           last=tps_compare2[len(tps_compare2)-1]
           # intermediarios
           try:             
             r1=float(fir)
           except:
             return 
           #
           if number_oper != None and number_oper <> '':
             if number_oper=='maior':
               if recp1 >r1 :
                nm = return_top
                ob.set_topico(nm)
                node.set_topico(nm)
                for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
                
             elif number_oper=='menor':
               if recp1 < r1 :
                nm = return_top
                ob.set_topico(nm)
                node.set_topico(nm)
                for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
             elif number_oper=='maior-igual':
               if recp1 >= r1 :
                nm = return_top
                ob.set_topico(nm)
                node.set_topico(nm)
                for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
             elif number_oper=='menor-igual':
               if recp1 <= r1 :
                nm = return_top
                ob.set_topico(nm)
                node.set_topico(nm)
             elif number_oper=='igual':
               if recp1 == r1 :
                nm = return_top
                ob.set_topico(nm)
                node.set_topico(nm)
                for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
           #
           try:             
             r2=float(last)
           except:
             return 
           #  
           if recp1 >=r1 and recp1<= r2:
               nm = return_top
               ob.set_topico(nm)
               node.set_topico(nm)
               for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
               
         elif is_qualify:
           # tps_compare2-> conjunto de elementos delimitados aos possiveis elementos           
           if grad1.in_nc(recp1,tps_compare2): 
               nm = return_top
               ob.set_topico(nm)
               node.set_topico(nm)
               for [lr,r2] in layers_collect_s:
                  lr.set_topico(nm)
    
   
  def process_inter_find_path(self,opcode_condition):
   # processar os find-path por linklayer
   nodes=self.get_links('find-path')
   for n in nodes:
    foco_o= n.foco_o
    foco_d= n.foco_d
    opcode=n.opcode_seg
    #==========
    foco_origem=[]
    foco_destino=[]
    #==============
    #opcode = purpose
    #==============
    ps=False
    if opcode_condition == opcode or opcode_condition == "$$all$$" or opcode == "$$all$$":
     # $$all$$-> incondicional
     ps=True
    
    if not ps : return
    
    just_foco=False
    foco_inc=False
    if opcode == 'just-foco':
     just_foco=True
    elif opcode == 'foco-inc':
     foco_inc=True
    #=========================================================================================================================    
    foco_orig=False
    s1=n.get_all()    
    if len(foco_o) == 0 : # se nao tem foco obrigatorio, verificacao=True
     foco_orig=True
    #=================  
    ifound=0
    for s_c2 in foco_o:
     for s_c in s1:
      if self.compare_dt_depend1(s_c,s_c1) :
       ifound+=1
       foco_origem.append(s_c)
       break
    if ifound >=  len(s_c2):
     foco_orig=True
    #=========================================================================================================================    
    foco_dest=False
    s2=self.get_all()    
    if len(foco_d) == 0 : # se nao tem foco obrigatorio, verificacao=True
     foco_dest=True
    #=================  
    ifound=0
    for s_c2 in foco_o:
     for s_c in s2:
      if self.compare_dt_depend1(s_c,s_c1) :
       ifound+=1
       foco_destino.append(s_c)
       break
    if ifound >=  len(s_c2):
     foco_dest=True
     
    if foco_orig and foco_dest:
        # se obedecidos os focos, analisar os opcodes:
        if  len(foco_destino) > 0 :
          # coletar informacoes de cada foco, complementando o foco dest
          for d in foco_destino:  
           if len(foco_origem) > 0 :          
            for o in foco_origem:
             for s in o.sinapses:
              d.connect_to_dyn(d.owner,s.nr,'Composicao')           
           else:
            ak=n.get_all()
            for a in ak:
             d.connect_to_dyn(d.owner,a,'Composicao')           
           
        else:
          ak=n.get_all()
          for a in ak:
           fs1=False
           for o in foco_origem:
            if o==ak: 
             fs1=True
             break
           if not fs1:
            self.dyns.append(a)
            self.set_topico_nr(a)
                   
  def clone_layer(self):
   rt=mdLayer()
   rt.name=self.name   
   for s in self.topicos:
    nr2 = mdNeuron(rt)
    nr2=nr2.clone_nr(s)
    rt.set_topico_nr(nr2)
   return rt 
    
  #runask(usr,purpc,identif,type_S)   
  def runask(usr,purpose_vocabulario,code,typeS):
    codes=''
    for c in code:
     codes+=( c + ' ')
    cmd = 'python \wamp\www\neural\SemaIndexerStage2.py "'+usr+'" "'+codes+'" "'+purpose_vocabulario+'" "'+typeS+'" '
    os.system(cmd)
    
   
    
  def process_RCT(self,layers,ontology,usr,purpose,relactionate=False,have_collected=[],aj_c=False):        
   # ractionline
   # verbos a serem processados
   #  compare(with need/way/mean)
   #  behaviour(with need/way/mean)
   #  inferencia preditiva(causa-efeito)
   #  inferencia reativa( acao-reacao)    
   #  --------  detectar need/mean/way automatico
   # 
   #obs: get_find_path -> opera nas implementacoes de rct
   #     process_inter_find_path -> opera em objectos, tanto nas rct quanto no paremetro de input "layers", a cada chamada do get_all()
   '''
      formato:
       topico->findpath para cada fact
       links-layer -> cada fact
   '''   
   self.variancy=[]
   self.novariancy=[]
   def get_fct_links():
    rt=[]
    nodes=self.get_links('FACT')
    print 'Layers FACTS:',len(nodes)
    for n in nodes:
     n2=n.lr.clone_layer()
     n.lr=n2
     rt.append(n)
    return rt 
   adm_return=True
   adm_serialize=False
   start_adm_serialize=-1
   start_adm_serialize_ind=-1
   adm_serialize_ind=-1
   adm_serialize_cmp=[]
   adm_return_val=False
   adm_return_val_call=False
   adm_return_val_p=[]
   adm_return_val_p_call=[]
   self.rct=True
   print 'Start Call RCT:',self.name , ' ,len buff(' ,len(layers), ')' 
   print 'Cache:[',self.name,']'
   print 'Buffer:[',layers,']'
   indck=0
   for lc1 in layers:
    if type(lc1) == type([]):
      lc1=lc1[0] 
      layers[indck]=lc1
    print '.el:',lc1.name
    indck+=1
    #lc1.dump_layer()
   print '===================='
   print 'All-L:len:',len(layers),' ,def:',layers
   aj_return=[]   
   aj_param=[]
   if aj_c:
    for a in layers:
     aj_param.append(a)
   print 'RCT->facts(',self.name,'):'
   layers_collect=[]    
   layers_collect_s=[]
   layers2=[]
   has_found_d=False
   for layer_Atu_r in layers:
    print 'RUn layer-cd:================================================================================================================='
    #
    layer_Atu_r=layer_Atu_r.clone_layer()
    layers2.append(layer_Atu_r)
    #for lj in self.e_layers_collect_s:
    #  layers_collect_s.append(lj)
    rtp=mdLayer ()
    nore=mdLayer ()   
    filtered=[]
    dyned=[]
    nodes=get_fct_links()
    #===
    for ns in nodes:
      print '[',ns.lr.name,']'    
    topicos_fpath=self.topicos
    idcx=0
    nodek=[]
    _nds2=[]
    node_cmps=[]
    has_found_dn=True
    for node in nodes:
     dmand=False    
     #if node.lr.name =='process_cmp_data_fct':
     #  print 'llll'
     start_adm_serialize_ind=-1 
     start_adm_serialize=-1
     node=node.lr
     node=node.clone_layer()
     for fpt in topicos_fpath:
      for dtsc in  fpt.dt:
       if node.name.upper () == dtsc.upper():
        dts_fnd=[]
        for sns in fpt.sinapses:
         for dtsn in sns.nr.dt:
           dts_fnd.append(dtsn)
        for dts_fnd1 in dts_fnd:
         #==                dt,usr,purpose
         '''
         'vb_preditiva' # find-path para consideracoes 
         'vb_reactive'  # find-path para consideracoes 
         '''
         self.get_find_path(node,dts_fnd1,usr,purpose,self)#[topico,sinapse,link], retorna os definidores de find-path direto do banco
         node.process_inter_find_path(dts_fnd1)
         #get_all -> 
         
     #=========     
     node.process_inter_find_path(purpose)
     dyned.append(node)
     nds=node.get_all ()
     nds_p2=node.topicos
     foco=node.get_all_f ()  
     nodek.append(node)    
     #_nds2=nodek.get_all_raw(node_cmps)     
     print 'NDS:======','[',node.name,']'
     #node.dump_layer()
     #print 'NDS(END):======','[',node.name,']'
     for n1 in nds:
      print '---------'
      #==============================      
      if self.s_compare_dt(n1,'mandator'):
        dmand=True
      #==============================      
      if self.s_compare_dt(n1,'source') or self.s_compare_dt(n1,'destination') or \
        self.s_compare_dt(n1,'compare') or \
        self.s_compare_dt(n1,'refresh') or \
        self.s_compare_dt(n1,'memorize') or \
        self.s_compare_dt(n1,'autoask') or \
        self.s_compare_dt(n1,'no_return') or \
        self.s_compare_dt(n1,'returns') or \
        self.s_compare_dt(n1,'behav') or \
        self.s_compare_dt(n1,'tstatic') or \
        self.s_compare_dt(n1,'serialize') or \
        self.s_compare_dt(n1,'raw_afininity') or \
        self.s_compare_dt(n1,'preditive') or \
        self.s_compare_dt(n1,'reactive') or \
        self.s_compare_dt(n1,'link') or \
        self.s_compare_dt(n1,'rename') or \
        self.s_compare_dt(n1,'write2') or \
        self.s_compare_dt(n1,'write') or \
        self.s_compare_dt(n1,'link-nr') or \
        self.s_compare_dt(n1,'filter') or \
        self.s_compare_dt(n1,'collect') or \
        self.s_compare_dt(n1,'collect_s') or \
        self.s_compare_dt(n1,'collect_apr')or\
        self.s_compare_dt(n1,'gravity')or\
        self.s_compare_dt(n1,'variancy')or\
        self.s_compare_dt(n1,'relative')or\
        self.s_compare_dt(n1,'refuzzy')or\
        self.s_compare_dt(n1,'mandator')or\
        self.s_compare_dt(n1,'interface'):
          continue
      print n1.dt
      for snd1 in n1.sinapses:
       print '+++++++++++++++++++++++++++++++++++++'
       print snd1.nr.dt
       print '++++++++++++++++++++++++++++++++++++++'
      print '---------'
     print '=========='
     nds2=self.get_all_it(node.name,usr)
     #==========  
     need_s=0
     need_s2=[]
     #== 
     for n1 in nds:
      neuron_base_c=n1 
      #================================  
      if self.s_compare_dt(n1,'source') or self.s_compare_dt(n1,'destination') or \
        self.s_compare_dt(n1,'compare') or \
        self.s_compare_dt(n1,'no_return') or \
        self.s_compare_dt(n1,'refresh') or \
        self.s_compare_dt(n1,'memorize') or \
        self.s_compare_dt(n1,'returns') or \
        self.s_compare_dt(n1,'tstatic') or \
        self.s_compare_dt(n1,'autoask') or \
        self.s_compare_dt(n1,'behav') or \
        self.s_compare_dt(n1,'serialize') or \
        self.s_compare_dt(n1,'raw_afininity') or \
        self.s_compare_dt(n1,'preditive') or \
        self.s_compare_dt(n1,'reactive') or \
        self.s_compare_dt(n1,'link') or \
        self.s_compare_dt(n1,'rename') or \
        self.s_compare_dt(n1,'write') or \
        self.s_compare_dt(n1,'write2') or \
        self.s_compare_dt(n1,'link-nr') or \
        self.s_compare_dt(n1,'filter') or \
        self.s_compare_dt(n1,'collect') or \
        self.s_compare_dt(n1,'collect_s') or \
        self.s_compare_dt(n1,'interface')or\
        self.s_compare_dt(n1,'mandator')or\
        self.s_compare_dt(n1,'gravity')or\
        self.s_compare_dt(n1,'variancy')or\
        self.s_compare_dt(n1,'relative')or\
        self.s_compare_dt(n1,'refuzzy')or\
        self.s_compare_dt(n1,'collect_apr') or\
        \
        self.s_compare_dt(n1,'interface') :
          pass
      else: 
       if dmand and n1.cmandatory: #neuron_base_c
        need_s+=1 
        need_s2.append(n1.dt)
       elif not dmand: 
        need_s+=1 
        need_s2.append(n1.dt)
     #==========    
     tps_len=0
     '''
       ==================================================
       ==================================================
     '''
     
     
     indckl=1     
     if True:
      l=layer_Atu_r
      #get_all() -> l.process_inter_find_path()
      print 'def-L:',l,',n:',indckl
      indckl+=1
      dyned.append(l)
      l.process_inter_find_path(purpose)
      nds2=l.get_all ()      
      #print 'dump.nds2-----------------------------------------------------'
      #l.dump_layer()
      print 'NDS2:======[',l.name,']'
      for n1 in nds2:
       print '----------'
       print n1.dt
       for snds in n1.sinapses:
        print '++++++++++++++'
        print snds.nr.dt
        print '++++++++++++++'
       print '----------'
      print '==========='
      colect_gr=[]
      colect_gr2=[]
      tps_len=0
      rt=mdLayer ()
      print 'NDS-LEN:',len(nds) ,'================='
      if len(nds)>0: print 'nds[0].owner:',nds[0].owner.name
      for d in nds:
       print d.dt,'<<'
      print '======================================'
      nrs_fnds=[]
      index_nr_Atu=-1
      len_mx=len(nds)
      while index_nr_Atu < len_mx:
        index_nr_Atu+=1
        if index_nr_Atu >= len(nds): continue
        nr_atu=nds[index_nr_Atu]
        _nds2=[]
        verbo_compl=[] # dados complementares do filtro
        for e in nodek:
         _nds22=e.get_all_raw(node_cmps)
         for _nd in _nds22 :
          _nds2.append(_nd)
        #=== 
        verbo='vbNoDef'      
        if self.s_compare_dt(nr_atu,'no_return'):
         verbo='vb_no_return'
        if self.s_compare_dt(nr_atu,'returns'):
         verbo='vb_return'
        if self.s_compare_dt(nr_atu,'novary'):
         verbo='vb_novary'         
        if self.s_compare_dt(nr_atu,'relative'):
         verbo='vb_relative'
        if self.s_compare_dt(nr_atu,'variancy'):
         verbo='vb_variancy'
        if self.s_compare_dt(nr_atu,'compare'):
         verbo='vb_compare'
        if self.s_compare_dt(nr_atu,'refresh'):
         verbo='vb_refresh'
        if self.s_compare_dt(nr_atu,'memorize'):
         verbo='vb_memorize'
        if self.s_compare_dt(nr_atu,'autoask'):
         verbo='vb_auto_ask'
        if self.s_compare_dt(nr_atu,'behav'):
         verbo='vb_behavior'
        if self.s_compare_dt(nr_atu,'raw_afininity'):
         have_collected.append(True)
         verbo='vb_afininity_raw'
        if self.s_compare_dt(nr_atu,'preditive'):
         verbo='vb_preditiva'
        if self.s_compare_dt(nr_atu,'reactive'):
         verbo='vb_reactive'
        if self.s_compare_dt(nr_atu,'link'):
         verbo='vb_link'
        if self.s_compare_dt(nr_atu,'rename'):
         verbo='vb_rename'
        if self.s_compare_dt(nr_atu,'write2'):
         verbo='vb_write2'
        if self.s_compare_dt(nr_atu,'write'):
         verbo='vb_write'
        if self.s_compare_dt(nr_atu,'link-nr'):
         verbo='vb_link_nr'
        if self.s_compare_dt(nr_atu,'filter'):
         verbo='vb_filter'
        if self.s_compare_dt(nr_atu,'collect'):
         have_collected.append(True)
         verbo='vb_collect'
        if self.s_compare_dt(nr_atu,'collect_s'):
         have_collected.append(True)
         verbo='vb_collect_s'
        if self.s_compare_dt(nr_atu,'collect_apr'):
         have_collected.append(True)
         verbo='vb_collect_apr'
        if self.s_compare_dt(nr_atu,'gravity'):
         verbo='vb_gravity'
        if self.s_compare_dt(nr_atu,'refuzzy'):
         verbo='vb_refuzzy'
        if self.s_compare_dt(nr_atu,'tstatic'):
         verbo='tstatic'
        if self.s_compare_dt(nr_atu,'serialize'):
         verbo='serialize'
         for s in nr_atu.sinapses:
          for ds in s.nr.dt:
           verbo_compl.append(ds)
        if verbo !=  'vbNoDef':
         for s in nr_atu.sinapses:
          for ds in s.nr.dt:
           verbo_compl.append(ds)
        #=============================================   
        if verbo=="vb_auto_ask":
         identif=[]
         type_S="1"
         purpc=purpose
         for nr_t in _nds2:
          if self.s_compare_dt(nr_t,'composicao'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
             identif.append(d)
          if self.s_compare_dt(nr_t,'datatype'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
             type_S=(d)
             break
          if self.s_compare_dt(nr_t,'destination'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
             purpc=(d)
             break
         if len(identif) > 0 :
           runask(usr,purpc,identif,type_S)         
        #======================================== 
        if verbo == 'vb_no_return':
          adm_return=False
        #==============================
        if verbo == 'serialize':
          adm_serialize=True
          for c in verbo_compl:
            adm_serialize_cmp.append(c)
        #==============================
        if verbo == 'vb_return':
          adm_return_val_call2=False 
          if len(verbo_compl)>=4:
            tpcall=verbo_compl[0]# $emptystack,$stack,$empty,$object,$pub,$top,$inverse -> only $stack,$emptystack,$inverse and $empty run
            if verbo_compl[1]=='rctline': #chamada secundaria de ractionline
             # $inverse : se nao funcionou o rct, chama outra rct 
             if tpcall in ['$stack','$emptystack','$inverse']:  
               adm_return_val_p_call=[tpcall,verbo_compl[2],[]] 
               adm_return_val_call=True
               adm_return_val_call2=True
             elif tpcall in ['$empty' ]: 
               adm_return_val_p_call=[tpcall,verbo_compl[2],verbo_compl[3]] 
               adm_return_val_call=True
               adm_return_val_call2=True
          #==============================================================  
          if not adm_return_val_call2:
           adm_return_val=True
           for compl in verbo_compl:  
            adm_return_val_p.append(compl)
           
        #==============================
        if verbo=='vb_novary': # especifia os elementos sem variacao
         for compl in verbo_compl:
          self.novariancy.append(compl)
        #==============================
        if verbo=='vb_variancy': # especifia os elementos com variacao e seus pares elemento-antigo>elemento-adicionar
         for compl in verbo_compl: # first element indicates qual item sofre alteracao, se o parametro rct  ou se o dado analizado
          self.variancy.append(compl)
        #==============================          
        if verbo=='vb_relative': # traduz data/hora simbolica em data/hora real        
         if len(verbo_compl)>=1 :
           import calendar
           def inc_Date(data,day,month,year):             
             import datetime as date2
             import time as time2
             dataref=None
             try:
              format_tmp='%d/%m/%Y' 
              dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
             except:
              try: 
               format_tmp='%d-%m-%Y' 
               dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
              except: 
                try: 
                 format_tmp='%Y-%m-%d' 
                 dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
                except: 
                  try: 
                   format_tmp='%m-%d-%Y' 
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
                  except: 
                    return None
             if dataref== None: return None
             if day != 0 :
              dref=dataref + date2.timedelta(days=day)
             if month != 0 :
              dref=dataref + date2.timedelta(months=month)
             if year != 0 :
              dref=dataref + date2.timedelta(years=year)
             return dref 
           def inc_hour(data,hour,minute,second):
             import datetime as date2
             import time as time2
             dataref=None
             try:
              format_tmp='%H:%M:%S' 
              dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
             except:
                 return None
             if dataref== None: return None
             if hour != 0 :
              dref=dataref + date2.timedelta(hours=hour)
             if minute != 0 :
              dref=dataref + date2.timedelta(minutes=minute)
             if second != 0 :
              dref=dataref + date2.timedelta(seconds=second)
             return dref 
           def get_days_of_week(dataref,start_day=0,fixed_day=0):# retornar sempre para o primeiro dia da semana se a semana nao for a semana atual
             import datetime as date2
             import time as time2 
             import calendar
             def week_of_month(tgtdate):    
               days_this_month = calendar.mdays[tgtdate.month]
               for i in range(1, days_this_month):
                  d = date2.date(tgtdate.year, tgtdate.month, i)
                  if d.day - d.weekday() > 0:
                   startdate = d
                   break
               # now we canuse the modulo 7 appraoch
               difdt=tgtdate.date() - startdate
               return ((difdt).days //7 + 1)+1
             def dia_da_Semana(data):
              EndDate=data.isoweekday()+1
              if EndDate > 7 : EndDate=1
              return  EndDate
             start=dia_da_Semana(dataref)
             dtoday=date2.datetime.today()
             atu_week=week_of_month(dtoday)
             dt_week=week_of_month(dataref)
             #print 'atu_week(',dtoday,'):',atu_week
             #print 'dt_week(',dataref,'):',dt_week
             #print 'start:',start
             datas=[]
             if dataref.month!=dtoday.month or dataref.year!=dtoday.year or atu_week!=dt_week:
              #
              if start != 7:
                  while start < 7:
                   dataref = dataref + date2.timedelta(days=1)
                   start+=1
                  #start-=1
                  #print 'start:',start
              #
              while start > 0 :
               if start_day > 0 :
                if start_day > start:
                 break
               if fixed_day > 0 :
                if fixed_day != start:
                 dataref = dataref + date2.timedelta(days=-1)
                 start-=1     
                 continue    
               #if fixed_day == 0 or start==1:
               datas.append([start,dataref])
               dataref = dataref + date2.timedelta(days=-1)
               #print 'weed.day:',start,',date:',dataref
               start-=1
             else:              
              if start != 7:
                  while start < 7:
                   dataref = dataref + date2.timedelta(days=1)
                   start+=1
                  #start-=1
                  
                  #print 'start:',start
              #                 
              while start > 0 :
               #print 'weed.day:',start,',date:',dataref
               if fixed_day > 0 :
                 if fixed_day != start:
                   start-=1
                   dataref = dataref + date2.timedelta(days=-1)
                   continue
                 
               datas.append([start,dataref])
               dataref = dataref + date2.timedelta(days=-1)
               start-=1
                 
             return datas
           def decr_month(when):
               import datetime as date2
               import time as time2
               import calendar
               days = calendar.monthrange(when.year, when.month)[1]
               return when + date2.timedelta(days=(days*-1))            
           def increment_month(when):
               import datetime as date2
               import time as time2
               import calendar
               days = calendar.monthrange(when.year, when.month)[1]
               return when + date2.timedelta(days=days)            
           def inc_week(data,week):
             import datetime as date2
             import time as time2
             if isinstance(data, str):
               pass
             else:
               data=data.strftime("%d-%m-%Y")
             dataref=None
             try:
              format_tmp='%d/%m/%Y' 
              dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
             except:
              try: 
               format_tmp='%d-%m-%Y' 
               dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
              except: 
                try: 
                 format_tmp='%Y-%m-%d' 
                 dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
                except: 
                  try: 
                   format_tmp='%m-%d-%Y' 
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(data, format_tmp)))
                  except: 
                    return None
             if dataref== None: return None
             dref=dataref + date2.timedelta(weeks=week)     
             return dref
              
             
           #verbo_compl[0]->type : date/time/datetime
           #verbo_compl[1]->topico-traduzir
           topico=verbo_compl[0]
           topico_aux=''
           topico_mult=''
           if len(verbo_compl)>1:
             topico_aux=verbo_compl[1] #referencia, para o caso de after-topico(indica a referencia a qual ser� aplicada a data) 
           if len(verbo_compl)>2:
             topico_mult=verbo_compl[2] #qtd(ex. next-hour->pode indicar mais de uma hora next)
             
           #==========================obs: topico->pode ter mais de uma linha, contendo data e hora ou varias datas ou varias horas, pois no verbo_compl[1] contem o nome do topico do [layers] a traduzir
           #$day->grava a data atual
           #$now->grava um topico com um child data e mais um child hora
           #$time->grava a hora atual
           #=========================
           #$all-time-> grava simplesmente um $$all$$ no retorno de hora(child time) para aceitar qualquer hora
           #=========================
           #next-day-relative(ex.proximo dia 31)-> grava a data completa do dia 31 desse mes.
           #every-day-month(grava cada dia(ex.31) de tds os meses do ano )
           #$next-day(grava a data de amanha),$next-week(grava todos os dias da prx semana),$next-year,$next-month
           #$day-after,$week-after(tds os dias da ultima semana),$month-after,
           #$day-before,$year-before,$month-before,$week-before
           #$next-sunday,....$next-saturday
           #$next-hour,$next-minute,$next-second,$hour-after,$minute-after,$hour-before,$minute-before,$second-before,
           dts_topico=[]
           dts_topicoaux=[]
           dts_topicoaux2=[]
           dts_topico_mult=[]
           import datetime as date2
           import time as time2           
           for nr_t in _nds2:
            if self.s_compare_dt(nr_t,topico):
             for s in nr_t.sinapses:
              for d in s.nr.dt:
               dts_topico.append([d,nr_t])
            if self.s_compare_dt(nr_t,topico_aux):
             dts_topicoaux2.append(nr_t) 
             for s in nr_t.sinapses:
              for dpt in s.nr.dt:
               if dpt != '$$all$$': 
                dts_topicoaux.append([dpt,nr_t])              
            if self.s_compare_dt(nr_t,topico_mult):
             for s in nr_t.sinapses:
              for dpt in s.nr.dt:
               if dpt != '$$all$$': 
                dts_topico_mult.append([dpt,nr_t])              
           for [dt,nr_t2] in dts_topico:
             ant_v=nr_t2.sinapses
             nr_t2.sinapses=[]
             snr_t2=nr_t2
             if dt == '$day':
               if len(dts_topicoaux) == 0 :
                hoje = str(datetime.today().day)
                snr_t2.owner.set_nr_ch(snr_t2,hoje,'composicao')     
               else:
                for [au,tp] in dts_topicoaux:
                  try:
                   sk=time2.mktime(time2.strptime(au, '%d-%m-%Y')) 
                   dataref=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     continue
                  #======================== 
                  hj = str(dataref.day)
                  snr_t2.owner.set_nr_ch(snr_t2,hj,'composicao')     

             if dt == '$date-interval':
               if len(dts_topicoaux) > 1:
                  snr_t2=nr_t2 
                  [au1,tp1] = dts_topicoaux[0]
                  [au2,tp2] = dts_topicoaux[1]
                  dataref=None
                  dataref2=None
                  try:
                   sk=time2.mktime(time2.strptime(au1, '%d-%m-%Y')) 
                   dataref=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     dataref=datetime.today()
                  #======================== 
                  try:
                   sk=time2.mktime(time2.strptime(au2, '%d-%m-%Y')) 
                   dataref2=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref2=date2.date(datetime.today().year,month,int(au2)) 
                   except : # nenhum formato valido
                     dataref2=datetime.today()
                  #========================
                  from datetime import date, timedelta as td
                  delta = dataref2 - dataref
                  for i in range(delta.days + 1):
                      datafin= dataref + td(days=i)                  
                      chrc=datafin.strftime("%d-%m-%Y")
                      snr_t2.owner.set_nr_ch(snr_t2,chrc,'composicao')     
                  #========================================
             if dt == '$month':
               if len(dts_topicoaux) == 0 :
                hoje = str(datetime.today().month)
                snr_t2.owner.set_nr_ch(snr_t2,hoje,'composicao')     
               else:
                for [au,tp] in dts_topicoaux:
                  try:
                   sk=time2.mktime(time2.strptime(au, '%d-%m-%Y')) 
                   dataref=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     continue
                  #======================== 
                  hj = str(dataref.month)
                  snr_t2.owner.set_nr_ch(snr_t2,hj,'composicao')     

             if dt == '$year':
               if len(dts_topicoaux) == 0 :
                hoje = str(datetime.today().year)
                snr_t2.owner.set_nr_ch(snr_t2,hoje,'composicao')     
               else:
                for [au,tp] in dts_topicoaux:
                  try:
                   sk=time2.mktime(time2.strptime(au, '%d-%m-%Y')) 
                   dataref=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     continue
                  #======================== 
                  hj = str(dataref.year)
                  snr_t2.owner.set_nr_ch(snr_t2,hj,'composicao')     
             if dt == '$date':
               hoje = datetime.today().strftime("%d-%m-%Y")
               snr_t2.owner.set_nr_ch(snr_t2,hoje,'composicao')   
               
             if dt == '$time':
               now = datetime.today().strftime("%H:%M:%S")               
               snr_t2.owner.set_nr_ch(snr_t2,now,'composicao')               
             if dt == '$now':
               hoje = datetime.today().strftime("%d-%m-%Y")
               snr_t2.owner.set_nr_ch(snr_t2,hoje,'composicao')               
               #===============
               now = datetime.today().strftime("%H:%M:%S")               
               snr_t2.owner.set_nr_ch(snr_t2,now,'composicao')               
             if dt == '$year-now':
               hoje = datetime.today().year
               snr_t2.owner.set_nr_ch(snr_t2,str(hoje),'composicao')               
             if dt == '$month-now':
               hoje = datetime.today().month
               snr_t2.owner.set_nr_ch(snr_t2,str(hoje),'composicao')               
             if dt == '$day-now':
               hoje = datetime.today().day
               snr_t2.owner.set_nr_ch(snr_t2,str(hoje),'composicao')               
             if dt == '$next-day-relative':
                for [au,tp] in dts_topicoaux:
                  try:
                   sk=time2.mktime(time2.strptime(au, '%d-%m-%Y')) 
                   dataref=date2.datetime.fromtimestamp(sk) 
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     continue
                  #======================== 
                  if len(dts_topico_mult)==0:
                   dataref = dataref + date2.timedelta(days=1)
                   date_consid=dataref.strftime("%d-%m-%Y")
                   snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
                  else:
                    for [a2,nra2] in dts_topico_mult: # multiplo de dias
                      dataref = dataref + date2.timedelta(days=int(a2))
                      date_consid=dataref.strftime("%d-%m-%Y")
                      snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
               
             if dt == '$every-day-month':
               month=datetime.today().month
               while month <= 12:
                for [au,topic] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                   dataref=date2.date(dataref.year,month,dataref.day)
                  except :  # dia de referencia
                   try: 
                    dataref=date2.date(datetime.today().year,month,int(au)) 
                   except : # nenhum formato valido
                     continue
                  #
                  date_consid=dataref.strftime("%d-%m-%Y")
                  #
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
                  #
                month+=1
             if dt == '$next-day':
               data=datetime.today()
               dataref = data + date2.timedelta(days=1)
               date_consid=dataref.strftime("%d-%m-%Y")
               snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-day2':#ex. depois de amanha
               data=datetime.today()
               dataref = data + date2.timedelta(days=2)
               date_consid=dataref.strftime("%d-%m-%Y")
               snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-week':
               data=datetime.today()
               if len(dts_topico_mult)==0:
                 date_consid=data.strftime("%d-%m-%Y") 
                 data=inc_week(date_consid,1)
                 days=get_days_of_week(data)
                 for [week,d] in days:
                    date_consid=d.strftime("%d-%m-%Y")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
               else:   
                 for [a2,nra2] in dts_topico_mult:
                   data=inc_week(data,int(a2))
                   days=get_days_of_week(data)
                   for [sm,d] in days:
                      date_consid=d.strftime("%d-%m-%Y")
                      snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$inc':
               for [au,nrau] in dts_topicoaux:
                   if True:
                     try:
                      number_base=float(au) 
                     except:
                       print 'number(0) invalid.',number_base
                       continue
                     if len(dts_topico_mult)>0:
                      for [a2,nra2] in dts_topico_mult:
                       try:
                        ndt=float(a2)
                       except:
                         print 'number(1) invalid:',au
                         continue
                       number_base=number_base+ndt
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
                     else: 
                       number_base+=1
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
             if dt == '$dec':
               for [au,nrau] in dts_topicoaux:
                   if True:
                     try:
                      number_base=float(au) 
                     except:
                       print 'number(0) invalid.',number_base
                       continue
                     if len(dts_topico_mult)>0:
                      for [a2,nra2] in dts_topico_mult:
                       try:
                        ndt=float(a2)
                       except:
                         print 'number(1) invalid:',au
                         continue
                       number_base=number_base-ndt
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
                     else: 
                       number_base-=1
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
             if dt == '$mul':
               for [au,nrau] in dts_topicoaux:
                   if True:
                     try:
                      number_base=float(au) 
                     except:
                       print 'number(0) invalid.',number_base
                       continue
                     if len(dts_topico_mult)>0:
                      for [a2,nra2] in dts_topico_mult:
                       try:
                        ndt=float(a2)
                       except:
                         print 'number(1) invalid:',au
                         continue
                       number_base=number_base*ndt
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
                     else: 
                       number_base=number_base*1
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
             if dt == '$div':
               for [au,nrau] in dts_topicoaux:
                   if True:
                     try:
                      number_base=float(au) 
                     except:
                       print 'number(0) invalid.',number_base
                       continue
                     if len(dts_topico_mult)>0:
                      for [a2,nra2] in dts_topico_mult:
                       try:
                        ndt=float(a2)
                       except:
                         print 'number(1) invalid:',au
                         continue
                       number_base=number_base/ndt
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
                     else: 
                       number_base=number_base/1
                       snr_t2.owner.set_nr_ch(nr_t2,str(number_base),'composicao')                  
             if dt == '$aglutinate':
               for nrau in dts_topicoaux2:
                tps=nrau
                if tps != None:
                 sins=tps.sinapses 
                 if len(tps.sinapses)==0:
                   sins=ant_v
                 val=''  
                 for da in sins: 
                   for dta in da.nr.dt:
                     val+=dta
                 snr_t2.sinapses=[]    
                 snr_t2.owner.set_nr_ch(nr_t2,val,'composicao')                  
             if dt == '$expand':
               for [au,nrau] in dts_topicoaux:
                 tps=[au]
                 for dta in tps: 
                      for [a3,nra3] in dts_topico_mult: # parametros para parser 
                        a2=a3.split(':')
                        sep=a2[0]
                        cs=a2[1:]
                        resultp=[]
                        for ak in cs:
                         r=ak.split('.')
                         for r1 in r:
                          resultp.append(r1) 
                        exp=dta.split(sep)
                        indexp=-1
                        for ex in exp:
                         indexp+=1 
                         if len(resultp)==0:  
                          snr_t2.owner.set_nr_ch(nr_t2,ex,'composicao')                  
                         else:
                           krp=None
                           if indexp < len(resultp):
                             krp=resultp[indexp]
                           else:
                             krp=resultp[len(resultp)-1]
                           ktpn=snr_t2.owner.set_nr_ch(nr_t2,krp,'composicao')  
                           snr_t2.owner.set_nr_ch(ktpn,ex,'composicao')  
                             
             if dt == '$next-week-dayfixed':
               for [au,nrau] in dts_topicoaux:
                data=datetime.today().strftime("%d-%m-%Y") 
                if len(dts_topico_mult)>0:
                 for [a2,nra2] in dts_topico_mult:
                  data=inc_week(data,int(a2))
                  try:
                   days=get_days_of_week(data,0,int(au))
                  except:
                    print 'days.fixedday invalid:',au
                    continue
                  for [sm,d] in days:
                    date_consid=d.strftime("%d-%m-%Y")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')                  
                else:  
                  data=inc_week(data.strftime("%d-%m-%Y"),1)
                  try:
                   days=get_days_of_week(data,0,int(au))
                  except:
                    print 'days.fixedday invalid:',au
                    continue
                  for [sem,d] in days:
                    date_consid=d.strftime("%d-%m-%Y")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-year':               
               data=datetime.today()
               if len(dts_topico_mult)==0:
                 dataref = date2.date(data.year+1,data.month,data.day)
                 ye=1
                 while ye <= 12:
                  dayst=calendar.monthrange(dataref.year,dataref.month)
                  days=list(dayst)
                  start=days[0]
                  end=days[1]
                  for d in range(start,end+1):
                   dataref=date2.date(dataref.year,dataref.month,d) 
                   date_consid=dataref.strftime("%d-%m-%Y")
                   snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
                  ye+=1 
               else:   
                 for [a2,nra2] in dts_topico_mult:                   
                   #dataref = data + date2.timedelta(years=int(a2))
                   dataref = date2.date(data.year+int(a2),data.month,data.day)
                   ye=1
                   while ye <= 12:
                    dayst=calendar.monthrange(dataref.year,dataref.month)
                    days=list(dayst)
                    start=days[0]
                    end=days[1]
                    for d in range(start,end+1):
                       dataref=date2.date(dataref.year,dataref.month,d) 
                       date_consid=dataref.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
                    ye+=1 
             if dt == '$next-month':              
               if len(dts_topico_mult)> 0 :
                for [a2,nra2] in dts_topico_mult:
                 data=datetime.today()
                 #dataref = data + date2.timedelta(months=int(a2))
                 dataref=increment_month(data)
                 #dataref = date2.date(data.year,data.month+int(a2),data.day)
                 dayst=calendar.monthrange(dataref.year,dataref.month)
                 days=list(dayst)
                 start=days[0]
                 end=days[1]
                 for d in range(start,end+1):
                  dataref=date2.date(dataref.year,dataref.month,d) 
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
               else:  
                 data=datetime.today()
                 dataref=increment_month(data)
                 dayst=calendar.monthrange(dataref.year,dataref.month)
                 days=list(dayst)
                 start=days[0]
                 end=days[1]
                 for d in range(start,end+1):
                  dataref=date2.date(dataref.year,dataref.month,d) 
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$week-in-month':
                def week_of_month(tgtdate):    
                 days_this_month = calendar.mdays[tgtdate.month]
                 for i in range(1, days_this_month):
                    d = date2.date(tgtdate.year, tgtdate.month, i)
                    if d.day - d.weekday() > 0:
                     startdate = d
                     break
                 # now we canuse the modulo 7 appraoch
                 difdt=tgtdate.date() - startdate
                 return ((difdt).days //7 + 1)+1                  
                #====================================================================               
                #====================================================================               
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido, entao defaul o dia atual
                      dataref=datetime.today()
                  #======================== 
                  wekk=week_of_month(dataref)
                  #
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    diff_week=int(a2)-wekk
                    #=  
                    data=inc_week(dataref.strftime("%d-%m-%Y") ,diff_week)
                    days=get_days_of_week(data)
                    for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')                  
             if dt == '$day-after':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  dataref = dataref + date2.timedelta(days=1)
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$week-after':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$month-after':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  #========================
                  dataref = increment_month(dataref)
                  
                  #dataref = dataref + date2.timedelta(months=1)
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$day-before':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  dataref = dataref + date2.timedelta(days=-1)
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$year-before':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  dataref = date2.date(dataref.year-1,dataref.month,dataref.day)
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$month-before':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  dataref = decr_month( dataref )
                  date_consid=dataref.strftime("%d-%m-%Y")
                  snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$week-before':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-domingo':
                if True:
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,1)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-segunda':
                if True:
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,2)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-terca':
                if True:
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,3)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-quarta':
                if True:
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,4)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-quinta':
                if True:
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,5)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-sexta':
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,6)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$next-sabado':
                  #======================== 
                  data=datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,1)
                  days=get_days_of_week(data,0,7)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             #=========================================================================
             #=========================================================================
             #=========================================================================
             if dt == '$last-domingo':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,1)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-segunda':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,2)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-terca':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,3)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-quarta':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,4)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-quinta':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-11)
                  days=get_days_of_week(data,0,5)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-sexta':
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,6)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$last-sabado': # relative
                for [au,nrau] in dts_topicoaux:
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,7)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$this-domingo':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,1)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$this-segunda':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,2)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$this-terca':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,3)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')
             if dt == '$this-quarta':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,4)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$this-quinta':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,5)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$this-sexta':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,6)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$this-sabado':
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  days=get_days_of_week(data,0,7)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')     
                       
             if dt == '$last-domingo-abs': # data nao relativa, apartir de hj
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,1)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                       
             if dt == '$last-segunda-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,2)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$last-terca-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,3)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$last-quarta-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,4)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$last-quinta-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,5)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$last-sexta-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,6)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$last-sabado-abs':  
                if True:
                  #======================== 
                  data=dtoday=date2.datetime.today()
                  data=inc_week(data.strftime("%d-%m-%Y") ,-1)
                  days=get_days_of_week(data,0,7)
                  for [sm,d] in days:
                       date_consid=d.strftime("%d-%m-%Y")
                       snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$next-hour':
                  data=dtoday=date2.datetime.now()
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(hours=int(a2))
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                  else:
                    dataref = data + date2.timedelta(hours=1)
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$next-minute':
                  data=dtoday=date2.datetime.now()
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(minutes=int(a2))
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(minutes=1)
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$next-second':
                  data=dtoday=date2.datetime.now()
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(seconds=int(a2))
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                  else:                   
                    dataref = data + date2.timedelta(seconds=1)
                    date_consid=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
             if dt == '$hour-after':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(hours=int(a2))
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:                  
                    dataref = data + date2.timedelta(hours=1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
             if dt == '$minute-after':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                     dataref = data + date2.timedelta(minutes=int(a2))
                     date_consid=dataref.strftime("%d-%m-%Y")
                     date_consid2=dataref.strftime("%H:%M:%S")
                     snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                     snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(minutes=1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
             if dt == '$second-after':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(seconds=int(a2))
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(seconds=1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
             if dt == '$hour-before':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(hours=int(a2)*-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(hours=-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
             if dt == '$minute-before':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(minutes=int(a2)*-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(minutes=-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
             if dt == '$second-before':
                if len(dts_topicoaux)>1:
                  [au,nrau] = dts_topicoaux[0]
                  [au2,nrau2] = dts_topicoaux[1]
                  au+=' '+au2
                  try:
                   dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d-%m-%Y %H:%M:%S'))) 
                  except :  # dia de referencia
                   try:
                    dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                   except :  # dia de referencia
                    try:
                     dataref=date2.datetime.fromtimestamp(time2.mktime(time2.strptime(au, '%d/%m/%Y %H:%M:%S'))) 
                    except :  # dia de referencia
                     try: 
                      dataref=date2.date(datetime.today().year,month,int(au)) 
                     except : # nenhum formato valido
                      continue
                  #======================== 
                  data=dataref
                  if len(dts_topico_mult)> 0 :
                   for [a2,nra2] in dts_topico_mult:
                    dataref = data + date2.timedelta(seconds=int(a2)*-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
                  else:   
                    dataref = data + date2.timedelta(seconds=-1)
                    date_consid=dataref.strftime("%d-%m-%Y")
                    date_consid2=dataref.strftime("%H:%M:%S")
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid,'composicao')               
                    snr_t2.owner.set_nr_ch(nr_t2,date_consid2,'composicao')               
        #============================== 
        if verbo == 'vb_memorize':
          to_memorize=None
          print 'Run.vb_memorize:',verbo_compl
          if len(verbo_compl) >= 1: #postar no GlobalStack.memorize 
            to_memorize=verbo_compl[0]
            if to_memorize == 'clear': #clear no GlobalStack.memorize
              GlobalStack.memorize=[]
            else:  
             for nr in nds2:
              if self.s_compare_dt(nr,to_memorize):
                print 'Memorize:',nr.dt
                nrM=nr.clone_nr(nr)
                GlobalStack.memorize.append(nrM)            
          elif len(verbo_compl) == 0:  # extrair do GlobalStack.memorize  
             for nr in GlobalStack.memorize:
                nr_p=nr.clone_nr(nr)
                node.set_topico_nr(nr_p)
                if rt!= None:
                 rt.set_topico_nr(nr_p)          
        if verbo=='vb_refresh':
         print 'Run.(',verbo,')'
         nds=node.get_all ()
         nds2=l.get_all()
         len_mx=len(nds)
        #==============================          
        if verbo=='vb_compare':
         print 'Run.(',verbo,')'
         # achar identificador
         # achar dest
         # gerar comparativo
         # anotar igualdades
         # anotar diferencas
         for nr_t in _nds2:
          identif=[]
          dest=[]
          foco=[]
          if self.s_compare_dt(nr_t,'identificador'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
                 if d == "$stack":
                   for [o,s] in layers_collect_s:
                    identif.append(o.name) 
                    if s != None:
                      if umisc.trim(s) != '':
                       identif.append(s)  
                 else:
                  identif.append(d)
          if self.s_compare_dt(nr_t,'dest'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
             dest.append(d)
          if self.s_compare_dt(nr_t,'foco'):
           for s in nr_t.sinapses:
            for d in s.nr.dt:
             foco.append(d)
          if len(identif) > 0  and len(dest) > 0 :
           [tfound,tnotfound]=self.compare_objs(identif,dest,foco,usr,purpose)
           if len(tfound) > 0 :
            tps=rt.set_topico('found')
            for r in tfound:
                nr= rt.set_nr(DT)
                tps.connect_to(rt,nr,'Composicao') 
           if len(tnotfound) > 0 :
            tps=rt.set_topico('not-found')
            for r in tnotfound:
                nr= rt.set_nr(DT)
                tps.connect_to(rt,nr,'Composicao') 
           if len(tfound) > 0 or len(tnotfound) > 0 :
            nds2=[]
            fnd_comp=True
            tps_len+=1 
        #==============================
        if verbo=='vb_refuzzy': # 
         if len(verbo_compl) > 1:
          refz=[]
          s=verbo_compl[0] 
          purp2=''
          if len(verbo_compl)>1:
            purp2=verbo_compl[1].split('%') #purpose%purpose%purpose
          for nr_t in _nds2:          
              if self.s_compare_dt(nr_t,s):
               for ss in nr_t.sinapses:
                for d in ss.nr.dt:
                 refz.append(d)
          if len(refz) > 0 :
            layers_collect_s=[]
            aj_param=[]
            refz2=''
            for r in refz:
              if refz2 != '': refz2+=' '
              refz2+=r
            import reFuzzy
            #entry(['groupon classe'], ['simple-search-classify'],'jean.marcos')
            rts=reFuzzy.entry([refz2],purp2,usr)
            if len(rts) > 0 :
              layers=[]
            for r in rts:
              layers_collect_s.append([r,r.name])
        #==============================
        if verbo=='vb_gravity': # collect aproux -> collect em cima de caracts mencionadas         
         direction=verbo_compl[0]
         opcode='refer-C'
         if len(verbo_compl) > 1:
          opcode=verbo_compl[1]
         #
         print 'Gravity point(',direction,',',opcode,',',colect_gr,')==========================='
         if direction == "L":
          tpsd=[]
          for k in nds2:
           print 'Proces nr gravity:',k.dt,k
           ntin=False
           fnd_tps=None
           for [t1,t2] in colect_gr:
            #print 'Nr.gravity:',k,',nr.collect:',t1
            if t1 == k:
              ntin=True 
              fnd_tps=t1             
           if not ntin:          
            tpsd.append(k)
           else:            
            if fnd_tps  :
             tp=fnd_tps
             for cos in tpsd:
                print 'Gravity:from',tp.dt,' to:',cos.dt
                #=== 
                tp.connect_to(cos.owner,cos,opcode)
            tpsd=[]             
            
         if direction == "R":
          topk=None
          foundCK=False
          for k in nds2:      
           dtsk=[]
           if topk != None:
            dtsk=topk.dt
           print 'Proces nr gravity:',k.dt,k,foundCK,topk,dtsk,',tps_len:',tps_len
           ntin=False
           fnd_tps=None
           for [t1,t2] in colect_gr:
            #print 'Nr.gravity:',k,',nr.collect:',t1
            if t1 == k:
              ntin=True 
              fnd_tps=t1  
              foundCK=False              
           if (not ntin) and foundCK and topk != None:   
            print 'Gravity:from',topk.dt,' to:',k.dt  
            topk.connect_to(topk.owner,k,opcode)
           elif ntin:            
            if tps_len > 0 and fnd_tps != None :
             topk=fnd_tps
             if topk == k:
              #if foundCK:
              # break
              foundCK=True
          
         print 'End.Gravity point==========================='
           
 
        #==============================
        if verbo=='vb_collect_apr': # collect aproux -> collect em cima de caracts mencionadas
          #usado para gerar relacao de layers a processar para o call de outra ractionline, fazer coleta de objetos com determinadas caracts
          print 'Run.(',verbo,')' 
          identif=[] # objects  
          referencia=[]
          purpose2=[]
          for nr_t in _nds2:
              if self.s_compare_dt(nr_t,'objective'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 identif.append(d)
              if self.s_compare_dt(nr_t,'reference'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 referencia.append(d)
              if self.s_compare_dt(nr_t,'purpose'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 purpose2.append(d)
             
          for id in identif:
           ob=get_ontology_s(id,purpose2,referencia,usr )
           #get_ontology_ponderate
           for obs in ob:
            layers_collect.append(obs)
 
        if verbo=='vb_collect': # collect  -> collect em ponderacao
          #usado para gerar relacao de layers a processar para o call de outra ractionline, fazer coleta de objetos com determinadas caracts
          print 'Run.(',verbo,')'
          identif=[] # objects  
          referencia=[]
          purpose2=[]
          for nr_t in _nds2:
              if self.s_compare_dt(nr_t,'objective'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 identif.append(d)
              if self.s_compare_dt(nr_t,'rule'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 referencia.append(d)
              if self.s_compare_dt(nr_t,'quality'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 purpose2.append(d)
             
          for id in identif:
           ob=self.s_get_ontology_ponderate(id,referencia,purpose2,usr )
           print 'Collect->result(',id,',',referencia,',',purpose2,',',usr,').[',ob,']'
           for [mx,obs] in ob:
            layers_collect.append(obs)
          
        if verbo=='vb_afininity_raw':# collect by two objects caracts
          print 'Run.(',verbo,')'
          identif=[] # objects  
          referencia=[]
          purpose2=[]
          dfin=[]
          tag_S=[]
          sinapses_consid=[]
          for nr_t in _nds2:
              #print 'Nr-C-COLL',nr_t.dt
              # identificador -> object atual com as carats a implementar
              if self.s_compare_dt(nr_t,'identificador') or self.s_compare_dt(nr_t,'ids2'):
               puc=''
               for s in nr_t.sinapses:
                for d in s.nr.dt: 
                 puc+=' '+d
               puc=umisc.trim(puc) 
               if puc == "$stack":
                   for [o,s] in layers_collect_s:
                    purpose2.append(o.name) 
               else:     
                if puc != '':
                 purpose2.append(puc)
              if self.s_compare_dt(nr_t,'rule'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 r=None
                 for [title,refs] in referencia:
                   if title == s.opcode :
                     r=refs  
                 if r==None:
                   referencia.append([s.opcode,[]])
                   r=referencia[len(referencia)-1][1]
                 if umisc.trim(d) != '':  
                  r.append(d)
              #source ==> objects collected to compar   
              if self.s_compare_dt(nr_t,'plus'): #plus, adicionam para ponderar
               for s in nr_t.sinapses:
                 for d in s.nr.dt:               
                  identif.append(d)
              #========================================    
              if self.s_compare_dt(nr_t,'dfin'):  
               for s in nr_t.sinapses:
                 for d in s.nr.dt:               
                  dfin.append(d)
              #========================================    
              if self.s_compare_dt(nr_t,'sins'):  
               for s in nr_t.sinapses:
                 for d in s.nr.dt:               
                  sinapses_consid.append(d)
              #========================================    
              if self.s_compare_dt(nr_t,'tag'):  
               for s in nr_t.sinapses:
                  print 'collect.tag_s:',s.nr.dt
                  if len(s.nr.dt) > 0 :
                   if s.nr.dt[0] != '':
                    tag_S.append(s.nr)
                   
          if len(purpose2) <= 1:
            #conferir se orig => none, usar os ja coletados como name
            f1=False
            if len(purpose2) ==1:
              if purpose2[0] == '$cache':
                f1=True
            else:
               purpose2.append('')
            if f1:
                purpose2=[]
                for lc2 in layers:
                 purpose2.append(lc2.name)  
                 for lc22 in lc2.get_links_raw(): 
                  purpose2.append(lc22.lr.name)  
          layers_collect_s=[]   
          aj_param=[]
          for id in purpose2:
           print 'Purpose.1:',id        
           if id == '$$all$$': id=''
           inds_tag=-1
           for [sinapse,referencia2] in referencia:
            inds_tag+=1
            print 'tag_S.len(',len(tag_S),')'
            if len(tag_S) > inds_tag:
             print 'tag_S(',inds_tag,'):',tag_S[inds_tag]
            print 'Collect_raw->result==>params:','{',id,'}',referencia2,identif,usr,purpose,referencia2,dfin,sinapses_consid
            ob=self.s_get_ontology_ponderate_caracts(id,referencia2,identif,usr,purpose,referencia2,dfin,sinapses_consid )
            print 'Collect_raw->result(',id,',',referencia2,',',identif,',',usr,').{',ob,'}'           
            for [mx,obs] in ob:
             d1=''
             if len(verbo_compl) > 0 :
              d1=verbo_compl[0]
             print 'layers_collect_s:',[obs,d1]            
             layers_collect_s.append([obs,d1])  
             if obs!= None:
               print 'Input.process:',obs.name 
               layers=[]
               print 'connect.tags.'
               if len(tag_S) > inds_tag:
                tags = tag_S[inds_tag]
                obs.set_topico_nr(tags)
               obs.dump_layer()
            
          
        if verbo=='vb_collect_s': # collect  -> collect em ponderacao
          #usado para gerar relacao de layers a processar para o call de outra ractionline, fazer coleta de objetos com determinadas caracts
          print 'Run.(',verbo,')'
          identif=[] # objects  
          referencia=[]
          purpose2=[]
          for nr_t in _nds2:
              #print 'Nr-C-COLL',nr_t.dt
              if self.s_compare_dt(nr_t,'objective'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 identif.append(d)
              if self.s_compare_dt(nr_t,'rule'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 referencia.append(d)
              if self.s_compare_dt(nr_t,'quality'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 purpose2.append(d)
          
          if len(identif) == 0 :
           ob=[]
           if len(referencia) > 0  or len(purpose2) > 0 :
            ob=self.s_get_ontology_ponderate(None,referencia,purpose2,usr )
           print 'Collect->result(',None,',',referencia,',',purpose2,',',usr,').{',ob,'}' 
           layers_collect_s=[]
           aj_param=[]
           for [mx,obs] in ob:
            d1=''
            if len(verbo_compl) > 0 :
             d1=verbo_compl[0]
            #print 'layers_collect_s:',[obs,d1]
            layers=[]
            layers_collect_s.append([obs,d1])    
          
          for id in identif:
           ob=self.s_get_ontology_ponderate(id,referencia,purpose2,usr )
           print 'Collect->result(',id,',',referencia,',',purpose2,',',usr,').{',ob,'}'           
           for [mx,obs] in ob:
            d1=''
            if len(verbo_compl) > 0 :
             d1=verbo_compl[0]
            #print 'layers_collect_s:',[obs,d1]
            layers_collect_s.append([obs,d1]) 
            layers=[]
            
        #==============================     
        if verbo=='vb_filter': 
          # usado para criar um publish( filtro de nrs em cadeia que sobem p topicos, dentro de uma chamada de rct)
          print 'Run.(',verbo,')'
          
          identif=[] # objects 
          alias=[]
          refer=[] # links
          reverse=[] # links
          for nr_t in _nds2:          
              if self.s_compare_dt(nr_t,'identificador'):
               puc=''
               for s in nr_t.sinapses:
                for d in s.nr.dt: 
                 puc+=' '+d
               puc=umisc.trim(puc) 
               '''
               if puc == '$$all$$':
                 puc2=''
                 for nr_t in nds2:
                   if self.s_compare_dt(nr_t,'identificador'):
                    for s in nr_t.sinapses:
                     for d in s.nr.dt: 
                      puc2+=' '+d
                    break  
                 if puc2 != '' and puc2 != '$$all$$':
                   puc=puc2                 
               '''   
               if puc == "$stack":
                   for [o,s] in layers_collect_s:
                    identif.append(o.name) 
               else:     
                if puc != '':
                 identif.append(puc)
              
              if self.s_compare_dt(nr_t,'alias'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 alias.append(d)
              if self.s_compare_dt(nr_t,'refer'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 refer.append(d)
              if self.s_compare_dt(nr_t,'reverse'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 reverse.append(d)

          
          fnd_al=False
          if len(alias) > 0:
           for nr_t in _nds2:
            for al in alias:           
              if self.s_compare_dt(nr_t,al):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 fnd_al=True
          
          if len(alias) > 0 and fnd_al:
           identif=[] # desconsidera identificador

                 
          if len(alias) > 0:
           for nr_t in _nds2:
            for al in alias:           
              if self.s_compare_dt(nr_t,al):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 identif.append(d)
                 
          print 'Params:','[',identif,'],[', alias,'],[' ,refer ,']' 
          collect_s2=[]           
          aj_param=[]
          for id in identif:
           ant_z=mdTb.Zeus_Mode=True
           ob=get_ontology( id,purpose,usr )
           if len(ob)==0:
            mdTb.Zeus_Mode=False
            ob=get_ontology( id,purpose,usr )             
            mdTb.Zeus_Mode=ant_z                    
           # capturar filtros 
           for obh in ob:
            print 'Filter.obj:',obh,obh.name,'==============='
            for tps in obh.topicos:
             print tps.dt
            print 'Filter.obj.end:',obh,obh.name,'===========(',type_coll,')'
           objs_lnk=[]
           for fl in refer:
            for obj in ob:
             obj.update_links()
             print 'Ob.links-len:',len(obj.links)
             if len(reverse)>0:              
              for lk in obj.get_Reverse_links() :
               print 'Ob.links:',lk.opcode
               # admitir variancy
               #if lk.opcode.upper() == fl.upper(): ## antigo meio de coletar
               if mdFuzzy.compare_n2(lk.opcode,fl,self.variancy,self.novariancy) > 0: # novo meio de coletar, considerando variancy
                objs_lnk.append([lk.lr,fl])
             else:  
              for lk in obj.links :
               print 'Ob.links:',lk.opcode
               # admitir variancy
               #if lk.opcode.upper() == fl.upper(): ## antigo meio de coletar
               if mdFuzzy.compare_n2(lk.opcode,fl,self.variancy,self.novariancy) > 0: # novo meio de coletar, considerando variancy
                objs_lnk.append([lk.lr,fl])
           #===============
           if True:
            for o in ob:
             d1=''
             if len(verbo_compl) > 0 :
              d1=verbo_compl[0]
             #==   
             
             print 'Filter.do(',o.name,',',refer,')'             
             filtered.append(o)
             
             for [oc,opcode_f] in objs_lnk:   
              print 'Get.onto:',oc.name
              oc2=get_ontology( oc.name,purpose,usr ) 
              if len(oc2)==0:
               mdTb.Zeus_Mode=False
               oc2=get_ontology( oc.name,purpose,usr )             
               mdTb.Zeus_Mode=ant_z                    
              #================
              if len(oc2) > 0 :
                layers=[]
              for oc in oc2:               
               print 'Filter.links(',oc.name,',tops:',oc.topicos,')' 
               collect_s2.append([oc,oc.name])
               o.do_filter(verbo_compl)
               #==
          if len(collect_s2)>0:
             layers_collect_s=[]
             for l in collect_s2:
               layers_collect_s.append(l)

        #=============================================================       
        if verbo=='vb_rename': # so opera nos layers_collect
          # linkar objetos, 'referencia' -> 'identificador' em 'purpose' -> motivo
          print 'Run.(',verbo,')->rnm'
          print 'Params:','[',verbo_compl,'],[', layers_collect,'],[',layers_collect_s,']' 
          if len(verbo_compl) > 1: 
           for nr in nds2:
            old=verbo_compl[0]
            new=verbo_compl[1]
            if self.s_compare_dt(nr,old):
              #print 'VB-rename :',old,', to:',new
              nr.dt[0]=new
              pass            
           for nr in nds_p2:
            old=verbo_compl[0]
            new=verbo_compl[1]
            if self.s_compare_dt(nr,old):
              #print 'VB-rename :',old,', to:',new
              nr.dt[0]=new
              pass            
           for nr in _nds2:
            old=verbo_compl[0]
            new=verbo_compl[1]
            if self.s_compare_dt(nr,old):
              #print 'VB-rename :',old,', to:',new
              nr.dt[0]=new
              pass            
        #=============================================================       
        if verbo=='vb_write2': # cria um novo topico e clona o indicado
          # linkar objetos, 'referencia' -> 'identificador' em 'purpose' -> motivo
          def get_subt(arr,nr):
            if len(arr)==0 or nr == None:
              return nr
            consid=arr[0]
            arr=arr[1:]
            if consid in nr.dt:
              for k in nr.sinapses:
                r=get_subt(arr,k.nr)
                if r!= None:
                  return r
            return None    
          def get_subt2(arr,nr,top=None):
            if len(arr)==0 or nr == None:
              return [top,nr]
            consid=arr[0]
            arr=arr[1:]
            if consid in nr.dt:
             if len(nr.sinapses)==0:
               return [nr,None]
             for k in nr.sinapses:
                [r,r2]=get_subt2(arr,k.nr,nr)
                if r!= None:
                  return [r,r2]
            return [None,None]    
          print 'Run.(',verbo,')->rnm'
          print 'Params:','[',verbo_compl,'],[', layers_collect,'],[',layers_collect_s,']' 
          if True:
           def get_dt_from_nr(lay,n):
             dt=n[1:]
             t=lay.get_topico(dt)
             r=''
             for s in t.sinapses:
               for d in s.nr.dt:
                 if r!='': r+=' '
                 r+=d
             return r     
           for compl  in verbo_compl: 
            verbo_compl1=compl.split('->') 
            a1=verbo_compl1[0].split('.')
            a2=verbo_compl1[1].split('.')
            old=a1[0]
            new=a2[0]
            if old[0]=='&': old=get_dt_from_nr(node,old)# referencia dado contido em outro topico
            if new[0]=='&': new=get_dt_from_nr(node,new)# referencia dado contido em outro topico
            for [nrto,nr] in colect_gr2: # colect_gr2-> coletados por conferencia anterior
              if self.s_compare_dt(nrto,old):
                if len(a1)==1  and len(a2)==1 :
                  print 'VB-write :',old,', to:',new
                  new_nr=nrto.clone_nr(nrto)
                  new_nr.sinapses=[]
                  new_nr.connect_to(new_nr.owner,nr,'defs') 
                  # zerar o anterior
                  for sk in nrto.sinapses:
                   if sk.nr == nr:                     
                    nrto.sinapses.remove(sk)
                  #==============
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  node.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
                elif len(a1)>1:
                 #                 
                 #tops=nr.owner.set_nr_ch(nr,'Marca2','Composicao')
                 #tops.owner.set_nr_ch(tops,'Marca3','Composicao')
                 # 
                 [nr1,nrsub]=get_subt2(a1[1:],nr)
                 nr2=None
                 if len(a2)>1:
                   nr2=get_subt(a2[1:],node.get_topico(new))
                 if nr1 != None :
                  print 'VB-write :',old,', to:',new
                  # zerar o anterior
                  for sk in nr1.sinapses:
                   if nrsub!=None: 
                    if sk.nr == nrsub:                     
                     nr1.sinapses.remove(sk)
                  #==============                  
                  new_nr=nr1.clone_nr(nr1)
                  new_nr.dt=[new]
                  new_nr.sinapses=[]
                  if nrsub!= None:
                   new_nr.connect_to(new_nr.owner,nrsub,'defs')                   
                  nds2.append(new_nr)
                  if nr2 != None:
                   nr2.connect_to(nr2.owner,new_nr,'defs') 
                   rt.set_topico_nr(nr2)
                  else:  
                   node.set_topico_nr(new_nr)
                   rt.set_topico_nr(new_nr)
                  break
           #============================  
           for [lrco,nm] in layers_collect_s:
            for cmpl in verbo_compl: 
             verbo_compl2=compl.split('->') 
             a1=verbo_compl2[0].split('.')
             a2=verbo_compl2[1].split('.')
             for nr in lrco.topicos:
              old=a1[0]
              new=a2[0]
              if self.s_compare_dt(nr,old):
                if len(a1)==1  and len(a2)==1 :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  lrco.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
                elif len(a1)>1:
                 nr1=get_subt(a1[1:],nr)
                 if nr1 != None :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  lrco.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
        #=============================================================        
        if verbo=='vb_write': # cria um novo topico e clona o indicado
          # linkar objetos, 'referencia' -> 'identificador' em 'purpose' -> motivo
          def get_subt(arr,nr):
            if len(arr)==0 or nr == None:
              return nr
            consid=arr[0]
            arr=arr[1:]
            if consid in nr.dt:
              for k in nr.sinapses:
                r=get_subt(arr,k.nr)
                if r!= None:
                  return r
            return None    
          print 'Run.(',verbo,')->rnm'
          print 'Params:','[',verbo_compl,'],[', layers_collect,'],[',layers_collect_s,']' 
          if True:
           def get_dt_from_nr(lay,n):
             dt=n[1:]
             t=lay.get_topico(dt)
             r=''
             for s in t.sinapses:
               for d in s.nr.dt:
                 if r!='': r+=' '
                 r+=d
             return r     
           for compl  in verbo_compl: 
            verbo_compl1=compl.split('->') 
            a1=verbo_compl1[0].split('.')
            a2=verbo_compl1[1].split('.')
            old=a1[0]
            new=a2[0]
            if old[0]=='&': old=get_dt_from_nr(node,old)# referencia dado contido em outro topico
            if new[0]=='&': new=get_dt_from_nr(node,new)# referencia dado contido em outro topico
            for nr in node.topicos:
              if self.s_compare_dt(nr,old):
                if len(a1)==1  and len(a2)==1 :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  #==============
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  node.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
                elif len(a1)>1:
                 nr1=get_subt(a1[1:],nr)
                 nr2=None
                 if len(a2)>1:
                   nr2=get_subt(a2[1:],node.get_topico(new))
                 if nr1 != None :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  if nr2 != None:
                   nr2.connect_to(nr2.owner,new_nr,'defs') 
                   rt.set_topico_nr(nr2)
                  else:  
                   node.set_topico_nr(new_nr)
                   rt.set_topico_nr(new_nr)
                  break
           #============================  
           for [lrco,nm] in layers_collect_s:
            for cmpl in verbo_compl: 
             verbo_compl2=compl.split('->') 
             a1=verbo_compl2[0].split('.')
             a2=verbo_compl2[1].split('.')
             for nr in lrco.topicos:
              old=a1[0]
              new=a2[0]
              if self.s_compare_dt(nr,old):
                if len(a1)==1  and len(a2)==1 :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  lrco.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
                elif len(a1)>1:
                 nr1=get_subt(a1[1:],nr)
                 if nr1 != None :
                  print 'VB-write :',old,', to:',new
                  new_nr=nr.clone_nr(nr)
                  new_nr.dt=[new]
                  nds2.append(new_nr)
                  lrco.set_topico_nr(new_nr)
                  rt.set_topico_nr(new_nr)
                  break
        #=============================================================        
        if verbo=='vb_link':
          # linkar objetos, 'referencia' -> 'identificador' em 'purpose' -> motivo
          print 'Run.(',verbo,')'
          identif=[] # objects  
          referencia=[]
          purposes=[]
          foco=[]
          condition=[]
          for nr_t in _nds2:
              if self.s_compare_dt(nr_t,'identificador'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 if d == "$stack":
                   for [o,s] in layers_collect_s:
                    identif.append(o.name) 
                    if s != None:
                      if umisc.trim(s) != '':
                       identif.append(s)  
                 else:
                  identif.append(d)
              if self.s_compare_dt(nr_t,'orig'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 referencia.append(d)
              if self.s_compare_dt(nr_t,'opcode'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 purposes.append(d)
              if self.s_compare_dt(nr_t,'foco'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 foco.append(d)
              if self.s_compare_dt(nr_t,'condition'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 condition.append(d)
          
          print 'Params:',identif,referencia, purposes ,foco ,condition,have_collected   
          #####
          #have_collected=[]          
          #####
          for id in identif:
           ob=get_ontology( id,purpose,usr )
           print 'Onto:',ob
           for o in ob:
            ob2=[]
            if len(have_collected) == 0:
             for id2 in referencia:
              ob22 = get_ontology( id2,purpose,usr )
              print 'Onto-REF:',id2,purpose,usr,'->',ob22
              for o22 in ob22:
               ob2.append(o22)
            else:
              ob2=layers   
              print 'Collected used:',have_collected,layers
              for s in layers:
               print '[',s.name,']'
            for o2 in ob2:
              #=============
              if umisc.trim(o2.name) == ''  or umisc.trim(o2.name) == '__TST__' : continue
              focus_o=[]
              focus_d=[]
              linkds=False
              if len(foco) > 0 :
               for foc in foco :
                tps=o.get_topico(foc)
                foco_o.append(tps)
               for foc in foco :
                tps=o2.get_topico(foc)
                foco_d.append(tps)
               for p in purposes:
                print 'Process link(1):',o.name,o2.name
                o.set_link_ds(o2,p,foco_o,foco_d,condition) 
                linkds =True
              else:             
               for p in purposes:
                print 'Process link(2):',o.name,o2.name
                o.set_link_ds(o2,p,[],[],condition)                 
                linkds=True
              # save presistable
              if linkds:
               print 'Post linked layer:',o.name
               post_object_by_data_es(o,usr)
               #post_object_by_data_es(o2,usr) 
        #==========================        
        if verbo=='vb_link_nr':
          print 'Run.(',verbo,')'
          # linkar objetos, 'referencia' -> 'identificador' em 'purpose' -> motivo
          identif=[] # objects  
          referencia=[]
          purposes=[]
          for nr_t in _nds2:
              if self.s_compare_dt(nr_t,'identificador'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 if d == "$stack":
                   for [o,s] in layers_collect_s:
                    identif.append(o.name) 
                    if s != None:
                      if umisc.trim(s) != '':
                       identif.append(s)  
                 else:
                  identif.append(d)
              if self.s_compare_dt(nr_t,'reference'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 referencia.append(d)
              if self.s_compare_dt(nr_t,'purpose'):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 purpose2.append(d)
             
          for id in identif:
           ob=get_ontology( id,purpose,usr )
           for o in ob:
            for id2 in referencia:
             ob2 = get_ontology( id2,purpose,usr )
             for o2 in ob2:
              #=============
              nr1=o.get_all () # considerando filter ativo
              nr2=o2.get_all ()
              for n1 in nr1:
               for n2 in nr2:
                for p in purposes:
                 nr1.connect_to(n1.owner,n2,p)
             # persist layers
             #post_object_by_data_es(ob,usr): 
             #post_object_by_data_es(ob2,usr): 
          
            
        if verbo=='vb_behavior': #trabalha diretamente com extruturas mais complexas, como interval e limits
          # params: direct ->(time,quantity), indirect ->(quality)
          # interval : #confere entrada se esta dentro dos limits da rct de conferencia
          #   direct->verbo q enquadra caracts q estejam posicionada entre inicio->fim,fim->inicio  -- obs: devem ser elementos time ou numericos(ex:quantity)
          #   indirect-> posicionamento relativo de    
          if len(verbo_compl) >= 3:
           # verbo_compl[0]->qualiticador que esta guardardo a informacao no modelo[o qual deve ser procurado para comparar os limits]
           # verbo_compl[1]->qualificador que esta guardando a informacao no layer a ser conferido
           # verbo_compl[2]->segundo parametro de informacao , limite final
           # verbo_compl[3]->qualificador a ser gravado no stack e no node se estiver dentro dos limites
           ql1=verbo_compl[0]
           ql2=verbo_compl[1]
           ql22=verbo_compl[2]
           return_top=verbo_compl[3]
           number_oper=None
           if len(verbo_compl) >= 5:
            number_oper=verbo_compl[4]             
           print 'Run.(',verbo,')'          
           print 'Params:(',ql1,ql2,ql22,')'
           identif=[] # objects  
           qual1=[]
           qual2=[]
           for nr_t in _nds2:
              if self.s_compare_dt(nr_t,ql1):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 qual1.append(d)
              if self.s_compare_dt(nr_t,ql22):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 qual2.append(d)   
              if self.s_compare_dt(nr_t,ql2):
               for s in nr_t.sinapses:
                for d in s.nr.dt:
                 qual2.append(d)   
           stack_proc=[]    
           for l in layers:
             stack_proc.append(l)
           for [l,nm] in layers_collect_s:
              stack_proc.append(l)
           #==========================================   
           self.process_behavior(layers_collect_s,layer_Atu_r,node,stack_proc,node,qual1, qual2,return_top,number_oper,purpose,usr)
        #===============================             
        #===============================             
        #===============================             
        #===============================             
        #===============================             
        if verbo=='tstatic': 
          for snr_atu in nr_atu.sinapses:
           rt.set_topico_nr(snr_atu.nr) 
          continue
        '''========= 
        '''      
         
        fnd_comp=False
        indx=-1
        fixed_position_serialize=False
        if adm_serialize:
          for ad in adm_serialize_cmp:
            if ad == '$fixed-position':
             fixed_position_serialize=True
        for n_c in nds2:
          indx+=1
          if adm_serialize:
             if (adm_serialize_ind != -1) and not fixed_position_serialize:
              if indx < adm_serialize_ind:
               continue
             if fixed_position_serialize: # deve fixar sempre o inicio encontrado com a sequencia de tds os elementos
               if start_adm_serialize > -1:
                  if start_adm_serialize+start_adm_serialize_ind != index_nr_Atu:
                    continue
          #chk_i=self.compare_dt_depend(usr,purpose,nr_atu,n_c,['identificador'])
          [chk_i,nrs_refer]=self.compare_dt_dependf(usr,purpose,nr_atu,n_c,[])
          #print 'compare_dt_dependf:',[chk_i,nrs_refer]
          #chk_i=self.compare_dt_depend(usr,purpose,nr_atu,n_c,[])
          if chk_i:
           adm_serialize_ind=indx 
           if adm_serialize and fixed_position_serialize:
              if start_adm_serialize == -1:
               start_adm_serialize=indx
              else:
                if start_adm_serialize_ind == -1:
                   start_adm_serialize_ind=0
                else:   
                   start_adm_serialize_ind+=1
           fnd_comp=True
           nr_atu.connect_to_dyn_c(nr_atu.owner,n_c,'Compare-DYN') 
           # 
           ####node_cmps.append([nr_atu,nr_atu.mont_return_data2() ])
           node_cmps.append([nr_atu,n_c ])           
           ###
           rt.set_topico_nr(n_c)                                 
           #print 'compare:',nr_atu.dt,n_c.dt,' result:',fnd_comp   
           colect_gr.append([n_c,nr_atu]) 
           colect_gr2.append(nrs_refer)
        #//1    
        if fnd_comp:  
           if dmand : #  
            if neuron_base_c.cmandatory: 
             tps_len+=1           
           else:           
            tps_len+=1           
         ###rt.set_topico_nr(nr_atu)                                                 
        else:
         nore.set_topico_nr(nr_atu)        
         
         
      #=======================          
      
      '''
      tps=rt.topicos
      rt.topicos=[]
      for r in tps:
       r2=mdNeuron(r.owner)
       r2=r.mont_return_data(tps,foco) 
       if r2!=None:               
        rt.set_topico_nr(r2)
      '''
      if adm_return_val_call and len(adm_return_val_p_call)>0:
          [param,to_call,param2]=adm_return_val_p_call
          # rodar call rct  
          adm_return_val_pass=False
          if param == "$stack":
           if len(layers_collect_s) > 0 :
             adm_return_val_pass=True 
          if param == "$inverse":
           rt_compare=tps_len >= need_s 
           if not rt_compare:
             adm_return_val_pass=True 
          if param == "$emptystack":
           if len(layers_collect_s) > 0 :
             adm_return_val_pass=False
           else:  
             adm_return_val_pass=True            
          if param == "$empty":
             obs=param2
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                if len(o.topicos)==0 and len(o.nrs)==0:
                 adm_return_val_pass=True 
          if adm_return_val_pass:
            import mdER
            second_Rcts=mdER.get_RactionLines2C(to_call,usr)
            print 'adm_return_val_call.rcts(O) [',len(second_Rcts),']'
            print '......................................'
            for secd in second_Rcts:
             #print 'S:',s
             for lrcth in secd:
              if type(lrcth).__name__ == 'instance':
               if lrcth.__class__.__name__ == 'mdLayer':
                  print 'to call:adm_return_val_call',lrcth.name
                  ant_l=[]
                  for isk in layers:
                    ant_l.append(isk)
                  #l.process_Call_RCT(layers,ontology,usr,purpose,relactionate,have_collected)
                  lrcth.process_RCT(ant_l,ontology,usr,purpose,relactionate,have_collected,True )
                  #GlobalStack.proc_pg.call_process(usr,[l],True)
                  print lrcth.name,'-->called'
                  aj_param=[]
                   
                                    
                                     
                                      
      adm_return_val_pass=False
      secods_rcts_cl=[]
      if adm_return_val:
         # analiza se tem coleta ou se tem , para o caso de $stack
        if len(adm_return_val_p)>0:
          param = adm_return_val_p[0]
          param2=[]
          if len(adm_return_val_p)>1:
             param2=param[1:]              
          if param == "$stack":
           if len(layers_collect_s) > 0 :
             adm_return_val_pass=True 
          if param == "$emptystack":
           if len(layers_collect_s) > 0 :
             adm_return_val_pass=False
           else:  
             adm_return_val_pass=True
          if param == "$object":
           for obs in param2:
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                adm_return_val_pass=True 
          if param == "$pub":
           for obs in param2:
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                if len(o.published):
                 adm_return_val_pass=True           
          if param == "$top":
           for obs in param2:
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                if len(o.topicos):
                 adm_return_val_pass=True 
          if param == "$top2" and len(adm_return_val_p)>0:            
            if len(layers_collect_s)> 0 :
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                for s in o.topicos:
                 if adm_return_val_p[0] in s.dt :
                  adm_return_val_pass=True 
            else:
             for o in layers2:
              for obs in adm_return_val_p[1:]: 
                 for s in o.topicos:
                   if len(s.dt)==0: continue
                   if mdFuzzy.compare_n2(s.dt[0],obs,self.variancy,self.novariancy) > 0:
                      adm_return_val_pass=True 
                      break
             for o in nodes:
              for obs in adm_return_val_p[1:]: 
                 for s in o.lr.topicos:
                   if len(s.dt)==0: continue
                   if mdFuzzy.compare_n2(s.dt[0],obs,self.variancy,self.novariancy) > 0:
                      adm_return_val_pass=True 
                      break
              
          if param == "$empty":
           for obs in param2:
             for [o,d] in layers_collect_s:
               #if o.name == obs:
               if mdFuzzy.compare_n2(o.name,obs,self.variancy,self.novariancy) > 0:
                if len(o.topicos)==0 and len(o.nrs)==0:
                 adm_return_val_pass=True 
         
      print 'NEED:',need_s,',FOUND:',tps_len
      print 'NEED:',need_s2      
      if ((tps_len > 0 and tps_len >= need_s and not adm_return_val )or has_found_d )or (adm_return_val and adm_return_val_pass ) :
       ''' adicionar  os not inface , se tiver '''       
       nrs=[]
       nrs_static=node.get_all_static()       
       dt_ts=[]
       def dump_t(nr):
        r=[]
        r.append(nr.dt)
        r.append('sins-----')
        for d in nr.sinapses:
         i=dump_t(d.nr)
         for a in i:
          r.append(i)
        return r 
       for s in rt.topicos:
        s=dump_t(s)
        for a in s:
         dt_ts.append(a)
        
       print 'RCT->interface(1):',nrs,len(nrs),rt.topicos,'->>' #,dt_ts
       print 'RCT->STATIC:',nrs_static,',foco:',layers_collect_s
       for [ob_atu,vb] in layers_collect_s:
        for n in nrs_static: 
         for n2 in n.sinapses: 
          ob_atu.set_topico_nr(n2.nr)
        #print 'dump.coll...'
        #ob_atu.dump_layer() 
         
       #rt.dump_layer()
       if len(nrs) > 0:
        # so retorna os n interface.nos q tem interface declarada, usa eles p conferencia e complemento(find-path),etc... mas n retorna as interfaces
        rt.topicos=[]
       for n in nrs:
         rt.topicos.append(n)
       for n in nrs_static: 
         rt.topicos.append(n)
       '''adicionar no rtp'''
       for ts in rt.topicos:
        rtp.set_topico_nr(ts)
       #=====================
       rt=None   
       #print 'dump.rtp:'
       #rtp.dump_layer()
      else : 
         has_found_dn=False
    #====================================================     
    #====================================================     
    #====================================================     
    has_found_d=( has_found_d or has_found_dn  )
    if len(nodes) == 0 and len(rtp.topicos) <= 0:
      i_ls=layers
      #print 'Prepare to call-inter-rtc(1)[',self.name,']:' ,layers_collect_s,i_ls,layers_collect
      print 'Prepare to call-inter-rtc(1)[',self.name,']:' ,layers_collect_s
      #process_Call_RCT(self,data_c,ontology,usr,purpose,relactionate)
      if len(layers_collect_s) > 0 and len(i_ls) > 0:
       for [ll,top_s] in layers_collect_s:
        s2=ll.name
        lr=i_ls[0]
        lr.set_topico_aq([top_s,s2])
        
      if len(layers_collect) > 0:
       i_ls=layers_collect
       
      if len(layers_collect_s) > 0:
       if len(layers_collect) == 0:
        i_ls=[]
       for l in layers_collect_s:
        i_ls.append(l)
      for ass in i_ls:
       if ass  not in aj_param:
        aj_param.append( ass )      
      
    
    if ( len(rtp.topicos) > 0 and not adm_return_val )or (adm_return_val and adm_return_val_pass ) :
      print 'Call Layout-Code from:',self.name
      layout_codes=self.get_links('LAYOUT-CODE')
      #===
      i_ls=[rtp]
      #print 'Prepare to call-inter-rtc(2):' ,layers_collect_s,i_ls
      print 'Prepare to call-inter-rtc(2):' ,layers_collect_s
      if len(layers_collect_s) > 0 and len(i_ls) > 0:
       for [ll,top_s] in layers_collect_s:
        s2=ll.name
        print 'Call-layout-name:',s2,layers
        if len(layers) > 0 :
         lr=layers[0]        
         lr.set_topico_aq([top_s,s2])   
         i_ls.append(lr) 
      #====================================  
      if len(layers_collect) > 0:
       i_ls=layers_collect   
       
      if len(layers_collect_s) > 0:
       if len(layers_collect) == 0:
        i_ls=[]
       for l in layers_collect_s:
        i_ls.append(l)
      #================
      nms=[]      
      for d in layout_codes:
       nms.append(d.lr.name)
      print 'Prepare to call layouts-code---{',nms,'}'
      rtps=[rtp]
      for d in layers_collect:
       rtps.append(d)
      for d in layers_collect_s:
       rtps.append(d[0])
      print 'Collect objects:',layers_collect,layers_collect_s 
      prepare_search_customlayouts(self,nms,rtps,usr,relactionate) #purposes,dts,usr
      #layers_collect = []
      #layers_collect_s =[]
      print 'layouts-code executed. ---'
      #print 'Lr:n',rtp.name
      #===
      #process_Call_RCT(self,data_c,ontology,usr,purpose,relactionate)
      #======================================== 
      for ass in i_ls:
       if ass  not in aj_param:
        aj_param.append( ass )      
      
      #self.process_Call_RCT(i_ls,ontology,usr,purpose,relactionate)
      for s in dyned:
       s.release_din_sinapse()
      for s in filtered:
         s.release_filter ()
      aj_return.append( [rtp,nore] )
      
      continue
    for s in filtered:
     s.release_filter ()
    for s in dyned:
     s.release_din_sinapse()
    #aj_return.append( [None,None] )
    continue 
    #====    
   if len(get_fct_links()) == 0: # se rct fazia e sem nodes, pode ser entry point
    print 'Process call empty rct.'   
    #self.e_layers_collect_s=layers_collect_s
    self.process_Call_RCT([],ontology,usr,purpose,relactionate,have_collected)
     
   if (len(aj_param) and adm_return and not adm_return_val )or (adm_return_val and adm_return_val_pass ) :
    #print 'layers:',layers  
    if aj_c:
     for a in aj_param:
      if type(a) == type([]):
       if a[0] not in layers :
        layers.append(a[0])             
      else:
       if a not in layers:
        layers.append(a)
    #print 'aj_param:',layers  
    #print 'aj_param(2):',aj_param
    print 'Process call lrs len(',len(aj_param),')'
    #self.e_layers_collect_s=layers_collect_s
    self.process_Call_RCT(aj_param,ontology,usr,purpose,relactionate,have_collected)
   if len(aj_return):
    al1=mdLayer()
    al2=mdLayer()
    print 'aj_return:',aj_return
    for [l1,l2] in aj_return:
      for a1 in l1.topicos:
       al1.set_topico_nr(a1)
    for [l1,l2] in aj_return:
      for a2 in l2.topicos:
       al2.set_topico_nr(a2)
    #========================== 
    self.variancy=[]
    self.novariancy=[]
    return [al1,al2] 
   return [None,None]    
    
  def process_raw_RCT(self,layers,ontology,usr,purpose,relactionate=False): # nao depende de links facts, executa compare em si mesma
    rtp=mdLayer ()
    nore=mdLayer ()
    nodes=[self]
    idcx=0
    for node in nodes:
     node=node.lr
     #=========
     nds=node.get_all ()
     foco=node.get_all_f ()
     print 'NDS:'
     for n1 in nds:
      print n1.dt
     print '----------'
     nds2=self.get_all_it(node.name,usr)
     #==========  
        
     need_s=0
     for n1 in nds:
      if self.s_compare_dt(n1,'source') or self.s_compare_dt(n1,'destination'): 
       pass
      else: 
       need_s+=1 
     #==========    
     tps_len=0
     for l in layers:
      nds2=l.get_all ()
      print 'NDS2:'
      for n1 in nds2:
       print n1.dt
      print '-----------'
      tps_len=0
      rt=mdLayer ()
      for nr_atu in nds:
        '''========= 
        ''' 
        nr_atuc=[]
        fnd_comp=False
        for n_c in nds2:
          #chk_i=self.compare_dt_depend(usr,purpose,nr_atu,n_c,['identificador'])
          chk_i=self.compare_dt_depend(usr,purpose,nr_atu,n_c,[])
          if chk_i:
           fnd_comp=True
        if fnd_comp:
          tps_len+=1  
          for kd in nr_atuc:
           nrs_fnds.append(kd)
          rt.set_topico_nr(nr_atu)     
        else:
         nore.set_topico_nr(nr_atu)        
      #=======================
      '''
      tps=rt.topicos
      rt.topicos=[]
      for r in tps:
       r2=mdNeuron(r.owner)
       r2=r.mont_return_data(tps,foco) 
       if r2!=None:
        rt.set_topico_nr(r2)
      '''  
      print 'NEED:',need_s,',FOUND:',tps_len
      if tps_len >= need_s :
       ''' adicionar  os not inface , se tiver '''       
       nrs=[]
       print 'RCT->interface(2):',nrs,len(nrs),rt.topicos
       if len(nrs) > 0:
        # so retorna os n interface.nos q tem interface declarada, usa eles p conferencia e complemento(find-path),etc... mas n retorna as interfaces
        rt.topicos=[]
       for n in nrs:
         rt.topicos.append(n)
       '''adicionar no rtp'''
       for ts in rt.topicos:
        rtp.set_topico_nr(ts)
       #=====================
       rt=None       
      else : pass
    if len(rtp.topicos) > 0 :
      layout_codes=self.get_links('LAYOUT-CODE')
      #===
      nms=[]      
      for d in layout_codes:
       nms.append(d.lr.name)
      print 'Prepare to call layouts-code---{',nms,'}'
      prepare_search_customlayouts(self,nms,[rtp],usr,relactionate) #purposes,dts,usr
      print 'layouts-code executed. ---'
      #===
      return [rtp,nore]
    return None
    #====      
  

    
    
    