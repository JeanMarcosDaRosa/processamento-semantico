#!/usr/bin/env python

#
#	Client that doesn't use the Name Server. Uses PYROLOC:// URI.
#

start_chave=0


import sys
import Pyro.core
import threading
import time 
import logging
import umisc

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('DYNAMIC-CODE-ENGINE')



import thread

processo_start=0

 
#sys.path.append('./pymongo')  

import pymongo

#cassandra
import pycassa
from pycassa.pool import ConnectionPool
from pycassa import index
from pycassa.columnfamily import ColumnFamily


pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10000)
web1=pycassa.ColumnFamily(pool2, 'web_cache15')
fcb=pycassa.ColumnFamily(pool2,'to_posting2')
webm=pycassa.ColumnFamily(pool2, 'web_cache1')
webm10=pycassa.ColumnFamily(pool2, 'web_cache10')

#hypertable
#import hyper_tb

 
import socket   
import simplejson as json
 
import friends13C321_d


#=============================================================================
MONGO_URL='mongodb://mdnet1:acc159753@localhost:27017/mdnet'
connM = pymongo.Connection(MONGO_URL) 
dbM=connM.mdnet
chaves = dbM['chaves']
#web1=dbM['web_cache']
#=============================================================================
mongo_arr=[web1]
mongo_arr2=[fcb]


def carrega_chaves():
    global start_chave
    global chaves
    ind=0
    friends13C321_d.tokens=[]
    #==
    while len(friends13C321_d.tokens) <= 150:
       key=str(start_chave+1)
       token_=chaves.find_one({'key':key})['token']
       friends13C321_d.tokens.append(token_)
       
carrega_chaves()
 
def get_feeds(u,itsc,itsc2,th,arrc1,uip,u2,indc ):
 if True:
     global processo_start
     processo=1+(indc*10)
     #=================
     cmd=''+str(u)+','+str(uip)+','+str(u2)+','+str(processo)+','
     return cmd


     
     
cache_pg=[]  
atu_index=0
pools=[]
mongos=[]
pusuarios=[] 
 
def get_by_u():
  users=[]
  r1=fcb.get_range()   
  for ky,col in r1:
          indx='N' 
          try:
             indx=col['indx1']
          except:
             indx='N' 
          if indx =='S'  :
               continue
          #col['indx1']='S'
          #fcb.insert(ky,col)
          from_id3=col['from_id']
          from_id3=from_id3.replace('\'','')
          from_id3=from_id3.replace('[','')
          from_id3=from_id3.replace(']','')
          from_id2=from_id3.split(',' )
          from_id=''
          for ifr in from_id2:
               ifr=umisc.trim(ifr)
               if len(ifr) > 1:
                    if ifr[0] in ['0','1','2','3','4','5','6','7','8','9'] and ifr[1] in ['0','1','2','3','4','5','6','7','8','9']:
                         from_id=ifr 
                         break
          # get user from msg id
          try:
             r1=webm.get(from_id)
          except:
            try:
              r1=webm10.get(from_id)
            except:
              print 'msg_id not found:',from_id
              continue  
          from_id=r1['id_usr']
          if from_id not in pusuarios: 
               print 'collect user:',from_id
               users.append([from_id,from_id,[[ky,col,fcb]]])
               pusuarios.append(from_id)
          else:
            idh=0
            while idh < len(pusuarios):
               if from_id == pusuarios[idh]:
                 users[idh][2].append([ky,col,fcb])
                 break
               idh+=1                 
  return users	    
    
def pre_get_pages(first=False): 
  global cache_pg
  global atu_index
  cache_pg=[]
  atu_index=0
  fnd=False
  curs=get_by_u()
  for cr in curs:
   fnd=True 
   cache_pg.append(cr)
  
   
     
     
def get_pages(i,a):
 arrpg=[]
 global atu_index
 global cache_pg
 if atu_index == 0:
   pre_get_pages()
   
 print 'Getting pages...'
 while atu_index < len(cache_pg):
  cr=cache_pg[atu_index]
  #================================
  important='E'
  #print 'Get-ID:',cr
  #rt=fcb.get(cr[0])
  important='E'  
  #===========  
  arrpg.append([cr[0],cr[0],important,cr[2]])
  atu_index+=1
  if len(arrpg) >= 10: break
 #============================== 
 return arrpg
  
import time
from threading import Thread

class thread_cntl:
 def __init__(self):
  self.finished=False


class connectionwb2:
 def __init__(self,_webs):
   self.webs=_webs
   self.atu=0
   self.lock=threading.Lock()
 def reset(self):
   self.atu=0
 def test_get(self,val):  
    for aw in self.webs:
     r=aw.find({'id':val['id']}) 
     for a in r:
      return True
    return False      
 def insert(self,ky,val):
  print 'insert:',val['id']
  with self.lock: 
   try:
      #if not self.test_get(val):
      r=self.webs[self.atu].insert(ky,val)     
   except Exception,err :   #  
      try:
       log.exception("")   
      except: pass 
   except Exception :     
    log.exception("Error scann...") 
    return None 


class connectionwb:
 def __init__(self,_webs):
   self.webs=_webs
   self.atu=0
   self.lock=threading.Lock()
 def reset(self):
   self.atu=0
 def test_get(self,val):  
    for aw in self.webs:
     r=aw.find({'id':val['id']}) 
     for a in r:
      return True
    return False      
 def insert(self,val):
   print 'connectionwb.insert:',val['id']
   with self.lock:
    try:
      #if not self.test_get(val):
      r=self.webs[self.atu].insert(val,w=1)     
    except pymongo.errors.OperationFailure,err :   #  
      try:
       log.exception("")   
      except: pass 
      errr=str(err)
      print 'get.exception-------------[',errr,']'      
      if 'quota exceeded' in errr:
       print 'Quota FULL************'
       print 'change table.....from:',self.atu,' to:',self.atu+1
       self.atu+=1
       if self.atu >= len(self.webs):
         self.atu=0
         return self.webs[self.atu]
       return self.webs[self.atu].insert(val,w=1)
      else: return None 
    except Exception : 
       log.exception("Error scann...") 
  
def run_cmd(params,thr1,_pool,_mongo,important,pab):
 [cass_pool,cass_fcb]=_pool
 [_mongo_database,mongo_fcb,mongo_web]=_mongo 
 try:
  #print 'pg.run:',params,cass_fcb,mongo_fcb,mongo_web
  rt=friends13C321_d.entry(params,cass_fcb,mongo_fcb,mongo_web,important,pab)
  
 except: 
  log.exception("run.exception***********")
 thr1.finished=True
  
#====================  
connw=connectionwb2(mongo_arr)
connw2=connectionwb(mongo_arr2)
for i in range(0,15):
 fcb2_1=connw2
 pool2_1=None
 #cassandra
 #pool2_1 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10000000)
 #fcb2_1 = pycassa.ColumnFamily(pool2_1, 'fcb_users3')
 #hyperable
 #fcb2_1=hyper_tb.hypconnection()
 #fcb2_1.open()
 #=================================
 pools.append([pool2_1,fcb2_1]) 
 #======================
 web=connw
 #=========
 mongos.append([connM,fcb,web]) 
  
def process_p(arrpg1,dm):
 print 'pages ok. init process ... len:',len(arrpg1),',len2:',len(pools)
 thst=[]
 indc=0
 for pg in arrpg1:
   [u,uip,important,pab]=pg
   thst.append(thread_cntl())
   params=get_feeds(u,0,'',None,[],uip,u,indc)
   #==========================
   t = Thread(target=run_cmd, args=(params,thst[len(thst)-1],pools[indc],mongos[indc],important,pab))
   t.start()
   indc+=1

 while True:
  fndac=False
  for t in thst:
   if not t.finished: fndac=True
  time.sleep(.5)
  if not fndac: break  
 
import time 

import sys 
param=int(sys.argv[1])
processo_start=param


while True:  
 #
 try:
  print 'get.tokens...'
  try:
   carrega_chaves()
  except:
   time.sleep(10)
   continue
  print 'get.pages.to.process..'
  a1=get_pages(1,1)
  #
  print 'protess.pages.'
  process_p(a1,0 )
  #
  print 'all.pgs.processed.'
 except: 
  log.exception("")
  time.sleep(10)
 print 'Sleep...'
 time.sleep(2)

 
 
 
 
 
 

     
