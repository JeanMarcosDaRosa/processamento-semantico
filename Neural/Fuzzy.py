# 2015.02.01 16:26:32 BRST
# Embedded file name: src/Fuzzy.py
import time, datetime
import umisc
import sys
import gradiente

def latinupper(string):
    return string
    #return string.decode('latin-1').upper().encode('latin-1')


class mdFz:

    def __init__(self, prefixo, sufixo, define, returnsa):
        prefix = []
        defs = []
        sfx = []
        for p in prefixo:
            prefix.append(latinupper(p))

        for d in define:
            if umisc.trim(d) != '':
                defs.append(latinupper(d))

        for sf in sufixo:
            sfxc = []
            for sf2 in sf:
                sfxc.append(latinupper(sf2))

            sfx.append(sfxc)

        self.prefixo = prefix
        self.define = defs
        self.sufixo = sfx
        self.returns = returnsa


class mdFuzzy:

    def __init__(self):
        self.fzSent = []
        self.name = ''
        self.cache_cmp = []
        self.cache_cmp_returns = []
        self.cache_tmp_ref = []
        self.data_h = False
        self.data_stp = False

    def cache(self):
        fzsC = self.fzSent
        self.cache_tmp_ref = []
        for fzs in fzsC:
            st = fzs
            st2c = []
            prefixo = st.prefixo
            sufixo = st.sufixo
            define = st.define
            returns = st.returns
            for kd in returns:
                if 'stop' in kd:
                    self.data_stp = True
                    self.data_h = True

            for pr in prefixo:
                cmp_ = pr
                for df in define:
                    cmp = cmp_ + df
                    self.cache_cmp.append(latinupper(cmp))
                    self.cache_cmp_returns.append(returns)
                    st2c.append(latinupper(cmp))
                    d1 = cmp.lower()
                    if d1 in ('#month', '#day', '#year', '#time', '#date', '#datetime', '#number'):
                        self.data_h = True
                    is1 = 0
                    while len(sufixo) > is1:
                        sufd = sufixo[is1]
                        is1 += 1
                        for sufd2 in sufd:
                            is2 = 0
                            while len(sufixo) > is2:
                                suf = sufixo[is2]
                                is2 += 1
                                if is2 > is1:
                                    for suf2 in suf:
                                        cmp2 = cmp + sufd2 + suf2
                                        self.cache_cmp.append(latinupper(cmp2))
                                        self.cache_cmp_returns.append(returns)
                                        st2c.append(latinupper(cmp2))
                                        d1 = cmp2.lower()
                                        if d1 in ('#month', '#day', '#year', '#time', '#date', '#datetime', '#number'):
                                            self.data_h = True

            self.cache_tmp_ref.append([st2c, fzs])

    def set_fz(self, arr, c, refer, force_position, arround, snret, direction, f_an, breaks):
        for ar in arr:
            prefixo, defs, sufixo = ar
            define, returns = defs
            self.fzSent.append(mdFz(prefixo, sufixo, define, returns))

        self.mandatory = c
        self.an = f_an
        self.refer = refer
        self.arround = arround
        self.force_position = force_position
        self.sn_ret = snret
        self.direction = direction
        self.breaks = breaks
        self.cache()

    def valida_arround(self, idx_atu, dt_arr):
        if len(self.arround) <= 0:
            return True
        ind = 0
        fnds = 0
        ind_a = 1
        tp = self.arround[0]
        if tp == '0':
            for d in dt_arr:
                if ind <= idx_atu:
                    if len(self.arround) > ind_a:
                        id = self.arround[ind_a]
                        if d.upper() == id.upper():
                            fnds += 1
                            ind_a += 1
                ind += 1

            if fnds >= len(self.arround) - 1:
                return True
        if tp == '1':
            for d in dt_arr:
                if ind >= idx_atu:
                    if len(self.arround) > ind_a:
                        id = self.arround[ind_a]
                        if d.upper() == id.upper():
                            fnds += 1
                            ind_a += 1
                ind += 1

            if fnds >= len(self.arround) - 1:
                return True
        return False

    def valida_arround2(self, idx_atu, dt_arr):
        if len(self.arround) <= 0:
            return True
        ind = 0
        fnds = 0
        ind_a = 1
        tp = self.arround[0]
        if tp == '0':
            for d in dt_arr:
                if ind <= idx_atu:
                    if len(self.arround) > ind_a:
                        id = self.arround[ind_a]
                        if d[0].upper() == id.upper():
                            fnds += 1
                            ind_a += 1
                ind += 1

            if fnds >= len(self.arround) - 1:
                return True
        if tp == '1':
            for d in dt_arr:
                if ind >= idx_atu:
                    if len(self.arround) > ind_a:
                        id = self.arround[ind_a]
                        if d[0].upper() == id.upper():
                            fnds += 1
                            ind_a += 1
                ind += 1

            if fnds >= len(self.arround) - 1:
                return True
        return False

    def compare_break(self, dt):
        for b in self.breaks:
            if b == dt:
                return True

        return False

    def process2(self, dt_k, dt_arr, indi):
        dt = dt_k[0]
        rts_nr = []
        if len(self.fzSent) <= 1 or self.an.upper() == 'OR':
            if len(self.fzSent) <= 1:
                r = self.compare(dt)
                if r:
                    if self.valida_arround2(indi, dt_arr):
                        if len(self.fzSent[0].returns) > 0:
                            ret = []
                            for r in self.fzSent[0].returns:
                                if r[1] != '$$data$$':
                                    ret.append([r[0],[r[1]],-1,self.refer,[r[2]]])
                                else:
                                    ret.append([r[0],[r[1]],-1,self.refer,[r[2]]])

                            rts_nr.append([dt_k[1], dt_k[2]])
                            return [ret, rts_nr]
                        else:
                            return [[[self.name,[],-1,self.refer,self.sn_ret]], rts_nr]
                    else:
                        return [[], []]
            else:
                r, posic = self.compare2(dt)
                if r:
                    if self.valida_arround2(indi, dt_arr):
                        if len(self.fzSent) > posic:
                            ret = []
                            r = self.fzSent[posic].returns
                            if r[1] != '$$data$$':
                                ret.append([r[0],[r[1]],-1,self.refer,[r[2]]])
                            else:
                                ret.append([r[0],[r[1]],-1,self.refer,[r[2]]])
                            rts_nr.append([dt_k[1], dt_k[2]])
                            return [ret, rts_nr]
                        else:
                            return [[[self.name,[],-1,self.refer,self.sn_ret]], rts_nr]
                return [[], []]
        elif len(self.fzSent) > 1:
            ind_p = 0
            ind_fz = 0
            fnd = 0
            rts = []
            ult_p = -1
            fz2 = []
            ind_first_fnd = False
            while ind_p < len(dt_arr):
                if ind_p >= indi:
                    if ind_fz < len(self.fzSent):
                        fz = self.fzSent[ind_fz]
                        if self.compare_break(dt_arr[ind_p][0]):
                            break
                        if self.compare(dt_arr[ind_p][0], fz, ind_fz):
                            ind_fz += 1
                            fnd += 1
                            rts.append(dt_arr[ind_p][0])
                            if ind_first_fnd:
                                ult_p = ind_p
                            ind_first_fnd = True
                            fz2.append([fz, ind_p])
                        else:
                            if not ind_first_fnd:
                                return [[], []]
                            if ind_first_fnd:
                                if self.force_position:
                                    return [[], []]
                ind_p += 1

            if len(fz2) >= len(self.fzSent):
                fc = False
                ret = []
                has_r = False
                for fz, ind_p2 in fz2:
                    if len(fz.returns) > 0:
                        has_r = True

                for fz, ind_p2 in fz2:
                    if self.valida_arround2(indi, dt_arr):
                        fc = True
                        if len(fz.returns) > 0:
                            for r in fz.returns:
                                ret.append([r[0],[r[1]],ult_p + 1,self.refer,[r[2]],ind_p2])
                        elif not has_r:
                            ret.append([self.name,rts,ult_p + 1,self.refer,self.sn_ret,ind_p2])
                        else:
                            ret.append([self.name,rts,ult_p + 1,self.refer,self.sn_ret,ind_p2,0])
                        rts_nr.append([dt_arr[ind_p2][1], dt_arr[ind_p2][2]])
                    else:
                        return [[], []]
                if fc:
                    return [ret, rts_nr]
        return [[], []]

    def mandatory_by_return(self, sent_ind):
        sent_f = False
        for s in self.fzSent:
            if len(s.returns) > 0:
                sent_f = True

        if not sent_f:
            return self.mandatory
        if len(self.fzSent) <= sent_ind:
            return False
        rts = self.fzSent[sent_ind].returns
        rtsb = False
        if len(rts) > 0:
            rtsb = True
        return rtsb

    def process(self, dt, dt_arr, indi):
        if len(self.fzSent) <= 1 or self.an.upper() == 'OR':
            if len(self.fzSent) <= 1:
                r = self.compare(dt)
                if r:
                    if self.valida_arround(indi, dt_arr):
                        if len(self.fzSent[0].returns) > 0:
                            ret = []
                            for r in self.fzSent[0].returns:
                                r3 = None
                                if len(r) > 3:
                                    r3 = r[3]
                                if r[1] != '$$data$$':
                                    ret.append([r[0],[r[1]],-1,self.refer,[r[2]],-1,r3])
                                else:
                                    ret.append([r[0],[dt],-1,self.refer,[r[2]],-1,r3])

                            ret.append([self.name,[],-1,self.refer,self.sn_ret])
                            return ret
                        return [[self.name,[],-1,self.refer,self.sn_ret]]
                    else:
                        return []
            else:
                r, posic = self.compare2(dt)
                if r:
                    if self.valida_arround(indi, dt_arr):
                        if len(self.fzSent) > posic or len(self.cache_cmp) > 0 and len(self.cache_cmp) > posic:
                            ret = []
                            r = None
                            if len(self.cache_cmp) > 0:
                                r = self.cache_cmp_returns[posic]
                            else:
                                r = self.fzSent[posic].returns
                            for r1 in r:
                                r3 = None
                                if len(r1) > 3:
                                    r3 = r1[3]
                                if r1[1] != '$$data$$':
                                    ret.append([r1[0],[r1[1]],-1,self.refer,[r1[2]],-1,r3])
                                else:
                                    ret.append([r1[0],[dt],-1,self.refer,[r1[2]],-1,r3])
                            ret.append([self.name,[],-1,self.refer,self.sn_ret])
                            return ret
                        return [[self.name,[],-1,self.refer,self.sn_ret]]
                return []
        elif len(self.fzSent) > 1:
            ind_p = 0
            ind_fz = 0
            fnd = 0
            rts = []
            ult_p = -1
            fz2 = []
            ind_first_fnd = False
            deb_break = False
            while ind_p < len(dt_arr):
                if ind_p >= indi:
                    if ind_fz < len(self.fzSent):
                        fz = self.fzSent[ind_fz]
                        if self.compare_break(dt_arr[ind_p]):
                            deb_break = True
                            break
                        fsf = self.compare(dt_arr[ind_p], fz, ind_fz)
                        if fsf:
                            ind_fz += 1
                            fnd += 1
                            rts.append(dt_arr[ind_p])
                            if ind_first_fnd:
                                ult_p = ind_p
                            ind_first_fnd = True
                            fz2.append([fz, ind_p, dt_arr[ind_p]])
                        else:
                            if not ind_first_fnd:
                                return []
                            if ind_first_fnd:
                                if self.force_position:
                                    return []
                ind_p += 1

            if len(fz2) >= len(self.fzSent):
                fc = False
                ret = []
                for fz, ind_p2, dt_atu in fz2:
                    if self.valida_arround2(indi, dt_arr):
                        fc = True
                        if len(fz.returns) > 0:
                            for r in fz.returns:
                                r3 = None
                                if len(r) > 3:
                                    r3 = r[3]
                                if r[1] != '$$data$$':
                                    ret.append([r[0],[r[1]],ult_p + 1,self.refer,[r[2]],ind_p2,r3])
                                else:
                                    ret.append([r[0],[dt_atu],ult_p + 1,self.refer,[r[2]],ind_p2,r3])

                        else:
                            ret.append([self.name,[],ult_p + 1,self.refer,self.sn_ret,ind_p2,''])
                    else:
                        return []

                if fc:
                    return ret
        return []

    def compare_Date_del(self, t_format, dt, ex = ''):
        rt = False
        try:
            if ex != '':
                dt = dt.replace(ex, '')
            r = datetime.datetime.fromtimestamp(time.mktime(time.strptime(dt, t_format)))
            return True
        except:
            pass

        return False

    def compare_data_h(self, dt1, dt2):
        d1 = dt1.lower()
        d2 = dt2.lower()
        if d1 == '#number':
            fs = True
            try:
                ts = float(d2)
            except:
                fs = False

            return fs
        if d1 == '#month':
            if d2 in ('janeiro', 'fevereiro', 'mar\xe7o', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'):
                return True
            if d2 in ('jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'):
                return True
            if d2 in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'):
                return True
            if d2 in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'):
                return True
        if d1 == '#day':
            try:
                if int(d2) in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31):
                    return True
            except:
                pass

        if d1 == '#year':
            try:
                if int(d2) > 0:
                    return True
            except Exception as e:
                pass

        if d1 == '#time':
            try:
                if self.compare_Date_del('%H:%M:%S', d2):
                    return True
                if self.compare_Date_del('%H:%M', d2):
                    return True
            except:
                pass

        if d1 == '#date':
            try:
                if self.compare_Date_del('%Y-%m-%d', d2):
                    return True
            except:
                pass

        if d1 == '#datetime':
            try:
                if self.compare_Date_del('%Y-%m-%d %H:%M:%S', d2):
                    return True
            except:
                pass

            return False

    def compare(self, dt, fz = None, fzind_k = 0):
        fzs = None
        dt = latinupper(dt)
        cach = self.cache_cmp
        if len(self.cache_cmp) > 0:
            if fz == None:
                if dt in self.cache_cmp or '$$ALL$$' in self.cache_cmp:
                    return True
                if gradiente.compare_arr(dt, cach):
                    return True
                if not self.data_h:
                    return False
            else:
                fd2 = self.cache_tmp_ref[fzind_k]
                if dt in fd2[0] or '$$ALL$$' in fd2[0]:
                    return True
                if gradiente.compare_arr(dt, fd2[0]):
                    return True
                if self.data_h:
                    for cmp in fd2[0]:
                        if self.compare_data_h(cmp, dt):
                            return True

                elif not self.data_h:
                    return False
        if fz:
            fzs = fz
        elif len(self.fzSent) > 0:
            fzs = self.fzSent[0]
        if True:
            st = fzs
            prefixo = st.prefixo
            sufixo = st.sufixo
            define = st.define
            returns = st.returns
            for pr in prefixo:
                cmp_ = pr
                for df in define:
                    cmp = cmp_ + df
                    if self.compare_data_h(cmp, dt):
                        return True
                    rs = dt == cmp or cmp.lower() == '$$all$$'
                    if rs:
                        return True
                    is1 = 0
                    while len(sufixo) > is1:
                        sufd = sufixo[is1]
                        is1 += 1
                        for sufd2 in sufd:
                            is2 = 0
                            while len(sufixo) > is2:
                                suf = sufixo[is2]
                                is2 += 1
                                if is2 > is1:
                                    for suf2 in suf:
                                        cmp2 = cmp + sufd2 + suf2
                                        if dt == cmp2:
                                            return True

        return False

    def compare2(self, dt, fz = None):
        indc = 0
        dt = latinupper(dt)
        for fzs in self.fzSent:
            st = fzs
            if len(self.cache_cmp) > 0:
                if fz == None:
                    chs = self.cache_cmp
                    rtind, indcc = gradiente.in_nc(dt, chs)
                    if rtind > 0 or '$$ALL$$' in self.cache_cmp:
                        indc = indcc
                        return [True, indc]
                    if not self.data_h:
                        return [False, 0]
                else:
                    fd2 = self.cache_tmp_ref[fzind_k]
                    if dt in fd2[0] or '$$ALL$$' in fd2[0]:
                        return [True, indc]
                    if not self.data_h:
                        return [False, 0]
            prefixo = st.prefixo
            sufixo = st.sufixo
            define = st.define
            for pr in prefixo:
                cmp_ = pr
                for df in define:
                    cmp = cmp_ + df
                    if dt == cmp or cmp.lower() == '$$all$$':
                        return [True, indc]
                    if self.compare_data_h(cmp, dt):
                        print 'cmp-dt(2):', cmp, dt
                        return [True, indc]
                    is1 = 0
                    while len(sufixo) > is1:
                        sufd = sufixo[is1]
                        is1 += 1
                        for sufd2 in sufd:
                            is2 = 0
                            while len(sufixo) > is2:
                                suf = sufixo[is2]
                                is2 += 1
                                if is2 > is1:
                                    for suf2 in suf:
                                        cmp2 = cmp + sufd2 + suf2
                                        if dt == cmp2:
                                            return [True, indc]

            indc += 1

        return [False, 0]
# okay decompyling Fuzzy.pyc 
# decompiled 1 files: 1 okay, 0 failed, 0 verify failed
# 2015.02.01 16:26:33 BRST
