# coding: latin-1
''' controla a aquisicao de dados extruturados, bem como validacao dos mesmos '''

import sys

import mdNeural
import umisc

# tipos de ER

import mdTb

import pycassa
from pycassa.pool import ConnectionPool
from pycassa import index
from pycassa.columnfamily import ColumnFamily 


from operator import itemgetter, attrgetter


def get_src_by_obj2(obj,usr): 
 obj_nm=obj 
 dts=[]
 # 
 group=False
 indl=0
 while True:  
      indl+=1
      try:
        resultSet=mdTb.tb_object_dt1.get(obj+"|"+str(indl))
      except: break  
      DT=resultSet[u'datach'] 
      TOP=resultSet[u'topico']
      if TOP == 'fzlayout': # usado para fins de marca��o de prop�sito
       dts.append( [DT,0,group] )
       #print 'FZ:',[DT,0,group]
      if TOP == 'group': 
       group=True
 return dts
 
def get_src_by_obj22(obj,usr): 
 obj_nm=obj 
 dts=[]
 #-----------
 lev=0
 group=False
 #for results in resultSet:  
 indkl=0
 while True: 
      indkl+=1
      try:
       results=mdTb.tb_object_dt1.get(obj+"|"+str(indkl)) 
      except: break 
      DT=results[u'datach'] 
      TOP=results[u'topico']
      if TOP == 'fzlayoutCh': # usado para fins de marca��o de prop�sito
       dts.append( [DT,lev,group] )
      if TOP == 'runlevel': 
        antlev=lev
        try:
         lev=int( umisc.trim(DT) )
        except: 
         lev=antlev
      if TOP == 'group': 
       group=True
 return dts
 
  
 
def get_src_by_obj23(obj,usr): 
 obj_nm=obj 
 dts=[]
 #-----------
 #sql='select DT,TOPICO from SEMANTIC_OBJECT_DT where OBJECT=? and USERNAME = ? order by i'
 resultSet = conn4.sqlX (sql,([obj,usr]))
 #for results in resultSet:  
 indkl=0
 while True:
      indkl+=1
      try:
       results=mdTb.tb_object_dt1.get(obj+"|"+str(indkl)) 
      except: break 
      DT=results[u'datach'] 
      TOP=results[u'topico']
      if TOP == 'source': # usado para fins de marca��o de prop�sito
       dts.append( DT )
 return dts
  
 
def collect_objs_fct2(ob,usr):
  cats_r=[]        
  #  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(ob))
  cl5 = index.create_index_expression(column_name='opcode', value='FACT')
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   rs=get_src_by_obj2(ido,usr)
   for [r,lev,grp] in rs:
    if umisc.trim(r) != '':
     cats_r.append([r,0,grp])
  return cats_r
 
 
def collect_objs_fct22(ob,usr):
  cats_r=[]
  #  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(ob))
  cl5 = index.create_index_expression(column_name='opcode', value='FACT')
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   rs=get_src_by_obj22(ido,usr)
   for [r,lev,grp] in rs:
    if umisc.trim(r) != '':
     cats_r.append([r,lev,grp])
  return cats_r

def collect_objs_fct23(ob,usr):
  cats_r=[]
  #  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(ob))
  cl5 = index.create_index_expression(column_name='opcode', value='FACT')
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   rs=get_src_by_obj23(ido,usr)
   for r in rs:
    if umisc.trim(r) != '':
     cats_r.append(r)
  return cats_r

  
def load_ractionlines(usr,global_er,global_dts,e_purpose):
 #lay=[name,[topicos],[links]]
 #formato de leitura dos onto-object:
 '''
 object:
 topico destinacao->destinacao{ex:pessoa-classificacao}
 topico->dt[sinapse]
 ^
 |
 [topico{sub-topico}]->dt[sinapse]
 '''
 #resultSet = conn.sql ("SELECT DT,OBJECT,topico FROM SEMANTIC_OBJECT_DT where username ='"+usr+"' and topico = 'destination' order by OBJECT ")
 objs=[]
 ant=None
 caracts=[]
 #for results in resultSet:
 cl4 = index.create_index_expression(column_name='topico', value='destination')
 clausec = index.create_index_clause([cl4],count=1000000)
 resultSet1=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
 for k1,re  in resultSet1:
    DT=re[u'datach']
    OBJECT=re[u'object']
    topico=re[u'topico']
    #
    #DT=results[0]
    #OBJECT=results[1]
    #topico=results[2]
    if ant == None:
        ant=OBJECT
        caracts.append([topico,DT])
    else: 
        if OBJECT != ant :
         objs.append([ant,caracts])
         caracts=[]
         caracts.append([topico,DT])
        else:
         caracts.append([topico,DT])
        ant = OBJECT
 if len(caracts) > 0 :
   objs.append([OBJECT,caracts])
 
 cr_ob=[]
 
 for ob in objs:
   have_er=False
   for dt in ob[1]:
     topico=dt[0]
     DT=dt[1]
     if topico.lower()=='destination' and DT.lower() == e_purpose.lower():
      have_er=True
   if have_er:
     cr_ob.append(ob)
 
 
 dest2=[]
 for o_t in cr_ob:
  tps=[] 
  print 'Run-OBJ:',o_t[0]
  #resultSet = conn4.sql ("SELECT DT,topico FROM SEMANTIC_OBJECT_DT where username ='"+usr+"' and object = '"+o_t[0]+"' order by OBJECT ")
  ant=None
  caracts=[]
  #for results in resultSet:
  startkl=0
  while True: 
     startkl+=1
     try:
       results=mdTb.tb_object_dt1.get(o_t[0]+"|"+str(startkl) )
     except: break 
     DT=results[u'datach']
     topico=results[u'topico']
     caracts.append([topico,DT])
  if len(caracts) > 0 :
    o_t[1]=caracts
  dest1=[]
  
  print 'Collect L:',o_t[0],usr
  dt2=collect_objs_fct2(o_t[0],usr)
  for dt2c in dt2:
   dest2.append(dt2c)
  #=============================
  for dt in o_t[1]:
   topico=dt[0]
   DT=dt[1]
   if topico.lower()==e_purpose :
    dest2.append(DT)
 
 print 'Layouts-F:',dest2
 return dest2 

   
 
 
def load_ractionlines2(usr,global_er,global_dts,e_purpose):
 #lay=[name,[topicos],[links]]
 #formato de leitura dos onto-object:
 '''
 object:
 topico destinacao->destinacao{ex:pessoa-classificacao}
 topico->dt[sinapse]
 ^
 |
 [topico{sub-topico}]->dt[sinapse]
 '''

 objs=[]
 ant=None
 caracts=[]
 cl4 = index.create_index_expression(column_name='topico', value='destination')
 clausec = index.create_index_clause([cl4],count=1000000)
 resultSet1=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
 for k1,re  in resultSet1:
    DT=re[u'datach']
    OBJECT=re[u'object']
    topico=re[u'topico']
    #==================================
    #DT=results[0]
    #OBJECT=results[1]
    #topico=results[2]
    if ant == None:
        ant=OBJECT
        caracts.append([topico,DT])
    else: 
        if OBJECT != ant :
         objs.append([ant,caracts])
         caracts=[]
         caracts.append([topico,DT])
        else:
         caracts.append([topico,DT])
        ant = OBJECT
 if len(caracts) > 0 :
   objs.append([OBJECT,caracts])
 
 cr_ob=[]
 for ob in objs:
   have_er=False
   for dt in ob[1]:
     topico=dt[0]
     DT=dt[1]
     if topico.lower()=='destination' and DT.lower() == e_purpose.lower():
      have_er=True
   if have_er:
     cr_ob.append(ob)
 
 dest2=[]
 for o_t in cr_ob:
  tps=[] 
  #resultSet = conn4.sql ("SELECT DT,topico FROM SEMANTIC_OBJECT_DT where username ='"+usr+"' and object = '"+o_t[0]+"' order by OBJECT ")
  ant=None
  caracts=[]
  startkl=0
  while True: 
     startkl+=1
     try:
       results=mdTb.tb_object_dt1.get(o_t[0]+"|"+startkl)
     except: break 
     DT=results[u'datach']
     topico=results[u'topico']
     caracts.append([topico,DT])
  if len(caracts) > 0 :
    o_t[1]=caracts
  dest1=[]
     
  dt2=collect_objs_fct22(o_t[0],usr)
  for dt2c in dt2:
   dest2.append(dt2c)
 
 print 'Layouts-2:',dest2
 return dest2  
 
 
def load_ractionlines3(usr,global_er,global_dts,e_purpose):
 #lay=[name,[topicos],[links]]
 #formato de leitura dos onto-object:
 '''
 object:
 topico destinacao->destinacao{ex:pessoa-classificacao}
 topico->dt[sinapse]
 ^
 |
 [topico{sub-topico}]->dt[sinapse]
 '''

 #resultSet = conn4.sql ("SELECT DT,OBJECT,topico FROM SEMANTIC_OBJECT_DT where username ='"+usr+"' and (topico = 'destination' or topico = 'purpose') order by OBJECT ")
 objs=[]
 ant=None
 caracts=[]
 
 cl4 = index.create_index_expression(column_name='topico', value='destination')
 clausec = index.create_index_clause([cl4],count=1000000)
 resultSet1=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
 for k1,re  in resultSet1:
    DT=re[u'datach']
    OBJECT=re[u'object']
    topico=re[u'topico']
    #
    #DT=results[0]
    #OBJECT=results[1]
    #topico=results[2]
    if ant == None:
        ant=OBJECT
        caracts.append([topico,DT])
    else: 
        if OBJECT != ant :
         objs.append([ant,caracts])
         caracts=[]
         caracts.append([topico,DT])
        else:
         caracts.append([topico,DT])
        ant = OBJECT
 if len(caracts) > 0 :
   objs.append([OBJECT,caracts])
 
 cr_ob=[]
 for ob in objs:
   have_er=False
   have_purp=False
   for dt in ob[1]:
     topico=dt[0]
     DT=dt[1]
     if (topico.lower()=='destination' or topico.lower()=='purpose') and DT.lower() == 'intern-er':
      have_er=True
   if have_er :
     print 'Collected dest-RCT:[',ob,']' 
     cr_ob.append(ob)
 
 dest2=[]
 for o_t in cr_ob:
  tps=[] 
  #resultSet = conn4.sql ("SELECT DT,topico FROM SEMANTIC_OBJECT_DT where username ='"+usr+"' and object = '"+o_t[0]+"' order by OBJECT ")
  ant=None
  caracts=[]
  startkl=0
  while True: 
     startkl+=1
     try:
       results=mdTb.tb_object_dt1.get(o_t[0]+"|"+startkl)
     except: break 
     DT=results[u'datach']
     topico=results[u'topico']
     caracts.append([topico,DT])
  if len(caracts) > 0 :
    o_t[1]=caracts
  dest1=[]
     
  dt2=collect_objs_fct23(o_t[0],usr)
  for dt2c in dt2:
   dest2.append(dt2c)
 
 print 'Layouts-3:',dest2
 return dest2   

def get_LayoutsFZ(usr,purp_c):
 global_dts=[]
 global_er=[]
 # carregar ractionlines desse proposito
 # carregar os facts necessarios para cara ractionline
 # carregar os layouts necessarios em cada fact
 print 'Get-layouts:',usr,global_er,global_dts,purp_c
 rs_r=load_ractionlines(usr,global_er,global_dts,purp_c)
 return rs_r
 
 
def get_LayoutsFZ2(usr,purp_c):
 global_dts=[]
 global_er=[]
 # carregar ractionlines desse proposito
 # carregar os facts necessarios para cara ractionline
 # carregar os layouts necessarios em cada fact
 rs_r=load_ractionlines2(usr,global_er,global_dts,purp_c)
 return rs_r
  
def get_purpsz(usr,purp_c):
 global_dts=[]
 global_er=[]
 # carregar ractionlines desse proposito
 # carregar os facts necessarios para cara ractionline
 # carregar os layouts necessarios em cada fact
 rs_r=load_ractionlines3(usr,global_er,global_dts,purp_c)
 return rs_r
  
  
 

 
 
 