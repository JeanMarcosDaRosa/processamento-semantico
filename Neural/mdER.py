# coding: latin-1
''' controla a aquisicao de dados extruturados, bem como validacao dos mesmos '''

import mdNeural
import umisc
import logging
import mdTb
import pycassa
from pycassa.pool import ConnectionPool
from pycassa import index
from pycassa.columnfamily import ColumnFamily 
from operator import itemgetter, attrgetter

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('mdER')

# tipos de ER

''' 
 =>fact descritivo : object build -> composicao de objeto:
  --definicao :
    composicao
    dominio(web)
    classificacao
    relacoes(relacoes importantes com outros elementos(amizada,namoro,profissional,...))
 =>analise caracts:
  --dependencias : need(web)
  --solucoes implementadas:(way)
  --reputacao :opiniao-sobre
 =>fact narrativo: (event->action)-(comportamento->situacao+action)
  ->o que
  ->com quem/o que
  ->quando
  ->como
  ->onde  
 =>filtro : foco( caracts,dominio,classe,relacao ) , foco(fact)
 =>analise cruzamento: comparacao , combinacao , checkagem de composicao,dominio,relactions,classificacao,facts de diferentes objetos  -- usado para medir aderenciam ao cruzar entidades em determinados niveis
 obs: filtros sucessivos + cruzamento == resumo 
'''
     
# exemploe de modelos de ER para analize de pessoas
'''
 ->pessoa->relacoes(amizade/parceria/empregado/chefe/casamento/colega-trabalho/representante/conveniado)
 ->pessoa->compra(demanda)
 ->pessoa->venda(oferta)
 ->pessoa->necessidade
 ->pesquisa->habilidades
 ->pessoa->reputacao
 ->pessoa->classificacao
 ->pessoa->historico( BPM )
 =>>pessoa->pesquisa(dominio/classificacao/posse/reputacao/habilidades )
 
 ->bem-de-consumo(carro,moto,imovel,etc..)
  ->preco
  ->caracts->caract c/ importancia
  ->reputacao
  ->historico( bem mut�vel )
 
'''


def get_src_by_obj(obj,usr): 
 obj_nm=obj 
 dts=[]
 #-----------
 # 
 # 
 indl=0
 while True:  
      indl+=1
      try:
        resultSet=mdTb.tb_object_dt1.get(obj+"|"+str(indl))
      except: break  
      DT=resultSet[u'datach'] 
      TOP=resultSet[u'topico']
      if TOP == 'source': # usado para fins de marca��o de prop�sito
       dts.append( DT )
 return dts

def get_src_by_obj2(obj,usr): 
 obj_nm=obj 
 dts=[]
 #-----------
 # 
 indl=0
 while True:  
      indl+=1
      try:
        resultSet=mdTb.tb_object_dt1.get(obj+"|"+str(indl))
      except: break  
      DT=resultSet[u'datach'] 
      TOP=resultSet[u'topico']
      if TOP == 'wsource': # usado para fins de marca��o de prop�sito
       dts.append( DT )
 return dts

 
def collect_objs_fct(ob,usr):
  cats_r=[]     
  #  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(ob))
  cl5 = index.create_index_expression(column_name='opcode', value='FACT')
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   rs=get_src_by_obj(ido,usr)
   for r in rs:
    if umisc.trim(r) != '':
     cats_r.append(r)
  return cats_r
  
  
def collect_objs_fct2(ob,usr):
  cats_r=[]        
  #  
  cl4 = index.create_index_expression(column_name='obj_orig', value=str(ob))
  cl5 = index.create_index_expression(column_name='opcode', value='FACT')
  #
  #
  clausec = index.create_index_clause([cl5,cl4],count=1000000)
  resultSet=mdTb.tb_relaction1.get_indexed_slices(clausec)
  for ky,resultsC in resultSet: 
   ido=resultsC[u'obj_dest']
   rs=get_src_by_obj2(ido,usr)
   for r in rs:
    if umisc.trim(r) != '':
     cats_r.append(r)
  return cats_r
 

def get_object_by_data(obj,usr): 
 obj_nm=obj 
 #-----------
 lay=mdNeural.mdLayer ()
 lay.name=obj_nm
 #----------- 
 obj_nm=None
 ind1=0
 while True:
      ind1+=1 
      try:
         results=mdTb.tb_object_dt1.get(obj+"|"+str(ind1))
      except: pass 
      DT=results[u'datach'] 
      TOP=results[u'topico']
      if TOP != 'destination': # usado para fins de marca��o de prop�sito
       nrs=[]
       tmp=''
       indice_p=0
       fir=True
       for d in DT:
        if d == ',':
         if fir:
          nrs.append(tmp)
          tmp=''
         else:
           if DT[indice_p-1] != '\\':
            nrs.append(tmp)
            tmp=''          
           else:tmp+=d
        else:tmp+=d
       if tmp != '': nrs.append(tmp)
       if len(nrs) == 0 :               
        nr= lay.set_nr(DT)
        tps=lay.get_topico(TOP)
        if tps == None:
         tps=lay.set_topico(TOP)       
        tps.connect_to(lay,nr,'Composicao') 
       else:
        for DT in nrs:
         nr= lay.set_nr(DT)
         tps=lay.get_topico(TOP)
         if tps == None:
          tps=lay.set_topico(TOP)       
         tps.connect_to(lay,nr,'Composicao') 
       
 return lay   
 
def get_ontology(aliases,purposes,usr): 
 tree_h=[]
 for alias in aliases:
     print 'Collect al for:',alias,usr
     try:
       obj_principal=mdTb.get_object_by_data2z_cod(alias,alias,False)
     except: continue
     tree_h.append(obj_principal)        
     #===
 return tree_h 

     
def create_RCT(lays,dests,global_er,global_dts):
 for lay in lays:
     global_er.append(lay)    
 for d in dests :
   global_dts.append(d)
     



def load_ractionlines(usr,purpose,global_er,global_dts):
 #lay=[name,[topicos],[links]]
 #formato de leitura dos onto-object:
 '''
 object:
 topico destinacao->destinacao{ex:pessoa-classificacao}
 topico->dt[sinapse]
 ^
 |
 [topico{sub-topico}]->dt[sinapse]
 '''

 def get_priority(usre,obj):
  cl4 = index.create_index_expression(column_name='object', value=obj)
  cl5 = index.create_index_expression(column_name='topico', value='priority')
  clausec = index.create_index_clause([cl4,cl5],count=1000000)
  resultSet=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
  #  
  ant=None
  caracts=[]
  prior=0
  for ky,results in resultSet:
     DT=results[u'datach']
     prior=int(DT)
     break
  return prior

 resultSet=[]

 cl4 = index.create_index_expression(column_name='topico', value='destination')
 clausec = index.create_index_clause([cl4],count=1000000)
 resultSet1=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
 for k1,re  in resultSet1:
    DT=re[u'datach']
    OBJECT=re[u'object']
    topico=re[u'topico']
    resultSet.append([DT,OBJECT,topico])

 def zin(el):
  fnd=False
  [DT1,OBJECT1,topico1]=el
  for [DT,OBJECT,topico] in resultSet:
    if DT == DT1 and OBJECT==OBJECT1 and  topico==topico1:
     fnd=True
     break
  return fnd  
 cl4 = index.create_index_expression(column_name='topico', value='purpose')
 clausec = index.create_index_clause([cl4],count=1000000)
 resultSet1=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
 for k1,re  in resultSet1:
    DT=re[u'datach']
    OBJECT=re[u'object']
    topico=re[u'topico']
    if not zin([DT,OBJECT,topico]):
      resultSet.append([DT,OBJECT,topico])
  
 resultSet=sorted(resultSet, key=itemgetter(1), reverse=False) 
  
 objs=[]
 ant=None
 caracts=[]
 
 for [DT,OBJECT,topico] in resultSet:
    if ant == None:
        ant=OBJECT
        caracts.append([topico,DT])
    else: 
        if OBJECT != ant :
         objs.append([ant,caracts])
         caracts=[]
         caracts.append([topico,DT])
        else:
         caracts.append([topico,DT])
        ant = OBJECT
 if len(caracts) > 0 :
   objs.append([OBJECT,caracts])
  

 cr_ob=[]
 for ob in objs:
   have_purp=False
   have_er=False
   for dt in ob[1]:
     topico=dt[0]
     DT=dt[1]
     if (topico.lower()=='destination' or topico.lower()=='purpose') and DT.lower () == 'intern-er':
      have_er=True
     if (topico.lower()=='destination' or topico.lower()=='purpose') and DT.lower () == purpose.lower():
      have_purp=True
   #==   
   if have_er and have_purp:
     print 'Collected rct:[',ob[0],']' 
     prior=get_priority(usr,ob[0])
     cr_ob.append([prior,ob])
 
 cr_ob.sort()
 
 for [prior,o_t] in cr_ob:
  tps=[]
  ant=None
  caracts=[]
  inds=0
  while True:
     inds+=1
     try:
      results=mdTb.tb_object_dt1.get(o_t[0]+"|"+str(inds)) 
     except:
      break
     #for results in resultSet:
     DT=results[u'datach']
     topico=results[u'topico']
     caracts.append([topico,DT])
  if len(caracts) > 0 :
    o_t[1]=caracts
  dest1=[]
  dest2=[]
  dt2=collect_objs_fct(o_t[0],usr)
  for dt2c in dt2:
   dest1.append(['source',[dt2c]])
  #=============================
  for dt in o_t[1]:
   topico=dt[0]
   DT=dt[1]
   if topico.lower()=='source' :
    dest1.append([topico,[DT]])
    
  dt2=collect_objs_fct2(o_t[0],usr)
  for dt2c in dt2:
   dest2.append(['wsource',[dt2c]])
  #=============================
  for dt in o_t[1]:
   topico=dt[0]
   DT=dt[1]
   if topico.lower()=='wsource' :
    dest2.append([topico,[DT]])

  dests=[dest1,dest2]
  #lay1=[ 'pessoa-classificacao',[['composition',['']],['class',['']],['filter',['']]  ],[] ] 
  lays=get_ontology([o_t[0]],['FACT','CALL','BEHAVIOR','LAYOUTCODE','LAYOUT-CODE'] ,usr) #FACT-> purpose de link ractionline->fact 
  #create_RCT( [lay1],dests,global_er,global_dts )   
  print 'RCT:','{',lays,'}'
  create_RCT( [lays[0]],dests,global_er,global_dts ) 


 
def load_ractionlines2C(usr,purpose,global_er,global_dts):
 #lay=[name,[topicos],[links]]
 #formato de leitura dos onto-object:
 '''
 object:
 topico destinacao->destinacao{ex:pessoa-classificacao}
 topico->dt[sinapse]
 ^
 |
 [topico{sub-topico}]->dt[sinapse]
 '''
 def get_priority(usre,obj):
  cl4 = index.create_index_expression(column_name='object', value=obj)
  cl5 = index.create_index_expression(column_name='topico', value='priority')
  clausec = index.create_index_clause([cl4,cl5],count=1000000)
  resultSet=mdTb.tb_object_dt1.get_indexed_slices(clausec)  
  #  
  ant=None
  caracts=[]
  prior=0
  for ky,results in resultSet:
     DT=results[u'datach']
     prior=int(DT)
     break
  return prior
 #
 #
 # 
  
 objs=[]
 ant=None
 caracts=[]
 
 #//--1 
 #for o in resultO: 
 while True:
  try:
   o=mdTb.tb_object1.get(purpose)
  except:  break 
  cro=purpose
  # 
  caracts=[]
  indlk=0
  while True:
    indlk+=1
    try:
     results=mdTb.tb_object_dt1.get(purpose+"|"+str(indlk))
    except:
     break
    DT=results[u'datach']    
    topico=results[u'topico']
    caracts.append([topico,DT])
  objs.append([cro,caracts]) 
  break
  
 
 cr_ob=[]
 for ob in objs:
   print 'Collected rct:[',ob[0],']' 
   prior=get_priority(usr,ob[0])
   cr_ob.append([prior,ob])
 
 cr_ob.sort()
 
 for [prior,o_t] in cr_ob:
  tps=[]
 
  # 
  ant=None
  caracts=[]
  indkl=0
  while True:
     indkl+=1
     try:
      results=mdTb.tb_object_dt1.get(o_t[0]+"|"+str(indkl))
     except: break 
     DT=results[u'datach']
     topico=results[u'topico']
     caracts.append([topico,DT])
  if len(caracts) > 0 :
    o_t[1]=caracts
  dest1=[]
  dest2=[]
  dt2=collect_objs_fct(o_t[0],usr)
  for dt2c in dt2:
   dest1.append(['source',[dt2c]])
  #=============================
  for dt in o_t[1]:
   topico=dt[0]
   DT=dt[1]
   if topico.lower()=='source' :
    dest1.append([topico,[DT]])
    
  dt2=collect_objs_fct2(o_t[0],usr)
  for dt2c in dt2:
   dest2.append(['wsource',[dt2c]])
  #=============================
  for dt in o_t[1]:
   topico=dt[0]
   DT=dt[1]
   if topico.lower()=='wsource' :
    dest2.append([topico,[DT]])

  dests=[dest1,dest2]
  #lay1=[ 'pessoa-classificacao',[['composition',['']],['class',['']],['filter',['']]  ],[] ] 
  lays=get_ontology([o_t[0]],['FACT','CALL','BEHAVIOR','LAYOUTCODE','LAYOUT-CODE'] ,usr) #FACT-> purpose de link ractionline->fact 
  
  #create_RCT( [lay1],dests,global_er,global_dts )   
  print 'RCT:','{',lays,'}'
  for l in lays:
    print '{',l.name,'}'
  create_RCT( [lays[0]],dests,global_er,global_dts )  

def get_RactionLines2C(usr,purpose):
 global_dts=[]
 global_er=[]
 # carregar ractionlines desse proposito
 # carregar os facts necessarios para cara ractionline
 # carregar os layouts necessarios em cada fact
 load_ractionlines2C(usr,purpose,global_er,global_dts)
 return [global_er ,global_dts]
 
def get_RactionLines(usr,purpose):
 global_dts=[]
 global_er=[]
 # carregar ractionlines desse proposito
 # carregar os facts necessarios para cara ractionline
 # carregar os layouts necessarios em cada fact
 load_ractionlines(usr,purpose,global_er,global_dts)
 return [global_er ,global_dts]


def get_external_data_ER(params,ini_par):
 # ini_par => contador para situar os cicles
 have_next_cicle=False # para o caso de paginas,querys,etc.. onde existam necessidade de aquisicao em lotes de dados, para processar toda a informacao, utiliza-se o cicle
 #========== 
 '''
  -> formato de leitura em sgdb:  titulo:dado titulo:dado titulo:dato |  relacao:data1,dado2 => transformar rows para txt
  ->excel -> exibir em tela os layouts encontrados(layout possiveis de documentos) -> assim um analista de informacao informa um layout para aquele tipo de informacao(disposicao de dados)
              a fim de encaixar os titulos com os dados subsequentes(obs: alguns layouts ja devem estar  instalados nos clientes. ex:disposicao em coluna X linha, separacao de pagina por linha em branco, etc..  )    
  ->email: subject como elemento principal e conteudo como complemento( :titulo encabeca o dado, o body fica como informacao lilnkada, como pagina subsequente. )            
 '''  
 
 #==========
 # verificar se o retorno nao depende de tratamento fuzzy
 #==========
 return[[],have_next_cicle]
 