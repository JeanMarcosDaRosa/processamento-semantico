fuzzy simple-act-1.travel
   force_position Y
   madatory Y
   layout_onto interact.event
   direction R 
   an AN
   sq 1000 1000
   pref 
   def 
    dt vai,vamos,predendo,predendemos,iremos,ir�o,foram,fomos,quero,queremos,querem,queriam,quiz
    sn 
    return 
    direct 
   def 
    dt viajar,passear,estar,visitar,conhecer
    sn 
    return 
    direct 
   def 
    dt em,para,no,pro,pra
    sn 
    return [interact.event,travel,interact.event]
    direct 
   suf 
end
